# Bewerbungs-Manager


## Installation

1) Starten Sie einen Terminal, wechseln Sie in den Document Root und führen Sie
```
composer install
```
aus (composer muss installiert sein).

2) Legen Sie in ihrem bevorzugten RDBMS eine leere Datenbank namens application_manager an. Bei einem anderen Namen müssten Sie noch die Migration anpassen.

3) Öffnen Sie im Document Root die Datei .env. Befüllen Sie die leere Variable
```
DATABASE_URL
```
mit dem Verbindungsstring zur gerade angelegten Datenbank. Mehr Infos dazu [hier](https://symfony.com/doc/current/doctrine.html#configuring-the-database).

4) Befüllen Sie die leere Variable
```
MAILER_DSN
```
mit einem Verbindungsstring zu ihrem bevorzugten Mailer-Transport-Service. Mehr Infos dazu [hier](https://symfony.com/doc/current/mailer.html#using-built-in-transports).

5) Öffnen Sie, ausgehend vom Document Root, die Datei services.yaml im Verzeichnis config. Der Parameter
```
app.adminEmail
```
stellt die Absende-E-Mail-Adresse für alle vom System ausgehenden Mails dar. Bitte befüllen Sie ihn mit der E-Mail-Adresse, für die der gerade konfigurierte Mailer-Service ausgelegt ist.

6) Führen Sie mit folgendem Kommando im Terminal die Datenbank-Migration aus:
```
php bin/console doctrine:migrations:migrate
```

7) Ab diesem Punkt sollte die Anwendung vollständig lauffähig sein. Überprüfen Sie dies sinnvollerweise mit den integrierten funktionalen Tests, die mit
```
php bin/phpunit
```
im Terminal aufgerufen werden.

8) Öffnen Sie im Browser, ausgehend vom Document Root, die Datei index.php im Verzeichnis public. Hier können Sie sich mit folgenden Daten einloggen:
```
Benutzer: demouser
Passwort: Test123!
```
Sie werden dann weitergeleitet und können alle Funktionen der Website nutzen.

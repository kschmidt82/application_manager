<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230706233433 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->createTables();
        $this->addSql(file_get_contents(__DIR__.'/queries1.sql'));
        $this->addSql('use `application_manager_test`');
        $this->createTables();
        $this->addSql(file_get_contents(__DIR__.'/queries2.sql'));
        $this->addSql('use `application_manager`');
    }

    private function createTables(): void
    {
        $this->addSql('CREATE TABLE application_events (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, application_id SMALLINT UNSIGNED NOT NULL, event_id SMALLINT UNSIGNED NOT NULL, notes LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_application_id (application_id), INDEX idx_event_id (event_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE applications (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED NOT NULL, company_id SMALLINT UNSIGNED DEFAULT NULL, company_contact_id SMALLINT UNSIGNED DEFAULT NULL, recruiter_id SMALLINT UNSIGNED DEFAULT NULL, recruiter_contact_id SMALLINT UNSIGNED DEFAULT NULL, job_title_id SMALLINT UNSIGNED NOT NULL, job_market_id SMALLINT UNSIGNED DEFAULT NULL, archive_id SMALLINT UNSIGNED DEFAULT NULL, reference_number VARCHAR(191) DEFAULT NULL, job_description_link VARCHAR(191) DEFAULT NULL, application_portal_link VARCHAR(191) DEFAULT NULL, no_automatic_emails TINYINT(1) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_company_id (company_id), INDEX idx_company_contact_id (company_contact_id), INDEX idx_job_market_id (job_market_id), INDEX idx_job_title_id (job_title_id), INDEX idx_archive_id (archive_id), INDEX idx_recruiter_id (recruiter_id), INDEX idx_recruiter_contact_id (recruiter_contact_id), INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE archives (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED DEFAULT NULL, name VARCHAR(191) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE columns (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, table_id SMALLINT UNSIGNED NOT NULL, name VARCHAR(191) NOT NULL, value VARCHAR(191) NOT NULL, INDEX idx_table_id (table_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE companies (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED NOT NULL, name VARCHAR(191) NOT NULL, address VARCHAR(191) DEFAULT NULL, zip VARCHAR(191) DEFAULT NULL, city VARCHAR(191) DEFAULT NULL, website VARCHAR(191) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_contacts (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, company_id SMALLINT UNSIGNED NOT NULL, gender_id SMALLINT UNSIGNED NOT NULL, title_id SMALLINT UNSIGNED DEFAULT NULL, is_informal TINYINT(1) NOT NULL, firstname VARCHAR(191) DEFAULT NULL, lastname VARCHAR(191) NOT NULL, email_address VARCHAR(191) NOT NULL, phone_number VARCHAR(191) DEFAULT NULL, address VARCHAR(191) DEFAULT NULL, zip VARCHAR(191) DEFAULT NULL, city VARCHAR(191) DEFAULT NULL, notes LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_2BD7001E979B1AD6 (company_id), INDEX idx_gender_id (gender_id), INDEX idx_title_id (title_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE conditions (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, result_id SMALLINT UNSIGNED NOT NULL, data_field_id SMALLINT UNSIGNED NOT NULL, operator_id SMALLINT UNSIGNED NOT NULL, value VARCHAR(191) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_result_id (result_id), INDEX idx_data_field_id (data_field_id), INDEX idx_operator_id (operator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_fields (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, column_id SMALLINT UNSIGNED DEFAULT NULL, placeholder_id SMALLINT UNSIGNED DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_column_id (column_id), INDEX idx_placeholder_id (placeholder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE email_types (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, name VARCHAR(191) NOT NULL, value VARCHAR(191) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE emails (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED NOT NULL, email_type_id SMALLINT UNSIGNED NOT NULL, data_field_id SMALLINT UNSIGNED NOT NULL, operator_id SMALLINT UNSIGNED NOT NULL, value VARCHAR(191) DEFAULT NULL, priority SMALLINT UNSIGNED DEFAULT NULL, name VARCHAR(191) NOT NULL, subject VARCHAR(191) DEFAULT NULL, text LONGTEXT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_user_id (user_id), INDEX idx_email_type_id (email_type_id), INDEX idx_data_field_id (data_field_id), INDEX idx_operator_id (operator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE events (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED DEFAULT NULL, name VARCHAR(191) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genders (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED DEFAULT NULL, name VARCHAR(191) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_markets (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED DEFAULT NULL, name VARCHAR(191) NOT NULL, link VARCHAR(191) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_titles (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED DEFAULT NULL, name VARCHAR(191) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE operators (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, name VARCHAR(191) NOT NULL, value VARCHAR(191) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE placeholders (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED DEFAULT NULL, name VARCHAR(191) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recruiter_contacts (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, recruiter_id SMALLINT UNSIGNED NOT NULL, gender_id SMALLINT UNSIGNED NOT NULL, title_id SMALLINT UNSIGNED DEFAULT NULL, is_informal TINYINT(1) NOT NULL, firstname VARCHAR(191) DEFAULT NULL, lastname VARCHAR(191) NOT NULL, email_address VARCHAR(191) NOT NULL, phone_number VARCHAR(191) DEFAULT NULL, address VARCHAR(191) DEFAULT NULL, zip VARCHAR(191) DEFAULT NULL, city VARCHAR(191) DEFAULT NULL, notes LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_gender_id (gender_id), INDEX idx_recruiter_id (recruiter_id), INDEX idx_title_id (title_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recruiters (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED NOT NULL, name VARCHAR(191) NOT NULL, address VARCHAR(191) DEFAULT NULL, zip VARCHAR(191) DEFAULT NULL, city VARCHAR(191) DEFAULT NULL, website VARCHAR(191) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_requests (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE results (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, placeholder_id SMALLINT UNSIGNED NOT NULL, result LONGTEXT NOT NULL, priority SMALLINT UNSIGNED DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_placeholder_id (placeholder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tables (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, name VARCHAR(191) NOT NULL, value VARCHAR(191) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE titles (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, user_id SMALLINT UNSIGNED DEFAULT NULL, name VARCHAR(191) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, username VARCHAR(20) NOT NULL, password VARCHAR(191) NOT NULL, email_address VARCHAR(191) NOT NULL, firstname VARCHAR(191) NOT NULL, lastname VARCHAR(191) NOT NULL, notes LONGTEXT DEFAULT NULL, is_verified TINYINT(1) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_general_ci` ENGINE = InnoDB');

        $this->addSql('ALTER TABLE application_events ADD CONSTRAINT fk_application_id_application_events FOREIGN KEY (application_id) REFERENCES applications (id), ADD CONSTRAINT fk_event_id_application_events FOREIGN KEY (event_id) REFERENCES events (id)');
        $this->addSql('ALTER TABLE applications ADD CONSTRAINT fk_user_id_applications FOREIGN KEY (user_id) REFERENCES users (id), ADD CONSTRAINT fk_company_id_applications FOREIGN KEY (company_id) REFERENCES companies (id), ADD CONSTRAINT fk_company_contact_id_applications FOREIGN KEY (company_contact_id) REFERENCES company_contacts (id), ADD CONSTRAINT fk_recruiter_id_applications FOREIGN KEY (recruiter_id) REFERENCES recruiters (id), ADD CONSTRAINT fk_recruiter_contact_id_applications FOREIGN KEY (recruiter_contact_id) REFERENCES recruiter_contacts (id), ADD CONSTRAINT fk_job_title_id_applications FOREIGN KEY (job_title_id) REFERENCES job_titles (id), ADD CONSTRAINT fk_job_market_id_applications FOREIGN KEY (job_market_id) REFERENCES job_markets (id), ADD CONSTRAINT fk_archive_id_applications FOREIGN KEY (archive_id) REFERENCES archives (id)');
        $this->addSql('ALTER TABLE archives ADD CONSTRAINT fk_user_id_archives FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE columns ADD CONSTRAINT fk_table_id_columns FOREIGN KEY (table_id) REFERENCES tables (id)');
        $this->addSql('ALTER TABLE companies ADD CONSTRAINT fk_user_id_companies FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE company_contacts ADD CONSTRAINT fk_company_id_company_contacts FOREIGN KEY (company_id) REFERENCES companies (id), ADD CONSTRAINT fk_gender_id_company_contacts FOREIGN KEY (gender_id) REFERENCES genders (id), ADD CONSTRAINT fk_title_id_company_contacts FOREIGN KEY (title_id) REFERENCES titles (id)');
        $this->addSql('ALTER TABLE conditions ADD CONSTRAINT fk_result_id_conditions FOREIGN KEY (result_id) REFERENCES results (id), ADD CONSTRAINT fk_data_field_id_conditions FOREIGN KEY (data_field_id) REFERENCES data_fields (id), ADD CONSTRAINT fk_operator_id_conditions FOREIGN KEY (operator_id) REFERENCES operators (id)');
        $this->addSql('ALTER TABLE data_fields ADD CONSTRAINT fk_column_id_data_fields FOREIGN KEY (column_id) REFERENCES columns (id), ADD CONSTRAINT fk_placeholder_id_data_fields FOREIGN KEY (placeholder_id) REFERENCES placeholders (id)');
        $this->addSql('ALTER TABLE emails ADD CONSTRAINT fk_user_id_emails FOREIGN KEY (user_id) REFERENCES users (id), ADD CONSTRAINT fk_email_type_id_emails FOREIGN KEY (email_type_id) REFERENCES email_types (id), ADD CONSTRAINT fk_data_field_id_emails FOREIGN KEY (data_field_id) REFERENCES data_fields (id), ADD CONSTRAINT fk_operator_id_emails FOREIGN KEY (operator_id) REFERENCES operators (id)');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT fk_user_id_events FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE genders ADD CONSTRAINT fk_user_id_genders FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE job_markets ADD CONSTRAINT fk_user_id_job_markets FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE job_titles ADD CONSTRAINT fk_user_id_job_titles FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE placeholders ADD CONSTRAINT fk_user_id_placeholders FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE recruiter_contacts ADD CONSTRAINT fk_recruiter_id_recruiter_contacts FOREIGN KEY (recruiter_id) REFERENCES recruiters (id), ADD CONSTRAINT fk_gender_id_recruiter_contacts FOREIGN KEY (gender_id) REFERENCES genders (id), ADD CONSTRAINT fk_title_id_recruiter_contacts FOREIGN KEY (title_id) REFERENCES titles (id)');
        $this->addSql('ALTER TABLE recruiters ADD CONSTRAINT fk_user_id_recruiters FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE reset_password_requests ADD CONSTRAINT fk_user_id_reset_password_requests FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE results ADD CONSTRAINT fk_placeholder_id_results FOREIGN KEY (placeholder_id) REFERENCES placeholders (id)');
        $this->addSql('ALTER TABLE titles ADD CONSTRAINT fk_user_id_titles FOREIGN KEY (user_id) REFERENCES users (id)');
    }

    private function dropTables(): void
    {
        $this->addSql('ALTER TABLE application_events DROP FOREIGN KEY fk_application_id_application_events, DROP FOREIGN KEY fk_event_id_application_events');
        $this->addSql('ALTER TABLE applications DROP FOREIGN KEY fk_user_id_applications, DROP FOREIGN KEY fk_company_id_applications, DROP FOREIGN KEY fk_company_contact_id_applications, DROP FOREIGN KEY fk_recruiter_id_applications, DROP FOREIGN KEY fk_recruiter_contact_id_applications, DROP FOREIGN KEY fk_job_title_id_applications, DROP FOREIGN KEY fk_job_market_id_applications, DROP FOREIGN KEY fk_archive_id_applications');
        $this->addSql('ALTER TABLE archives DROP FOREIGN KEY fk_user_id_archives');
        $this->addSql('ALTER TABLE columns DROP FOREIGN KEY fk_table_id_columns');
        $this->addSql('ALTER TABLE companies DROP FOREIGN KEY fk_user_id_companies');
        $this->addSql('ALTER TABLE company_contacts DROP FOREIGN KEY fk_company_id_company_contacts, DROP FOREIGN KEY fk_gender_id_company_contacts, DROP FOREIGN KEY fk_title_id_company_contacts');
        $this->addSql('ALTER TABLE conditions DROP FOREIGN KEY fk_result_id_conditions, DROP FOREIGN KEY fk_data_field_id_conditions, DROP FOREIGN KEY fk_operator_id_conditions');
        $this->addSql('ALTER TABLE data_fields DROP FOREIGN KEY fk_column_id_data_fields, DROP FOREIGN KEY fk_placeholder_id_data_fields');
        $this->addSql('ALTER TABLE emails DROP FOREIGN KEY fk_user_id_emails, DROP FOREIGN KEY fk_email_type_id_emails, DROP FOREIGN KEY fk_data_field_id_emails, DROP FOREIGN KEY fk_operator_id_emails');
        $this->addSql('ALTER TABLE events DROP FOREIGN KEY fk_user_id_events');
        $this->addSql('ALTER TABLE genders DROP FOREIGN KEY fk_user_id_genders');
        $this->addSql('ALTER TABLE job_markets DROP FOREIGN KEY fk_user_id_job_markets');
        $this->addSql('ALTER TABLE job_titles DROP FOREIGN KEY fk_user_id_job_titles');
        $this->addSql('ALTER TABLE placeholders DROP FOREIGN KEY fk_user_id_placeholders');
        $this->addSql('ALTER TABLE recruiter_contacts DROP FOREIGN KEY fk_recruiter_id_recruiter_contacts, DROP FOREIGN KEY fk_gender_id_recruiter_contacts, DROP FOREIGN KEY fk_title_id_recruiter_contacts');
        $this->addSql('ALTER TABLE recruiters DROP FOREIGN KEY fk_user_id_recruiters');
        $this->addSql('ALTER TABLE reset_password_requests DROP FOREIGN KEY fk_user_id_reset_password_requests');
        $this->addSql('ALTER TABLE results DROP FOREIGN KEY fk_placeholder_id_results');
        $this->addSql('ALTER TABLE titles DROP FOREIGN KEY fk_user_id_titles');

        $this->addSql('DROP TABLE application_events');
        $this->addSql('DROP TABLE applications');
        $this->addSql('DROP TABLE archives');
        $this->addSql('DROP TABLE columns');
        $this->addSql('DROP TABLE companies');
        $this->addSql('DROP TABLE company_contacts');
        $this->addSql('DROP TABLE conditions');
        $this->addSql('DROP TABLE data_fields');
        $this->addSql('DROP TABLE email_types');
        $this->addSql('DROP TABLE emails');
        $this->addSql('DROP TABLE events');
        $this->addSql('DROP TABLE genders');
        $this->addSql('DROP TABLE job_markets');
        $this->addSql('DROP TABLE job_titles');
        $this->addSql('DROP TABLE operators');
        $this->addSql('DROP TABLE placeholders');
        $this->addSql('DROP TABLE recruiter_contacts');
        $this->addSql('DROP TABLE recruiters');
        $this->addSql('DROP TABLE reset_password_requests');
        $this->addSql('DROP TABLE tables');
        $this->addSql('DROP TABLE titles');
        $this->addSql('DROP TABLE users');
    }

    public function down(Schema $schema): void
    {
        $this->dropTables();
        $this->addSql('DROP DATABASE `application_manager_test`');
    }
}

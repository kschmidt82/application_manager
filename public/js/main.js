function getElements(selector)
{
	return document.querySelectorAll(selector);
}

function deleteRow(id, buttonId, formName, confirmText = '')
{
	if (confirmText === '') {
		let parts = id.split('--');
		confirmText = parts[parts.length-1];
	}
	if (confirm(confirmText)) {
		document.getElementById(buttonId).remove();
		document.forms[formName].submit();
	}
}

function deleteRowInIndex(id)
{
	let parts = id.split('--');
	deleteRow(id, 'deletebutton-' + parts[1], 'delete-' + parts[1], parts[2]);
}

function modifySelect(id)
{
	document.forms[0].hidden.value = 1;
	document.getElementById('submit').click();
}

window.addEventListener('DOMContentLoaded', function ()
{
	getElements('[id^=delete-link--]').forEach(el => el.addEventListener('click', event => {
		deleteRow(el.id, 'deletebutton', 'delete');
	}));

	getElements('[id^=delete-link-index--]').forEach(el => el.addEventListener('click', event => {
		deleteRowInIndex(el.id);
	}));

	getElements('#company').forEach(el => el.addEventListener('change', event => {
		modifySelect(el.id);
	}));

	getElements('#recruiter').forEach(el => el.addEventListener('change', event => {
		modifySelect(el.id);
	}));
});

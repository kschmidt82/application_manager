<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Application;
use App\Interface\ControllerConfigFactoryInterface;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Interface\OtherInterface;
use App\Interface\ShowInterface;
use App\Service\Logic\ApplicationAfterFlushService;
use App\Service\Logic\ApplicationBeforeValidationService;
use App\Service\Logic\ApplicationCreateAfterValidationService;
use App\Service\Logic\ApplicationEditAfterValidationService;
use App\Service\Logic\ApplicationLogicService;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/application')]
final readonly class ApplicationController
{
    use ControllerTrait;
    
    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = [
            ['id' => 'name', 'name' => 'company', 'target' => '../../company/{id}/show', 'object' => true],
            [
                'id' => 'emailAddress',
                'name' => 'companyContact',
                'lowerName' => 'company_contact',
                'object' => true,
                'noIndex' => true
            ],
            ['id' => 'name', 'name' => 'recruiter', 'target' => '../../recruiter/{id}/show', 'object' => true],
            [
                'id' => 'emailAddress',
                'name' => 'recruiterContact',
                'lowerName' => 'recruiter_contact',
                'object' => true,
                'noIndex' => true
            ],
            ['id' => 'name', 'name' => 'jobTitle', 'lowerName' => 'job_title', 'object' => true],
            ['id' => 'name', 'name' => 'jobMarket', 'lowerName' => 'job_market', 'object' => true],
            ['id' => 'referenceNumber', 'lowerName' => 'reference_number', 'noIndex' => true],
            ['id' => 'jobDescriptionLink', 'lowerName' => 'job_description_link', 'noIndex' => true, 'type' => 'link'],
            [
                'id' => 'applicationPortalLink',
                'lowerName' => 'application_portal_link',
                'noIndex' => true,
                'type' => 'link'
            ],
            ['id' => 'noAutomaticEmails', 'lowerName' => 'no_automatic_emails', 'noIndex' => true, 'type' => 'bool'],
            ['id' => 'createdAt', 'lowerName' => 'created_at', 'type' => 'date']
        ];
        $this->renderParameters = [
            'sendMails' => true,
            'subName' => 'application_event',
            'subLogo' => 'bi-calendar-event-fill'
        ];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{page<\d+>}', name: 'app_application_index', methods: ['GET', 'POST'])]
    public function index(IndexInterface $index, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page'], [$page]));
    }

    #[Route('/{id<\d+>}/show', name: 'app_application_show', methods: ['GET'])]
    public function show(ShowInterface $show, Application $application): Response
    {
        return $show->callRoute($this->controllerConfig->setValues(['entity'], [$application]));
    }

    #[Route('/create', name: 'app_application_create', methods: ['GET', 'POST'])]
    public function create(
        CreateInterface $create,
        ApplicationCreateAfterValidationService $afterValidation,
        ApplicationAfterFlushService $afterFlush,
        ApplicationBeforeValidationService $beforeValidation
    ): Response {
        $keys = ['beforeValidation', 'afterValidation', 'afterFlush', 'removeFormElements'];
        $values = [$beforeValidation, $afterValidation, $afterFlush, ['job_title']];
        return $create->callRoute($this->controllerConfig->setValues($keys, $values));
    }

    #[Route('/{id<\d+>}/edit', name: 'app_application_edit', methods: ['GET', 'POST'])]
    public function edit(
        EditInterface $edit,
        Application $application,
        ApplicationEditAfterValidationService $afterValidation,
        ApplicationBeforeValidationService $beforeValidation
    ): Response {
        $keys = ['entity', 'beforeValidation', 'afterValidation', 'removeFormElements'];
        $removals = ['type', 'type_name', 'website', 'gender_application', 'title', 'is_informal', 'firstname'];
        array_push($removals, 'lastname', 'email_address_application', 'job_title_application', 'new_job_title');
        $values = [$application, $beforeValidation, $afterValidation, $removals];
        return $edit->callRoute($this->controllerConfig->setValues($keys, $values));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_application_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, Application $application): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$application]));
    }

    #[Route('/emails', name: 'app_application_emails', methods: ['GET'])]
    public function email(OtherInterface $other, ApplicationLogicService $logic): Response
    {
        return $other->callRoute($this->controllerConfig->setValues(['action', 'logic'], ['email', $logic]));
    }

    #[Route('/close', name: 'app_application_close', methods: ['GET'])]
    public function close(OtherInterface $other, ApplicationLogicService $logic): Response
    {
        return $other->callRoute($this->controllerConfig->setValues(['action', 'logic'], ['close', $logic]));
    }
}

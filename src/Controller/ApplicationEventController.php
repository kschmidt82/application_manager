<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\ApplicationEvent;
use App\Interface\ControllerConfigFactoryInterface;
use App\Service\Logic\ApplicationEventAfterFlushService;
use App\Interface\CreateInterface as Create;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Interface\ShowInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/application_event')]
final readonly class ApplicationEventController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = [
            ['id' => 'name', 'name' => 'event', 'object' => true],
            ['id' => 'notes'],
            ['id' => 'createdAt', 'lowerName' => 'created_at', 'type' => 'date']
        ];
        $this->entityNameTop = 'Application';
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{application_id<\d+>}/{page<\d+>}', name: 'app_application_event_index', methods: ['GET'])]
    public function index(IndexInterface $index, int $application_id, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page', 'value'], [$page, $application_id]));
    }

    #[Route('/{id<\d+>}/show', name: 'app_application_event_show', methods: ['GET'])]
    public function show(ShowInterface $show, ApplicationEvent $applicationEvent): Response
    {
        return $show->callRoute($this->controllerConfig->setValues(['entity'], [$applicationEvent]));
    }

    #[Route('/{application_id<\d+>}/create', name: 'app_application_event_create', methods: ['GET', 'POST'])]
    public function create(Create $create, ApplicationEventAfterFlushService $afterFlush, int $application_id): Response
    {
        $values = [$afterFlush, $application_id];
        return $create->callRoute($this->controllerConfig->setValues(['afterFlush', 'value'], $values));
    }

    #[Route('/{id<\d+>}/edit', name: 'app_application_event_edit', methods: ['GET', 'POST'])]
    public function edit(EditInterface $edit, ApplicationEvent $applicationEvent): Response
    {
        return $edit->callRoute($this->controllerConfig->setValues(['entity'], [$applicationEvent]));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_application_event_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, ApplicationEvent $applicationEvent): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$applicationEvent]));
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\CompanyContact;
use App\Interface\ControllerConfigFactoryInterface;
use App\Trait\ContactColumnTrait;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Interface\ShowInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/company_contact')]
final class CompanyContactController
{
    use ContactColumnTrait;
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = $this->columns;
        $this->entityNameTop = 'Company';
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{company_id<\d+>}/{page<\d+>}', name: 'app_company_contact_index', methods: ['GET'])]
    public function index(IndexInterface $index, int $company_id, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page', 'value'], [$page, $company_id]));
    }

    #[Route('/{id<\d+>}/show', name: 'app_company_contact_show', methods: ['GET'])]
    public function show(ShowInterface $show, CompanyContact $companyContact): Response
    {
        return $show->callRoute($this->controllerConfig->setValues(['entity'], [$companyContact]));
    }

    #[Route('/{company_id<\d+>}/create', name: 'app_company_contact_create', methods: ['GET', 'POST'])]
    public function create(CreateInterface $create, int $company_id): Response
    {
        return $create->callRoute($this->controllerConfig->setValues(['value'], [$company_id]));
    }

    #[Route('/{id<\d+>}/edit', name: 'app_company_contact_edit', methods: ['GET', 'POST'])]
    public function edit(EditInterface $edit, CompanyContact $companyContact): Response
    {
        return $edit->callRoute($this->controllerConfig->setValues(['entity'], [$companyContact]));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_company_contact_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, CompanyContact $companyContact): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$companyContact]));
    }
}

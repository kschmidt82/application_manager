<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Condition;
use App\Interface\ControllerConfigFactoryInterface;
use App\Service\Logic\ConditionAfterValidationService;
use App\Service\Logic\ConditionBeforeValidationService;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Interface\ShowInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/condition')]
final readonly class ConditionController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = [
            ['id' => 'name', 'name' => 'dataField', 'multiples' => ['column', 'placeholder']],
            ['id' => 'name', 'name' => 'operator', 'object' => true],
            ['id' => 'value']
        ];
        $this->entityNameTop = 'Result';
        $this->entityNameTopTop = 'Placeholder';
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{result_id<\d+>}/{page<\d+>}', name: 'app_condition_index', methods: ['GET'])]
    public function index(IndexInterface $index, int $result_id, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page', 'value'], [$page, $result_id]));
    }

    #[Route('/{id<\d+>}/show', name: 'app_condition_show', methods: ['GET'])]
    public function show(ShowInterface $show, Condition $condition): Response
    {
        return $show->callRoute($this->controllerConfig->setValues(['entity'], [$condition]));
    }

    #[Route('/{result_id<\d+>}/create', name: 'app_condition_create', methods: ['GET', 'POST'])]
    public function create(
        CreateInterface $create,
        int $result_id,
        ConditionAfterValidationService $afterValidation,
        ConditionBeforeValidationService $beforeValidation
    ): Response {
        $keys = ['afterValidation', 'beforeValidation', 'value'];
        $values = [$afterValidation, $beforeValidation, $result_id];
        return $create->callRoute($this->controllerConfig->setValues($keys, $values));
    }

    #[Route('/{id<\d+>}/edit', name: 'app_condition_edit', methods: ['GET', 'POST'])]
    public function edit(
        EditInterface $edit,
        Condition $condition,
        ConditionAfterValidationService $afterValidation,
        ConditionBeforeValidationService $beforeValidation
    ): Response {
        $keys = ['afterValidation', 'beforeValidation', 'entity'];
        $values = [$afterValidation, $beforeValidation, $condition];
        return $edit->callRoute($this->controllerConfig->setValues($keys, $values));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_condition_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, Condition $condition): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$condition]));
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Email;
use App\Interface\ControllerConfigFactoryInterface;
use App\Service\Logic\EmailAfterValidationService;
use App\Service\Logic\EmailBeforeValidationService;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Interface\ShowInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/email')]
final readonly class EmailController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = [
            ['id' => 'name'],
            ['id' => 'name', 'name' => 'emailType', 'lowerName' => 'email_type', 'object' => true],
            ['id' => 'name', 'name' => 'dataField', 'multiples' => ['column', 'placeholder'], 'noIndex' => true],
            ['id' => 'name', 'name' => 'operator', 'object' => true, 'noIndex' => true],
            ['id' => 'value', 'noIndex' => true],
            ['id' => 'subject'],
            ['id' => 'text', 'noIndex' => true]
        ];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{page<\d+>}', name: 'app_email_index', methods: ['GET'])]
    public function index(IndexInterface $index, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page'], [$page]));
    }

    #[Route('/{id<\d+>}/show', name: 'app_email_show', methods: ['GET'])]
    public function show(ShowInterface $show, Email $email): Response
    {
        return $show->callRoute($this->controllerConfig->setValues(['entity'], [$email]));
    }

    #[Route('/create', name: 'app_email_create', methods: ['GET', 'POST'])]
    public function create(
        CreateInterface $create,
        EmailAfterValidationService $afterValidation,
        EmailBeforeValidationService $beforeValidation
    ): Response {
        $values = [$afterValidation, $beforeValidation];
        return $create->callRoute($this->controllerConfig->setValues(['afterValidation', 'beforeValidation'], $values));
    }

    #[Route('/{id<\d+>}/edit', name: 'app_email_edit', methods: ['GET', 'POST'])]
    public function edit(
        EditInterface $edit,
        Email $email,
        EmailAfterValidationService $afterValidation,
        EmailBeforeValidationService $beforeValidation
    ): Response {
        $keys = ['afterValidation', 'beforeValidation', 'entity'];
        $values = [$afterValidation, $beforeValidation, $email];
        return $edit->callRoute($this->controllerConfig->setValues($keys, $values));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_email_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, Email $email): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$email]));
    }
}

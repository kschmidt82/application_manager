<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Event;
use App\Interface\ControllerConfigFactoryInterface;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/event')]
final readonly class EventController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = [['id' => 'name']];
        $this->renderParameters = ['noShowLink' => true];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{page<\d+>}', name: 'app_event_index', methods: ['GET'])]
    public function index(IndexInterface $index, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page'], [$page]));
    }

    #[Route('/create', name: 'app_event_create', methods: ['GET', 'POST'])]
    public function create(CreateInterface $create): Response
    {
        return $create->callRoute($this->controllerConfig->setValues());
    }

    #[Route('/{id<\d+>}/edit', name: 'app_event_edit', methods: ['GET', 'POST'])]
    public function edit(EditInterface $edit, Event $event): Response
    {
        return $edit->callRoute($this->controllerConfig->setValues(['entity'], [$event]));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_event_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, Event $event): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$event]));
    }
}

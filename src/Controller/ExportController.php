<?php

declare(strict_types=1);

namespace App\Controller;

use App\Enum\FormUse;
use App\Interface\ControllerConfigFactoryInterface;
use App\Interface\OtherInterface;
use App\Service\Logic\ExportLogicService;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final readonly class ExportController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->renderParameters = ['noList' => true];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/export', name: 'app_export_index', methods: ['GET', 'POST'])]
    public function export(OtherInterface $other, ExportLogicService $logic): Response
    {
        $values = ['export', FormUse::BEFORE, $logic];
        return $other->callRoute($this->controllerConfig->setValues(['action', 'formUse', 'logic'], $values));
    }
}

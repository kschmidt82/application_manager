<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Gender;
use App\Interface\ControllerConfigFactoryInterface;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/gender')]
final readonly class GenderController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = [['id' => 'name']];
        $this->renderParameters = ['noShowLink' => true];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{page<\d+>}', name: 'app_gender_index', methods: ['GET'])]
    public function index(IndexInterface $index, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page'], [$page]));
    }

    #[Route('/create', name: 'app_gender_create', methods: ['GET', 'POST'])]
    public function create(CreateInterface $create): Response
    {
        return $create->callRoute($this->controllerConfig->setValues());
    }

    #[Route('/{id<\d+>}/edit', name: 'app_gender_edit', methods: ['GET', 'POST'])]
    public function edit(EditInterface $edit, Gender $gender): Response
    {
        return $edit->callRoute($this->controllerConfig->setValues(['entity'], [$gender]));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_gender_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, Gender $gender): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$gender]));
    }
}

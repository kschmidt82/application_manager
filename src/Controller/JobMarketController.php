<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\JobMarket;
use App\Interface\ControllerConfigFactoryInterface;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Interface\ShowInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/job_market')]
final readonly class JobMarketController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = [['id' => 'name'], ['id' => 'link', 'type' => 'link']];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{page<\d+>}', name: 'app_job_market_index', methods: ['GET'])]
    public function index(IndexInterface $index, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page'], [$page]));
    }

    #[Route('/{id<\d+>}/show', name: 'app_job_market_show', methods: ['GET'])]
    public function show(ShowInterface $show, JobMarket $jobMarket): Response
    {
        return $show->callRoute($this->controllerConfig->setValues(['entity'], [$jobMarket]));
    }

    #[Route('/create', name: 'app_job_market_create', methods: ['GET', 'POST'])]
    public function create(CreateInterface $create): Response
    {
        return $create->callRoute($this->controllerConfig->setValues());
    }

    #[Route('/{id<\d+>}/edit', name: 'app_job_market_edit', methods: ['GET', 'POST'])]
    public function edit(EditInterface $edit, JobMarket $jobMarket): Response
    {
        return $edit->callRoute($this->controllerConfig->setValues(['entity'], [$jobMarket]));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_job_market_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, JobMarket $jobMarket): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$jobMarket]));
    }
}

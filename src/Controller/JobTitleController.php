<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\JobTitle;
use App\Interface\ControllerConfigFactoryInterface;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/job_title')]
final readonly class JobTitleController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = [['id' => 'name']];
        $this->renderParameters = ['noShowLink' => true];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{page<\d+>}', name: 'app_job_title_index', methods: ['GET'])]
    public function index(IndexInterface $index, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page'], [$page]));
    }

    #[Route('/create', name: 'app_job_title_create', methods: ['GET', 'POST'])]
    public function create(CreateInterface $create): Response
    {
        return $create->callRoute($this->controllerConfig->setValues());
    }

    #[Route('/{id<\d+>}/edit', name: 'app_job_title_edit', methods: ['GET', 'POST'])]
    public function edit(EditInterface $edit, JobTitle $jobTitle): Response
    {
        return $edit->callRoute($this->controllerConfig->setValues(['entity'], [$jobTitle]));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_job_title_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, JobTitle $jobTitle): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$jobTitle]));
    }
}

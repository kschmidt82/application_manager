<?php

declare(strict_types=1);

namespace App\Controller;

use App\Enum\FormUse;
use App\Interface\ControllerConfigFactoryInterface;
use App\Interface\OtherInterface;
use App\Service\Logic\LoginLogicService;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final readonly class LoginController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->renderParameters = ['resetPassword' => true];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/', name: 'app_login', methods: ['GET', 'POST'])]
    public function login(OtherInterface $other, LoginLogicService $logic): Response
    {
        $keys = ['action', 'formUse', 'logic', 'login'];
        return $other->callRoute($this->controllerConfig->setValues($keys, ['login', FormUse::AFTER, $logic, false]));
    }
}

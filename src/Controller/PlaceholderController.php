<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Placeholder;
use App\Interface\ControllerConfigFactoryInterface;
use App\Service\Logic\PlaceholderAfterValidationService;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/placeholder')]
final readonly class PlaceholderController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = [['id' => 'name']];
        $this->renderParameters = ['subName' => 'result', 'subLogo' => 'bi-asterisk', 'noShowLink' => true];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{page<\d+>}', name: 'app_placeholder_index', methods: ['GET'])]
    public function index(IndexInterface $index, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page'], [$page]));
    }

    #[Route('/create', name: 'app_placeholder_create', methods: ['GET', 'POST'])]
    public function create(CreateInterface $create, PlaceholderAfterValidationService $afterValidation): Response
    {
        return $create->callRoute($this->controllerConfig->setValues(['afterValidation'], [$afterValidation]));
    }

    #[Route('/{id<\d+>}/edit', name: 'app_placeholder_edit', methods: ['GET', 'POST'])]
    public function edit(
        EditInterface $edit,
        Placeholder $placeholder,
        PlaceholderAfterValidationService $afterValidation
    ): Response {
        $values = [$afterValidation, $placeholder];
        return $edit->callRoute($this->controllerConfig->setValues(['afterValidation', 'entity'], $values));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_placeholder_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, Placeholder $placeholder): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$placeholder]));
    }
}

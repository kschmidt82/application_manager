<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\RecruiterContact;
use App\Interface\ControllerConfigFactoryInterface;
use App\Trait\ContactColumnTrait;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Interface\ShowInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/recruiter_contact')]
final class RecruiterContactController
{
    use ContactColumnTrait;
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = $this->columns;
        $this->entityNameTop = 'Recruiter';
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{recruiter_id<\d+>}/{page<\d+>}', name: 'app_recruiter_contact_index', methods: ['GET'])]
    public function index(IndexInterface $index, int $recruiter_id, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page', 'value'], [$page, $recruiter_id]));
    }

    #[Route('/{id<\d+>}/show', name: 'app_recruiter_contact_show', methods: ['GET'])]
    public function show(ShowInterface $show, RecruiterContact $recruiterContact): Response
    {
        return $show->callRoute($this->controllerConfig->setValues(['entity'], [$recruiterContact]));
    }

    #[Route('/{recruiter_id<\d+>}/create', name: 'app_recruiter_contact_create', methods: ['GET', 'POST'])]
    public function create(CreateInterface $create, int $recruiter_id): Response
    {
        return $create->callRoute($this->controllerConfig->setValues(['value'], [$recruiter_id]));
    }

    #[Route('/{id<\d+>}/edit', name: 'app_recruiter_contact_edit', methods: ['GET', 'POST'])]
    public function edit(EditInterface $edit, RecruiterContact $recruiterContact): Response
    {
        return $edit->callRoute($this->controllerConfig->setValues(['entity'], [$recruiterContact]));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_recruiter_contact_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, RecruiterContact $recruiterContact): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$recruiterContact]));
    }
}

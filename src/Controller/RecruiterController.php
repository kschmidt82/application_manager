<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Recruiter;
use App\Interface\ControllerConfigFactoryInterface;
use App\Trait\CompanyRecruiterColumnTrait;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Interface\ShowInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/recruiter')]
final class RecruiterController
{
    use CompanyRecruiterColumnTrait;
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = $this->columns;
        $this->renderParameters = ['subName' => 'recruiter_contact'];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{page<\d+>}', name: 'app_recruiter_index', methods: ['GET', 'POST'])]
    public function index(IndexInterface $index, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page'], [$page]));
    }

    #[Route('/{id<\d+>}/show', name: 'app_recruiter_show', methods: ['GET'])]
    public function show(ShowInterface $show, Recruiter $recruiter): Response
    {
        return $show->callRoute($this->controllerConfig->setValues(['entity'], [$recruiter]));
    }

    #[Route('/create', name: 'app_recruiter_create', methods: ['GET', 'POST'])]
    public function create(CreateInterface $create): Response
    {
        return $create->callRoute($this->controllerConfig->setValues());
    }

    #[Route('/{id<\d+>}/edit', name: 'app_recruiter_edit', methods: ['GET', 'POST'])]
    public function edit(EditInterface $edit, Recruiter $recruiter): Response
    {
        return $edit->callRoute($this->controllerConfig->setValues(['entity'], [$recruiter]));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_recruiter_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, Recruiter $recruiter): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$recruiter]));
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Enum\FormUse;
use App\Interface\ControllerConfigFactoryInterface;
use App\Interface\OtherInterface as Other;
use App\Service\Logic\ResetPasswordRequestEmailLogicService;
use App\Service\Logic\ResetPasswordRequestRequestLogicService;
use App\Service\Logic\ResetPasswordRequestResetLogicService;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/reset-password')]
final readonly class ResetPasswordRequestController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->renderParameters = ['noList' => true];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('', name: 'app_forgot_password_request', methods: ['GET', 'POST'])]
    public function request(Other $other, ResetPasswordRequestRequestLogicService $logic): Response
    {
        $values = ['request', FormUse::BEFORE, $logic, false];
        return $other->callRoute($this->controllerConfig->setValues(['action', 'formUse', 'logic', 'login'], $values));
    }

    #[Route('/check-email', name: 'app_check_email', methods: ['GET'])]
    public function checkEmail(Other $other, ResetPasswordRequestEmailLogicService $logic): Response
    {
        $values = [$logic, false, 'reset_password_request/check_email'];
        return $other->callRoute($this->controllerConfig->setValues(['logic', 'login', 'template'], $values));
    }

    #[Route('/reset/{token}', name: 'app_reset_password', methods: ['GET', 'POST'])]
    public function reset(Other $other, ResetPasswordRequestResetLogicService $logic, string $token = ''): Response
    {
        $values = ['request', $logic, false, $token];
        return $other->callRoute($this->controllerConfig->setValues(['action', 'logic', 'login', 'value'], $values));
    }
}

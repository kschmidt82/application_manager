<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Result;
use App\Interface\ControllerConfigFactoryInterface;
use App\Interface\CreateInterface;
use App\Interface\DeleteInterface;
use App\Interface\EditInterface;
use App\Interface\IndexInterface;
use App\Interface\ShowInterface;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/result')]
final readonly class ResultController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->dataColumns = [['id' => 'result'], ['id' => 'priority']];
        $this->entityNameTop = 'Placeholder';
        $this->renderParameters = ['subName' => 'condition', 'subLogo' => 'bi-asterisk'];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/{placeholder_id<\d+>}/{page<\d+>}', name: 'app_result_index', methods: ['GET'])]
    public function index(IndexInterface $index, int $placeholder_id, int $page = 1): Response
    {
        return $index->callRoute($this->controllerConfig->setValues(['page', 'value'], [$page, $placeholder_id]));
    }

    #[Route('/{id<\d+>}/show', name: 'app_result_show', methods: ['GET'])]
    public function show(ShowInterface $show, Result $result): Response
    {
        return $show->callRoute($this->controllerConfig->setValues(['entity'], [$result]));
    }

    #[Route('/{placeholder_id<\d+>}/create', name: 'app_result_create', methods: ['GET', 'POST'])]
    public function create(CreateInterface $create, int $placeholder_id): Response
    {
        return $create->callRoute($this->controllerConfig->setValues(['value'], [$placeholder_id]));
    }

    #[Route('/{id<\d+>}/edit', name: 'app_result_edit', methods: ['GET', 'POST'])]
    public function edit(EditInterface $edit, Result $result): Response
    {
        return $edit->callRoute($this->controllerConfig->setValues(['entity'], [$result]));
    }

    #[Route('/{id<\d+>}/delete', name: 'app_result_delete', methods: ['POST'])]
    public function delete(DeleteInterface $delete, Result $result): Response
    {
        return $delete->callRoute($this->controllerConfig->setValues(['entity'], [$result]));
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Interface\ControllerConfigFactoryInterface;
use App\Interface\CreateInterface;
use App\Interface\OtherInterface;
use App\Service\Logic\UserCreateAfterValidationService;
use App\Service\Logic\UserEditAfterValidationService;
use App\Service\Logic\UserEmailLogicService;
use App\Service\Logic\UserVerifyEmailLogicService;
use App\Interface\EditInterface as Edit;
use App\Trait\ControllerTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/user')]
final readonly class UserController
{
    use ControllerTrait;

    public function __construct(ControllerConfigFactoryInterface $controllerConfigFactory)
    {
        $this->renderParameters = ['noList' => true, 'noDelete' => true, 'text' => 'user.edit.changePassword'];
        $this->controllerConfig = $controllerConfigFactory->create($this);
    }

    #[Route('/register', name: 'app_user_register', methods: ['GET', 'POST'])]
    public function register(CreateInterface $create, UserCreateAfterValidationService $afterValidation): Response
    {
        $keys = ['afterValidation', 'message', 'removeFormElements', 'route', 'userId'];
        $values = [$afterValidation, 'user.create.successful', ['old_password', 'new_password'], 'app_login', 1];
        return $create->callRoute($this->controllerConfig->setValues($keys, $values));
    }

    #[Route('/{id<\d+>}/edit', name: 'app_user_edit', methods: ['GET', 'POST'])]
    public function edit(Edit $edit, User $user, UserEditAfterValidationService $afterValidation, int $id): Response
    {
        $params = array_merge($this->renderParameters, ['parameters' => ['id' => $id]]);
        $keys = ['entity', 'afterValidation', 'removeFormElements', 'renderParameters', 'route'];
        $values = [$user, $afterValidation, ['username', 'email_address', 'repeat_password'], $params, 'app_user_edit'];
        return $edit->callRoute($this->controllerConfig->setValues($keys, $values));
    }

    #[Route('/email/{id<\d+>}/{name}/{email}', name: 'app_user_email', methods: ['GET'])]
    public function email(OtherInterface $other, UserEmailLogicService $logic): Response
    {
        return $other->callRoute($this->controllerConfig->setValues(['logic', 'login'], [$logic, false]));
    }

    #[Route('/verify/email', name: 'app_user_verify_email', methods: ['GET'])]
    public function verifyUserEmail(OtherInterface $other, UserVerifyEmailLogicService $logic): Response
    {
        $values = ['verify', $logic, false];
        return $other->callRoute($this->controllerConfig->setValues(['action', 'logic', 'login'], $values));
    }
}

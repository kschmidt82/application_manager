<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ApplicationRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table('applications')]
#[ORM\Entity(ApplicationRepository::class)]
#[ORM\Index(['company_id'], name: 'idx_company_id')]
#[ORM\Index(['company_contact_id'], name: 'idx_company_contact_id')]
#[ORM\Index(['job_market_id'], name: 'idx_job_market_id')]
#[ORM\Index(['job_title_id'], name: 'idx_job_title_id')]
#[ORM\Index(['recruiter_id'], name: 'idx_recruiter_id')]
#[ORM\Index(['recruiter_contact_id'], name: 'idx_recruiter_contact_id')]
#[ORM\Index(['user_id'], name: 'idx_user_id')]
class Application
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::SMALLINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'applications')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'applications')]
    private ?Company $company = null;

    #[ORM\ManyToOne(inversedBy: 'applications')]
    private ?CompanyContact $company_contact = null;

    #[ORM\ManyToOne(inversedBy: 'applications')]
    private ?Recruiter $recruiter = null;

    #[ORM\ManyToOne(inversedBy: 'applications')]
    private ?RecruiterContact $recruiter_contact = null;

    #[ORM\ManyToOne(inversedBy: 'applications')]
    #[ORM\JoinColumn(nullable: false)]
    private ?JobTitle $job_title = null;

    #[ORM\ManyToOne(inversedBy: 'applications')]
    private ?JobMarket $job_market = null;

    #[ORM\ManyToOne(inversedBy: 'applications')]
    private ?Archive $archive = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $reference_number = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $job_description_link = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $application_portal_link = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $no_automatic_emails = false;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?DateTimeInterface $created_at = null;

    #[ORM\OneToMany(mappedBy: 'application', targetEntity: ApplicationEvent::class, orphanRemoval: true)]
    private Collection $application_events;

    public function __construct()
    {
        $this->application_events = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getCompanyContact(): ?CompanyContact
    {
        return $this->company_contact;
    }

    public function setCompanyContact(?CompanyContact $company_contact): self
    {
        $this->company_contact = $company_contact;

        return $this;
    }

    public function getRecruiter(): ?Recruiter
    {
        return $this->recruiter;
    }

    public function setRecruiter(?Recruiter $recruiter): self
    {
        $this->recruiter = $recruiter;

        return $this;
    }

    public function getRecruiterContact(): ?RecruiterContact
    {
        return $this->recruiter_contact;
    }

    public function setRecruiterContact(?RecruiterContact $recruiter_contact): self
    {
        $this->recruiter_contact = $recruiter_contact;

        return $this;
    }

    public function getJobTitle(): ?JobTitle
    {
        return $this->job_title;
    }

    public function setJobTitle(?JobTitle $job_title): self
    {
        $this->job_title = $job_title;

        return $this;
    }

    public function getJobMarket(): ?JobMarket
    {
        return $this->job_market;
    }

    public function setJobMarket(?JobMarket $job_market): self
    {
        $this->job_market = $job_market;

        return $this;
    }

    public function getArchive(): ?Archive
    {
        return $this->archive;
    }

    public function setArchive(?Archive $archive): self
    {
        $this->archive = $archive;

        return $this;
    }

    public function getReferenceNumber(): ?string
    {
        return $this->reference_number;
    }

    public function setReferenceNumber(?string $reference_number): self
    {
        $this->reference_number = $reference_number;

        return $this;
    }

    public function getJobDescriptionLink(): ?string
    {
        return $this->job_description_link;
    }

    public function setJobDescriptionLink(?string $job_description_link): self
    {
        $this->job_description_link = $job_description_link;

        return $this;
    }

    public function getApplicationPortalLink(): ?string
    {
        return $this->application_portal_link;
    }

    public function setApplicationPortalLink(?string $application_portal_link): self
    {
        $this->application_portal_link = $application_portal_link;

        return $this;
    }

    public function getNoAutomaticEmails(): ?bool
    {
        return $this->no_automatic_emails;
    }

    public function setNoAutomaticEmails(?bool $no_automatic_emails): self
    {
        $this->no_automatic_emails = $no_automatic_emails;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getApplicationEvents(): Collection
    {
        return $this->application_events;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ColumnRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table('columns')]
#[ORM\Entity(ColumnRepository::class)]
#[ORM\Index(['table_id'], name: 'idx_table_id')]
class Column
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::SMALLINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'columns')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Table $table = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $name = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $value = null;

    #[ORM\OneToMany(mappedBy: 'column', targetEntity: DataField::class)]
    private Collection $data_fields;

    public function __construct()
    {
        $this->data_fields = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTable(): ?Table
    {
        return $this->table;
    }

    public function setTable(?Table $table): self
    {
        $this->table = $table;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDataFields(): Collection
    {
        return $this->data_fields;
    }
}

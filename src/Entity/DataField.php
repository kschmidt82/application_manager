<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\DataFieldRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Table('data_fields')]
#[ORM\Entity(DataFieldRepository::class)]
#[ORM\Index(['column_id'], name: 'idx_column_id')]
#[ORM\Index(['placeholder_id'], name: 'idx_placeholder_id')]
#[UniqueEntity(['column', 'placeholder'], repositoryMethod: 'getResults', ignoreNull: false)]
class DataField
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::SMALLINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'data_fields')]
    private ?Column $column = null;

    #[ORM\ManyToOne(inversedBy: 'data_fields')]
    private ?Placeholder $placeholder = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?DateTimeInterface $created_at = null;

    #[ORM\OneToMany(mappedBy: 'data_field', targetEntity: Condition::class, orphanRemoval: true)]
    private Collection $conditions;

    #[ORM\OneToMany(mappedBy: 'data_field', targetEntity: Email::class, orphanRemoval: true)]
    private Collection $emails;

    public function __construct()
    {
        $this->conditions = new ArrayCollection();
        $this->emails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColumn(): ?Column
    {
        return $this->column;
    }

    public function setColumn(?Column $column): self
    {
        $this->column = $column;

        return $this;
    }

    public function getPlaceholder(): ?Placeholder
    {
        return $this->placeholder;
    }

    public function setPlaceholder(?Placeholder $placeholder): self
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getConditions(): Collection
    {
        return $this->conditions;
    }

    public function getEmails(): Collection
    {
        return $this->emails;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\EmailRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Table('emails')]
#[ORM\Entity(EmailRepository::class)]
#[ORM\Index(['user_id'], name: 'idx_user_id')]
#[ORM\Index(['email_type_id'], name: 'idx_email_type_id')]
#[ORM\Index(['data_field_id'], name: 'idx_data_field_id')]
#[ORM\Index(['operator_id'], name: 'idx_operator_id')]
#[UniqueEntity(
    ['user', 'email_type', 'data_field', 'operator', 'value'],
    repositoryMethod: 'getResults',
    ignoreNull: false
)]
#[UniqueEntity(['user', 'name'], repositoryMethod: 'getResults', ignoreNull: false)]
class Email
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::SMALLINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'emails')]
    #[ORM\JoinColumn(nullable: false, options: ['unsigned' => true])]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'emails')]
    #[ORM\JoinColumn(nullable: false)]
    private ?EmailType $email_type = null;

    #[ORM\ManyToOne(inversedBy: 'emails')]
    #[ORM\JoinColumn(nullable: false)]
    private ?DataField $data_field = null;

    #[ORM\ManyToOne(inversedBy: 'emails')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Operator $operator = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $value = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true, options: ['unsigned' => true])]
    private ?int $priority = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $name = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $subject = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $text = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?DateTimeInterface $created_at = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getEmailType(): ?EmailType
    {
        return $this->email_type;
    }

    public function setEmailType(?EmailType $email_type): self
    {
        $this->email_type = $email_type;

        return $this;
    }

    public function getDataField(): ?DataField
    {
        return $this->data_field;
    }

    public function setDataField(?DataField $data_field): self
    {
        $this->data_field = $data_field;

        return $this;
    }

    public function getOperator(): ?Operator
    {
        return $this->operator;
    }

    public function setOperator(?Operator $operator): self
    {
        $this->operator = $operator;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(?int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}

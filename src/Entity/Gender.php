<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\GenderRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Table('genders')]
#[ORM\Entity(GenderRepository::class)]
#[ORM\Index(['user_id'], name: 'idx_user_id')]
#[UniqueEntity(['user', 'name'], repositoryMethod: 'getResults', ignoreNull: false)]
class Gender
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::SMALLINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'genders')]
    private ?User $user = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $name = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?DateTimeInterface $created_at = null;

    #[ORM\OneToMany(mappedBy: 'gender', targetEntity: CompanyContact::class)]
    private Collection $company_contacts;

    #[ORM\OneToMany(mappedBy: 'gender', targetEntity: RecruiterContact::class)]
    private Collection $recruiter_contacts;

    public function __construct()
    {
        $this->company_contacts = new ArrayCollection();
        $this->recruiter_contacts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getCompanyContacts(): Collection
    {
        return $this->company_contacts;
    }

    public function getRecruiterContacts(): Collection
    {
        return $this->recruiter_contacts;
    }
}

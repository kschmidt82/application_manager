<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\OperatorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table('operators')]
#[ORM\Entity(OperatorRepository::class)]
class Operator
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::SMALLINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $name = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $value = null;

    #[ORM\OneToMany(mappedBy: 'operator', targetEntity: Condition::class)]
    private Collection $conditions;

    #[ORM\OneToMany(mappedBy: 'operator', targetEntity: Email::class)]
    private Collection $emails;

    public function __construct()
    {
        $this->conditions = new ArrayCollection();
        $this->emails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getConditions(): Collection
    {
        return $this->conditions;
    }

    public function getEmails(): Collection
    {
        return $this->emails;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\PlaceholderRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Table('placeholders')]
#[ORM\Entity(PlaceholderRepository::class)]
#[ORM\Index(['user_id'], name: 'idx_user_id')]
#[UniqueEntity(['user', 'name'], repositoryMethod: 'getResults', ignoreNull: false)]
class Placeholder
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::SMALLINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'placeholders')]
    private ?User $user = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $name = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?DateTimeInterface $created_at = null;

    #[ORM\OneToMany(mappedBy: 'placeholder', targetEntity: DataField::class, orphanRemoval: true)]
    private Collection $data_fields;

    #[ORM\OneToMany(mappedBy: 'placeholder', targetEntity: Result::class, orphanRemoval: true)]
    private Collection $results;

    public function __construct()
    {
        $this->data_fields = new ArrayCollection();
        $this->results = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDataFields(): Collection
    {
        return $this->data_fields;
    }

    public function getResults(): Collection
    {
        return $this->results;
    }
}

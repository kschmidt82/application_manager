<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\RecruiterRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Table('recruiters')]
#[ORM\Entity(RecruiterRepository::class)]
#[ORM\Index(['user_id'], name: 'idx_user_id')]
#[UniqueEntity(['user', 'name'], repositoryMethod: 'getResults', ignoreNull: false)]
class Recruiter
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::SMALLINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'recruiters')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $name = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $address = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $zip = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $city = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $website = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?DateTimeInterface $created_at = null;

    #[ORM\OneToMany(mappedBy: 'recruiter', targetEntity: Application::class, orphanRemoval: true)]
    private Collection $applications;

    #[ORM\OneToMany(mappedBy: 'recruiter', targetEntity: RecruiterContact::class, orphanRemoval: true)]
    private Collection $recruiter_contacts;

    public function __construct()
    {
        $this->applications = new ArrayCollection();
        $this->recruiter_contacts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getApplications(): Collection
    {
        return $this->applications;
    }

    public function getRecruiterContacts(): Collection
    {
        return $this->recruiter_contacts;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\RecruiterContactRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Table('recruiter_contacts')]
#[ORM\Entity(RecruiterContactRepository::class)]
#[ORM\Index(['gender_id'], name: 'idx_gender_id')]
#[ORM\Index(['recruiter_id'], name: 'idx_recruiter_id')]
#[ORM\Index(['title_id'], name: 'idx_title_id')]
#[UniqueEntity(['recruiter', 'email_address'], repositoryMethod: 'getResults', ignoreNull: false)]
class RecruiterContact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::SMALLINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'recruiter_contacts')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Recruiter $recruiter = null;

    #[ORM\ManyToOne(inversedBy: 'recruiter_contacts')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Gender $gender = null;

    #[ORM\ManyToOne(inversedBy: 'recruiter_contacts')]
    private ?Title $title = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $is_informal = false;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $firstname = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $lastname = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $email_address = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $phone_number = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $address = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $zip = null;

    #[ORM\Column(type: Types::STRING, length: 191, nullable: true)]
    private ?string $city = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $notes = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?DateTimeInterface $created_at = null;

    #[ORM\OneToMany(mappedBy: 'recruiter_contact', targetEntity: Application::class, orphanRemoval: true)]
    private Collection $applications;

    public function __construct()
    {
        $this->applications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecruiter(): ?Recruiter
    {
        return $this->recruiter;
    }

    public function setRecruiter(?Recruiter $recruiter): self
    {
        $this->recruiter = $recruiter;

        return $this;
    }

    public function getGender(): ?Gender
    {
        return $this->gender;
    }

    public function setGender(?Gender $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getTitle(): ?Title
    {
        return $this->title;
    }

    public function setTitle(?Title $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getIsInformal(): bool
    {
        return $this->is_informal;
    }

    public function setIsInformal(bool $is_informal): self
    {
        $this->is_informal = $is_informal;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmailAddress(): ?string
    {
        return $this->email_address;
    }

    public function setEmailAddress(string $email_address): self
    {
        $this->email_address = $email_address;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(?string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getApplications(): Collection
    {
        return $this->applications;
    }
}

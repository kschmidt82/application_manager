<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Table('users')]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity('username')]
#[UniqueEntity('email_address')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::SMALLINT, options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\Column(type: Types::STRING, length: 20)]
    private ?string $username = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $password = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $email_address = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $firstname = null;

    #[ORM\Column(type: Types::STRING, length: 191)]
    private ?string $lastname = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $notes = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $is_verified = false;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?DateTimeInterface $created_at = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Application::class, orphanRemoval: true)]
    private Collection $applications;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Company::class, orphanRemoval: true)]
    private Collection $companies;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Email::class, orphanRemoval: true)]
    private Collection $emails;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Event::class, orphanRemoval: true)]
    private Collection $events;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Gender::class, orphanRemoval: true)]
    private Collection $genders;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: JobMarket::class, orphanRemoval: true)]
    private Collection $job_markets;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: JobTitle::class, orphanRemoval: true)]
    private Collection $job_titles;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Placeholder::class, orphanRemoval: true)]
    private Collection $placeholders;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Recruiter::class, orphanRemoval: true)]
    private Collection $recruiters;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ResetPasswordRequest::class, orphanRemoval: true)]
    private Collection $reset_password_requests;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Title::class, orphanRemoval: true)]
    private Collection $titles;

    public function __construct()
    {
        $this->applications = new ArrayCollection();
        $this->companies = new ArrayCollection();
        $this->emails = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->genders = new ArrayCollection();
        $this->job_markets = new ArrayCollection();
        $this->job_titles = new ArrayCollection();
        $this->placeholders = new ArrayCollection();
        $this->recruiters = new ArrayCollection();
        $this->reset_password_requests = new ArrayCollection();
        $this->titles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmailAddress(): ?string
    {
        return $this->email_address;
    }

    public function setEmailAddress(string $email_address): self
    {
        $this->email_address = $email_address;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->is_verified;
    }

    public function setIsVerified(bool $is_verified): self
    {
        $this->is_verified = $is_verified;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    public function getUserIdentifier(): string
    {
        return (string)$this->username;
    }

    public function eraseCredentials()
    {
    }

    public function getApplications(): Collection
    {
        return $this->applications;
    }

    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    public function getEmails(): Collection
    {
        return $this->emails;
    }

    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function getGenders(): Collection
    {
        return $this->genders;
    }

    public function getJobMarkets(): Collection
    {
        return $this->job_markets;
    }

    public function getJobTitles(): Collection
    {
        return $this->job_titles;
    }

    public function getPlaceholders(): Collection
    {
        return $this->placeholders;
    }

    public function getRecruiters(): Collection
    {
        return $this->recruiters;
    }

    public function getResetPasswordRequests(): Collection
    {
        return $this->reset_password_requests;
    }

    public function getTitles(): Collection
    {
        return $this->titles;
    }
}

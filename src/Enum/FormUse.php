<?php

declare(strict_types=1);

namespace App\Enum;

enum FormUse
{
    case BEFORE;
    case AFTER;
    case NO;
}

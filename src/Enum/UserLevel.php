<?php

declare(strict_types=1);

namespace App\Enum;

enum UserLevel: int
{
    case RANDOM = 0;
    case INVALID = -1;
}

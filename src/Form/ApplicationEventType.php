<?php

declare(strict_types=1);

namespace App\Form;

use App\Trait\FormTrait;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;

final readonly class ApplicationEventType implements FormTypeInterface
{
    use FormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('event', EntityType::class, $this->field->get('event'))
            ->add('notes', TextareaType::class, $this->field->get('notes'))
            ->add('submit', SubmitType::class, $this->field->get('submit'))
        ;
    }
}

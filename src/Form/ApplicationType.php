<?php

declare(strict_types=1);

namespace App\Form;

use App\Trait\FormTrait;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;

final readonly class ApplicationType implements FormTypeInterface
{
    use FormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('company', EntityType::class, $this->field->get('company'))
            ->add('recruiter', EntityType::class, $this->field->get('recruiter'))
            ->add('company_contact', EntityType::class, $this->field->get('company_contact'))
            ->add('recruiter_contact', EntityType::class, $this->field->get('recruiter_contact'))
            ->add('type', ChoiceType::class, $this->field->get('type'))
            ->add('type_name', TextType::class, $this->field->get('type_name'))
            ->add('website', TextType::class, $this->field->get('website', false))
            ->add('gender_application', EntityType::class, $this->field->get('gender_application'))
            ->add('title', EntityType::class, $this->field->get('title', false))
            ->add('is_informal', CheckboxType::class, $this->field->get('is_informal', false))
            ->add('firstname', TextType::class, $this->field->get('firstname', false))
            ->add('lastname', TextType::class, $this->field->get('lastname', false))
            ->add('email_address_application', TextType::class, $this->field->get('email_address_application'))
            ->add('job_title_application', EntityType::class, $this->field->get('job_title_application'))
            ->add('new_job_title', TextType::class, $this->field->get('new_job_title'))
            ->add('job_title', EntityType::class, $this->field->get('job_title'))
            ->add('job_market', EntityType::class, $this->field->get('job_market'))
            ->add('reference_number', TextType::class, $this->field->get('reference_number'))
            ->add('job_description_link', TextType::class, $this->field->get('job_description_link'))
            ->add('application_portal_link', TextType::class, $this->field->get('application_portal_link'))
            ->add('no_automatic_emails', CheckboxType::class, $this->field->get('no_automatic_emails'))
            ->add('hidden', HiddenType::class, $this->field->get('hidden'))
            ->add('submit', SubmitType::class, $this->field->get('submit'))
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form;

use App\Trait\FormTrait;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;

final readonly class ChangePasswordType implements FormTypeInterface
{
    use FormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('password', RepeatedType::class, $this->field->get('password'))
            ->add('submit', SubmitType::class, $this->field->get('reset_submit'))
        ;
    }
}

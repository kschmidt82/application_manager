<?php

declare(strict_types=1);

namespace App\Form;

use App\Trait\ContactFormTrait;
use Symfony\Component\Form\FormTypeInterface;

final readonly class CompanyContactType implements FormTypeInterface
{
    use ContactFormTrait;
}

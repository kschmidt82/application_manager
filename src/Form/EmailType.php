<?php

declare(strict_types=1);

namespace App\Form;

use App\Trait\FormTrait;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;

final readonly class EmailType implements FormTypeInterface
{
    use FormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email_type', EntityType::class, $this->field->get('email_type'))
            ->add('data_field', ChoiceType::class, $this->field->get('data_field'))
            ->add('operator', EntityType::class, $this->field->get('operator'))
            ->add('value', TextType::class, $this->field->get('value'))
            ->add('priority', TextType::class, $this->field->get('priority'))
            ->add('name', TextType::class, $this->field->get('name'))
            ->add('subject', TextType::class, $this->field->get('subject'))
            ->add('text', TextareaType::class, $this->field->get('text'))
            ->add('submit', SubmitType::class, $this->field->get('submit'))
        ;
    }
}

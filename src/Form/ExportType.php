<?php

declare(strict_types=1);

namespace App\Form;

use App\Trait\FormTrait;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;

final readonly class ExportType implements FormTypeInterface
{
    use FormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date_from', DateType::class, $this->field->get('date_from'))
            ->add('date_to', DateType::class, $this->field->get('date_to'))
            ->add('columns', ChoiceType::class, $this->field->get('columns'))
            ->add('submit', SubmitType::class, $this->field->get('submit'))
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form;

use App\Interface\FormFieldOptionInterface;
use App\Trait\FormTrait;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\RequestStack;

final readonly class LoginType implements FormTypeInterface
{
    use FormTrait;

    public function __construct(private FormFieldOptionInterface $field, private RequestStack $requestStack)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('_username', TextType::class, $this->field->get('_username'))
            ->add('_password', PasswordType::class, $this->field->get('_password'))
            ->add('_target_path', HiddenType::class)
            ->add('submit', SubmitType::class, $this->field->get('login_submit'))
        ;
        $request = $this->requestStack->getCurrentRequest();
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($request) {
            if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
                $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
            } else {
                $error = $request->getSession()->get(Security::AUTHENTICATION_ERROR);
            }
            if ($error) {
                $event->getForm()->addError(new FormError($error->getMessage()));
            }
            $event->setData(
                array_replace((array)$event->getData(), [
                    'username' => $request->getSession()->get(Security::LAST_USERNAME)
                ])
            );
        });
    }

    private function getDefaults(): array
    {
        return ['csrf_field_name' => '_csrf_token', 'csrf_token_id' => 'authenticate'];
    }
}

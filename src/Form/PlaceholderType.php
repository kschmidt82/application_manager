<?php

declare(strict_types=1);

namespace App\Form;

use App\Trait\StandardFormTrait;
use Symfony\Component\Form\FormTypeInterface;

final readonly class PlaceholderType implements FormTypeInterface
{
    use StandardFormTrait;
}

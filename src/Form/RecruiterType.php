<?php

declare(strict_types=1);

namespace App\Form;

use App\Trait\CompanyRecruiterFormTrait;
use Symfony\Component\Form\FormTypeInterface;

final readonly class RecruiterType implements FormTypeInterface
{
    use CompanyRecruiterFormTrait;
}

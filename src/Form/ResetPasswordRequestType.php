<?php

declare(strict_types=1);

namespace App\Form;

use App\Trait\FormTrait;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;

final readonly class ResetPasswordRequestType implements FormTypeInterface
{
    use FormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email_address', EmailType::class, $this->field->get('email_address'))
            ->add('submit', SubmitType::class, $this->field->get('reset_submit'))
        ;
    }

    private function getDefaults(): array
    {
        return ['data_class' => null];
    }
}

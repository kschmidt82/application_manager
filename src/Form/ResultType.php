<?php

declare(strict_types=1);

namespace App\Form;

use App\Trait\FormTrait;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;

final readonly class ResultType implements FormTypeInterface
{
    use FormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('result', TextareaType::class, $this->field->get('result'))
            ->add('priority', TextType::class, $this->field->get('priority'))
            ->add('submit', SubmitType::class, $this->field->get('submit'))
        ;
    }
}

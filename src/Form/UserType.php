<?php

declare(strict_types=1);

namespace App\Form;

use App\Trait\FormTrait;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;

final readonly class UserType implements FormTypeInterface
{
    use FormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class, $this->field->get('username'))
            ->add('repeat_password', RepeatedType::class, $this->field->get('repeat_password'))
            ->add('old_password', PasswordType::class, $this->field->get('old_password'))
            ->add('new_password', RepeatedType::class, $this->field->get('new_password'))
            ->add('email_address', EmailType::class, $this->field->get('user_email_address'))
            ->add('firstname', TextType::class, $this->field->get('user_firstname'))
            ->add('lastname', TextType::class, $this->field->get('user_lastname'))
            ->add('notes', TextareaType::class, $this->field->get('notes'))
            ->add('submit', SubmitType::class, $this->field->get('submit'))
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Interface;

interface AccessInterface
{
    public function check(object $controller, string $method): void;
}

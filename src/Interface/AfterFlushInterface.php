<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Component\Form\FormInterface;

interface AfterFlushInterface
{
    public function modify(RouteConfigInterface $route, FormInterface $form): void;
}

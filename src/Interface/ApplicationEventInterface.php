<?php

declare(strict_types=1);

namespace App\Interface;

use App\Entity\Application;
use App\Entity\Event;

interface ApplicationEventInterface
{
    public function create(Application $application, Event $event, string $notes = ''): void;
}

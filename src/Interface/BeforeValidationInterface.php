<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Component\Form\FormInterface;

interface BeforeValidationInterface
{
    public function modify(RouteConfigInterface $route, FormInterface $form): array;
}

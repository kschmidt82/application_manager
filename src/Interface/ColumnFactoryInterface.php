<?php

declare(strict_types=1);

namespace App\Interface;

use App\Entity\Application;

interface ColumnFactoryInterface
{
    public function create(Application $application): ColumnInterface;
}

<?php

declare(strict_types=1);

namespace App\Interface;

interface ColumnInterface
{
    public function getReplacementFromObject(object $data): string;

    public function getReplacement(string $placeholder): string;

    public function getResult(string $table, string $column): string;
}

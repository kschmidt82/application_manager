<?php

declare(strict_types=1);

namespace App\Interface;

interface ConditionInterface
{
    public function isCorrect(object $data, string $replacement): bool;
}

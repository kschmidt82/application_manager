<?php

declare(strict_types=1);

namespace App\Interface;

interface ControllerConfigFactoryInterface
{
    public function create(object $controller): ControllerConfigInterface;
}

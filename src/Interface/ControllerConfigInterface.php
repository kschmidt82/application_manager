<?php

declare(strict_types=1);

namespace App\Interface;

use App\Service\Variable\ControllerConfigService;

interface ControllerConfigInterface
{
    public function setValues(array $keys = [], array $values = []): ControllerConfigService;
}

<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Component\HttpFoundation\Response;

interface CreateInterface
{
    public function callRoute(ControllerConfigInterface $controllerConfig): Response;
}

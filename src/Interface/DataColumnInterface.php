<?php

declare(strict_types=1);

namespace App\Interface;

interface DataColumnInterface
{
    public function getName(array $column): string;
}

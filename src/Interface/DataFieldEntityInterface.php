<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Component\Form\FormInterface;

interface DataFieldEntityInterface
{
    public function set(RouteConfigInterface $route, FormInterface $form): object;
}

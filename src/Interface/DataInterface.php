<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Component\Form\FormInterface;

interface DataInterface
{
    public function flush(RouteConfigInterface $route, string $method = 'persist'): void;

    public function getRedirectParameters(RouteConfigInterface $route, FormInterface $form): array;
}

<?php

declare(strict_types=1);

namespace App\Interface;

use DateTimeImmutable;

interface DateInterface
{
    public function getModified(): DateTimeImmutable;

    public function addOneDay(string $dateTime): string;
}

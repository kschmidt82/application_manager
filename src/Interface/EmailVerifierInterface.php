<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Bridge\Twig\Mime\TemplatedEmail as Email;
use Symfony\Component\Security\Core\User\UserInterface;

interface EmailVerifierInterface
{
    public function sendEmailConfirmation(string $verifyEmailRouteName, UserInterface $user, Email $email): void;

    public function handleEmailConfirmation(RouteConfigInterface $route, UserInterface $user): void;
}

<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Component\Form\FormInterface;

interface FilterFormInterface
{
    public function get(RouteConfigInterface $route): FormInterface;
}

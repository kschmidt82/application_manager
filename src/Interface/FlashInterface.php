<?php

declare(strict_types=1);

namespace App\Interface;

interface FlashInterface
{
    public function add(RouteConfigInterface $route, string $message, string $type = 'success'): void;
}

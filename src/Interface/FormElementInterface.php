<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Component\Form\FormInterface;

interface FormElementInterface
{
    public function remove(FormInterface $form, array $elements): FormInterface;
}

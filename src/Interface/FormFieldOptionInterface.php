<?php

declare(strict_types=1);

namespace App\Interface;

interface FormFieldOptionInterface
{
    public function get(string $key): mixed;
}

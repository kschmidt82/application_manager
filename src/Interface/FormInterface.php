<?php

declare(strict_types=1);

namespace App\Interface;

interface FormInterface
{
    public function get(RouteConfigInterface $route): \Symfony\Component\Form\FormInterface;
}

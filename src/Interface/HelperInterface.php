<?php

declare(strict_types=1);

namespace App\Interface;

interface HelperInterface
{
    public function getLowerName(string $name): string;

    public function getFullEntityName(string $entity): string;

    public function getFullFormName(string $entity): string;

    public function getShortName(string $name, string $search = 'Controller'): string;

    public function getPascalCase(string $string): string;
}

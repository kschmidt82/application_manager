<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;

interface LogicInterface
{
    public function modify(RouteConfigInterface $route, ?FormInterface $form = null): array|Response;
}

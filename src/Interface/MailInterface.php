<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Bridge\Twig\Mime\TemplatedEmail as Email;
use Symfony\Component\Security\Core\User\UserInterface;

interface MailInterface
{
    public function generate(UserInterface $user, string $subject, string $template, array $context = []): Email;

    public function send(UserInterface $user, string $subject, string $template, array $context = []): void;

    public function sendApplicationMail(string $recipient, string $subject, string $text): void;
}

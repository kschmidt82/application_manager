<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Component\HttpFoundation\Response;

interface OtherInterface
{
    public function callRoute(ControllerConfigInterface $controllerConfig): Response;
}

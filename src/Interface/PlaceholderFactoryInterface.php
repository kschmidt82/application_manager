<?php

declare(strict_types=1);

namespace App\Interface;

interface PlaceholderFactoryInterface
{
    public function create(ColumnInterface $column): PlaceholderInterface;
}

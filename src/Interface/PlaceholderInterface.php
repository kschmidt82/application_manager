<?php

declare(strict_types=1);

namespace App\Interface;

interface PlaceholderInterface
{
    public function getReplacementFromObject(object $data): string;

    public function getReplacement(string $placeholderName): string;
}

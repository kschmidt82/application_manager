<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Component\Form\FormInterface;

interface QueryFilterInterface
{
    public function getParameters(RouteConfigInterface $route, FormInterface $form): array;
}

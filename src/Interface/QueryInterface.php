<?php

declare(strict_types=1);

namespace App\Interface;

use Pagerfanta\PagerfantaInterface;

interface QueryInterface
{
    public function getRows(object $route): PagerfantaInterface;
}

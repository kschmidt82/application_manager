<?php

declare(strict_types=1);

namespace App\Interface;

interface QuerySortInterface
{
    public function getParameters(RouteConfigInterface $route): array;
}

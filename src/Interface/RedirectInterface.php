<?php

declare(strict_types=1);

namespace App\Interface;

interface RedirectInterface
{
    public function getParameters(string $route, array $renderParameters = []): array;
}

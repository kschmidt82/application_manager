<?php

declare(strict_types=1);

namespace App\Interface;

use App\Interface\RouteConfigInterface as RouteValues;
use Symfony\Component\Form\FormInterface as Form;

interface RenderInterface
{
    public function getParameters(RouteValues $route, ?Form $form = null, array $renderParameters = []): array;
}

<?php

declare(strict_types=1);

namespace App\Interface;

interface RouteConfigInterface
{
    public function set(RouteInterface $route, ControllerConfigInterface $controllerConfig, string $method): void;

    public function with(array $parameters): RouteConfigInterface;
}

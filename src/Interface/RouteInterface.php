<?php

declare(strict_types=1);

namespace App\Interface;

use App\Service\Route\RouteConfigService;
use Symfony\Component\HttpFoundation\Response;

interface RouteInterface
{
    public function getValues(string $method, ControllerConfigInterface $controllerConfig): RouteConfigService;

    public function getResponse(array $routeParameters): Response;
}

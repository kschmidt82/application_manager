<?php

declare(strict_types=1);

namespace App\Interface;

interface SearchesInterface
{
    public function get(string $text): array;
}

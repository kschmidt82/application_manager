<?php

declare(strict_types=1);

namespace App\Interface;

use Symfony\Component\Security\Core\User\UserInterface;

interface UserMailInterface
{
    public function sendConfirmation(UserInterface $user): void;
}

<?php

declare(strict_types=1);

namespace App\Repository;

use App\Trait\RepositoryTrait;
use DateTimeImmutable;
use Doctrine\ORM\QueryBuilder;

final class ApplicationRepository extends AbstractRepository
{
    use RepositoryTrait;

    public function findData(array $criteria = [], array $sort = []): QueryBuilder
    {
        $sort = $sort ?: [['column' => 'id', 'sort' => 'desc']];
        $qb = $this->createQueryBuilder('q')->andWhere('q.archive IS NULL');
        return $this->setQuery($qb, $criteria, $sort);
    }

    public function findByDateRange(DateTimeImmutable $dateFrom, DateTimeImmutable $dateTo): ?array
    {
        $qb = $this->createQueryBuilder('q');
        return $qb
            ->andWhere('q.created_at >= :dateFrom')
            ->andWhere('q.created_at < :dateTo')
            ->setParameters(['dateFrom' => $dateFrom, 'dateTo' => $dateTo])
            ->getQuery()
            ->getResult()
        ;
    }
}

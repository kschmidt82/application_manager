<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Column;
use App\Trait\RepositoryTrait;
use Doctrine\ORM\Query\Expr\Join;
use Exception;

final class ColumnRepository extends AbstractRepository
{
    use RepositoryTrait;

    public function findOneByTableAndColumn(string $tableName, string $columnName): ?Column
    {
        $qb = $this->createQueryBuilder('q');
        try {
            return $qb
                ->join('q.table', 't', Join::WITH, $qb->expr()->eq('t.name', ':tableName'))
                ->andWhere('q.name = :columnName')
                ->setParameters(['tableName' => $tableName, 'columnName' => $columnName])
                ->getQuery()
                ->getOneOrNullResult()
            ;
        } catch (Exception) {
            return null;
        }
    }
}

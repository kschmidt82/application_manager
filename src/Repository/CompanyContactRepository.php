<?php

declare(strict_types=1);

namespace App\Repository;

use App\Trait\RepositoryTrait;

final class CompanyContactRepository extends AbstractRepository
{
    use RepositoryTrait;
}

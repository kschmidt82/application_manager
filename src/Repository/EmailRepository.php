<?php

declare(strict_types=1);

namespace App\Repository;

use App\Trait\RepositoryTrait;
use Doctrine\ORM\Query\Expr\Join;

final class EmailRepository extends AbstractRepository
{
    use RepositoryTrait;

    public function findByEmailType(string $typeName): ?array
    {
        $qb = $this->createQueryBuilder('q');
        return $qb
            ->join('q.email_type', 'et', Join::WITH, $qb->expr()->eq('et.name', ':name'))
            ->andWhere('q.user = :user')
            ->setParameters(['name' => $typeName, 'user' => $this->security->getUser()])
            ->getQuery()
            ->getResult()
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Repository;

use App\Trait\RepositoryTrait;

final class EventRepository extends AbstractRepository
{
    use RepositoryTrait;
}

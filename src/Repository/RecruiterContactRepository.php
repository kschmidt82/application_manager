<?php

declare(strict_types=1);

namespace App\Repository;

use App\Trait\RepositoryTrait;

final class RecruiterContactRepository extends AbstractRepository
{
    use RepositoryTrait;
}

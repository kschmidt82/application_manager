<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ResetPasswordRequest;
use App\Entity\User;
use App\Trait\RepositoryTrait;
use DateTimeInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordRequestInterface;
use SymfonyCasts\Bundle\ResetPassword\Persistence\Repository\ResetPasswordRequestRepositoryTrait;
use SymfonyCasts\Bundle\ResetPassword\Persistence\ResetPasswordRequestRepositoryInterface as RPRRepository;

final class ResetPasswordRequestRepository extends AbstractRepository implements RPRRepository
{
    use RepositoryTrait;
    use ResetPasswordRequestRepositoryTrait;

    public function createResetPasswordRequest(
        object $user,
        DateTimeInterface $expiresAt,
        string $selector,
        string $hashedToken
    ): ResetPasswordRequestInterface {
        $user = ($user instanceof User) ? $user : null;
        return new ResetPasswordRequest($user, $expiresAt, $selector, $hashedToken);
    }
}

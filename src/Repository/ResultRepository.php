<?php

declare(strict_types=1);

namespace App\Repository;

use App\Trait\RepositoryTrait;
use Doctrine\ORM\Query\Expr\Join;

final class ResultRepository extends AbstractRepository
{
    use RepositoryTrait;

    public function findByPlaceholder(string $placeholderName): ?array
    {
        $qb = $this->createQueryBuilder('q');
        return $qb
            ->join('q.placeholder', 'p', Join::WITH, $qb->expr()->eq('p.name', ':name'))
            ->andWhere('p.user = :user')
            ->setParameters(['name' => $placeholderName, 'user' => $this->security->getUser()])
            ->orderBy('q.priority', 'desc')
            ->getQuery()
            ->getResult()
        ;
    }
}

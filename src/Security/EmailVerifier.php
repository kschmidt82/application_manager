<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use App\Interface\EmailVerifierInterface;
use App\Interface\FlashInterface;
use App\Interface\RouteConfigInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail as Email;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

final readonly class EmailVerifier implements EmailVerifierInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private FlashInterface $flash,
        private MailerInterface $mailer,
        private VerifyEmailHelperInterface $verifyEmailHelper
    ) {
    }

    public function sendEmailConfirmation(string $verifyEmailRouteName, UserInterface $user, Email $email): void
    {
        if ($user instanceof User) {
            $signatureComponents = $this->verifyEmailHelper->generateSignature(
                $verifyEmailRouteName,
                $user->getUsername(),
                $user->getEmailAddress(),
                ['username' => $user->getUsername()]
            );
            $context = $email->getContext();
            $context['signedUrl'] = $signatureComponents->getSignedUrl();
            $context['expiresAtMessageKey'] = $signatureComponents->getExpirationMessageKey();
            $context['expiresAtMessageData'] = $signatureComponents->getExpirationMessageData();
            $email->context($context);
            try {
                $this->mailer->send($email);
            } catch (TransportExceptionInterface) {
            }
        }
    }

    public function handleEmailConfirmation(RouteConfigInterface $route, UserInterface $user): void
    {
        if ($user instanceof User) {
            try {
                $this->verifyEmailHelper->validateEmailConfirmation(
                    $route->request->getUri(),
                    $user->getUsername(),
                    $user->getEmailAddress()
                );
                $user->setIsVerified(true);
                $this->em->persist($user);
                $this->em->flush();
                $this->flash->add($route, 'user.verify.successful');
            } catch (VerifyEmailExceptionInterface) {
                $this->flash->add($route, 'user.create.error', 'error');
            }
        }
    }
}

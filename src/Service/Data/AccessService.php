<?php

declare(strict_types=1);

namespace App\Service\Data;

use App\Entity\User;
use App\Enum\UserLevel;
use App\Interface\AccessInterface;
use App\Interface\HelperInterface as Helper;
use Doctrine\ORM\EntityManagerInterface as EntityManager;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final readonly class AccessService implements AccessInterface
{
    public function __construct(private EntityManager $em, private Helper $helper, private Security $security)
    {
    }

    public function check(object $controller, string $method): void
    {
        if (!$controller->login) {
            return;
        }
        $this->denyAccessWithWrongUserId($controller->userId);
        $userId = $this->getCorrectUserId($controller, $method);
        $this->denyAccessWithWrongUserId($userId);
        if ($userId == UserLevel::RANDOM->value && !$this->security->isGranted('IS_AUTHENTICATED')) {
            throw new AccessDeniedException();
        } elseif ($userId == UserLevel::INVALID->value) {
            throw new NotFoundHttpException();
        }
    }

    private function denyAccessWithWrongUserId(int $userId): void
    {
        $method = 'getId';
        if ($userId > 0 && $userId !== $this->security->getUser()?->$method()) {
            throw new AccessDeniedException();
        }
    }

    private function getCorrectUserId(object $controller, string $method): int
    {
        $userId = UserLevel::RANDOM->value;
        $entity = $controller->entity;
        $entityNameTop = $controller->entityNameTop;
        $entityNameTopTop = $controller->entityNameTopTop;
        if ($controller->value) {
            $entityNameFull = $this->helper->getFullEntityName($entityNameTop);
            $rows = $this->em->getRepository($entityNameFull)->findOneBy(['id' => $controller->value]);
            $userId = ($rows && $entityNameTopTop) ? $rows->{'get'.$entityNameTopTop}()?->getUser()?->getId() : $userId;
            $userId = ($rows && method_exists($rows, 'getUser')) ? $rows->getUser()?->getId() : $userId;
        } elseif ($entity) {
            if ($entityNameTop || $entityNameTopTop) {
                $object = $entity->{'get'.$entityNameTop}();
                $call = 'get'.$entityNameTopTop;
                $userId = ($entityNameTopTop) ? $object->$call()->getUser()->getId() : $object->getUser()->getId();
            } elseif (method_exists($entity, 'getUser')) {
                $userId = $entity->getUser()?->getId();
                $userId = (is_null($userId) && $method == 'show') ? UserLevel::RANDOM->value : $userId;
            } elseif ($entity instanceof User) {
                $userId = $entity->getId();
            }
        }
        $userId ??= UserLevel::INVALID->value;
        return $userId;
    }
}

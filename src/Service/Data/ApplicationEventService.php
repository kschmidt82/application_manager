<?php

declare(strict_types=1);

namespace App\Service\Data;

use App\Entity\Application;
use App\Entity\ApplicationEvent;
use App\Entity\Event;
use App\Interface\ApplicationEventInterface;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;

final readonly class ApplicationEventService implements ApplicationEventInterface
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function create(Application $application, Event $event, string $notes = ''): void
    {
        $applicationEvent = new ApplicationEvent();
        $applicationEvent
            ->setApplication($application)
            ->setEvent($event)
            ->setNotes($notes)
            ->setCreatedAt(new DateTimeImmutable())
        ;
        $this->em->persist($applicationEvent);
        $this->em->flush();
    }
}

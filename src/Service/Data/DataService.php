<?php

declare(strict_types=1);

namespace App\Service\Data;

use App\Interface\DataInterface;
use App\Interface\FlashInterface;
use App\Interface\RedirectInterface;
use App\Interface\RouteConfigInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

final readonly class DataService implements DataInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private FlashInterface $flash,
        private RedirectInterface $redirect,
        private RequestStack $request
    ) {
    }

    public function flush(RouteConfigInterface $route, string $method = 'persist'): void
    {
        $this->em->$method($route->entity);
        $this->em->flush();
        $this->flash->add($route, $route->message);
    }

    public function getRedirectParameters(RouteConfigInterface $route, FormInterface $form): array
    {
        $method = 'all';
        $flashes = $this->request->getSession()->getBag('flashes')->$method();
        if ($form->isSubmitted() && $form->isValid() && $flashes) {
            $renderParameters = $this->redirect->getParameters($route->route, $route->renderParameters);
        }
        foreach ($flashes as $key => $values) {
            foreach ($values as $value) {
                $this->request->getSession()->getFlashBag()->add($key, $value);
            }
        }
        return $renderParameters ?? [];
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Data;

use App\Interface\FlashInterface;
use App\Interface\RouteConfigInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class FlashService implements FlashInterface
{
    public function __construct(private RequestStack $request, private TranslatorInterface $translator)
    {
    }

    public function add(RouteConfigInterface $route, string $message, string $type = 'success'): void
    {
        $this->request->getSession()->getFlashBag()->add($type, $this->translator->trans($message));
    }
}

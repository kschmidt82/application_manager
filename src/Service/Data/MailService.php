<?php

declare(strict_types=1);

namespace App\Service\Data;

use App\Interface\MailInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail as Email;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final class MailService implements MailInterface
{
    private bool $isSent = true;

    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly ParameterBagInterface $parameter,
        private readonly TranslatorInterface $translator
    ) {
    }

    public function isSent(): bool
    {
        return $this->isSent;
    }

    public function sendApplicationMail(string $recipient, string $subject, string $text): void
    {
        try {
            if ($_ENV['APP_ENV'] != 'test') {
                $this->mailer->send($this->customGenerate($recipient, $subject, $text));
            }
            $this->isSent = true;
        } catch (TransportExceptionInterface) {
            $this->isSent = false;
        }
    }

    public function send(UserInterface $user, string $subject, string $template, array $context = []): void
    {
        try {
            if ($_ENV['APP_ENV'] != 'test') {
                $this->mailer->send($this->generate($user, $subject, $template, $context));
            }
            $this->isSent = true;
        } catch (TransportExceptionInterface) {
            $this->isSent = false;
        }
    }

    public function generate(UserInterface $user, string $subject, string $template, array $context = []): Email
    {
        $method = 'getEmailAddress';
        return (new Email())
            ->from(new Address($this->parameter->get('app.adminEmail'), $this->translator->trans('project.full')))
            ->to($user->$method())
            ->subject($this->translator->trans($subject))
            ->textTemplate($template)
            ->context($context)
        ;
    }

    private function customGenerate(string $recipient, string $subject, string $text): Email
    {
        return (new Email())
            ->from(new Address($this->parameter->get('app.adminEmail')))
            ->to($recipient)
            ->subject($subject)
            ->text($text)
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Data;

use App\Interface\QueryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\PagerfantaInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final readonly class QueryService implements QueryInterface
{
    public function __construct(private EntityManagerInterface $em, private ParameterBagInterface $parameterBag)
    {
    }

    public function getRows(object $route): PagerfantaInterface
    {
        $method = 'findData';
        $query = $this->em->getRepository($route->entityNameFull)->$method(array_merge($route->criteria));
        $rows = new Pagerfanta(new QueryAdapter($query));
        $rows->setMaxPerPage($this->parameterBag->get('app.maxPerPage'));
        $rows->setCurrentPage($route->page);
        return $rows;
    }
}

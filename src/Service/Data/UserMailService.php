<?php

declare(strict_types=1);

namespace App\Service\Data;

use App\Interface\EmailVerifierInterface;
use App\Interface\MailInterface;
use App\Interface\UserMailInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final readonly class UserMailService implements UserMailInterface
{
    public function __construct(private EmailVerifierInterface $emailVerifier, private MailInterface $mail)
    {
    }

    public function sendConfirmation(UserInterface $user): void
    {
        if ($_ENV['APP_ENV'] != 'test') {
            $this->emailVerifier->sendEmailConfirmation(
                'app_user_verify_email',
                $user,
                $this->mail->generate($user, 'user.verify.title', 'user/confirmation_email.html.twig')
            );
        }
    }
}

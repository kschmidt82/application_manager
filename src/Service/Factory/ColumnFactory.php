<?php

declare(strict_types=1);

namespace App\Service\Factory;

use App\Entity\Application;
use App\Interface\ColumnFactoryInterface;
use App\Interface\ColumnInterface;
use App\Interface\HelperInterface;
use App\Repository\ColumnRepository;
use App\Service\Variable\ColumnService;

final readonly class ColumnFactory implements ColumnFactoryInterface
{
    public function __construct(private ColumnRepository $column, private HelperInterface $helper)
    {
    }

    public function create(Application $application): ColumnInterface
    {
        return new ColumnService($application, $this->column, $this->helper);
    }
}

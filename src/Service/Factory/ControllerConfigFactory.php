<?php

declare(strict_types=1);

namespace App\Service\Factory;

use App\Interface\ControllerConfigFactoryInterface;
use App\Interface\ControllerConfigInterface;
use App\Service\Variable\ControllerConfigService;

final readonly class ControllerConfigFactory implements ControllerConfigFactoryInterface
{
    public function create(object $controller): ControllerConfigInterface
    {
        return new ControllerConfigService($controller);
    }
}

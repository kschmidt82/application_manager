<?php

declare(strict_types=1);

namespace App\Service\Factory;

use App\Interface\ColumnInterface;
use App\Interface\ConditionInterface;
use App\Interface\PlaceholderFactoryInterface;
use App\Interface\PlaceholderInterface;
use App\Interface\SearchesInterface;
use App\Repository\ConditionRepository;
use App\Repository\ResultRepository;
use App\Service\Variable\PlaceholderService;

final readonly class PlaceholderFactory implements PlaceholderFactoryInterface
{
    public function __construct(
        private ConditionInterface $condition,
        private ConditionRepository $conditionRepository,
        private ResultRepository $result,
        private SearchesInterface $searches
    ) {
    }

    public function create(ColumnInterface $column): PlaceholderInterface
    {
        $condition = $this->condition;
        return new PlaceholderService($column, $condition, $this->conditionRepository, $this->result, $this->searches);
    }
}

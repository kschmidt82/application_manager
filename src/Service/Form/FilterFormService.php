<?php

declare(strict_types=1);

namespace App\Service\Form;

use App\Interface\DataColumnInterface;
use App\Interface\FilterFormInterface;
use App\Interface\HelperInterface;
use App\Interface\RouteConfigInterface;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

final readonly class FilterFormService implements FilterFormInterface
{
    public function __construct(
        private DataColumnInterface $dataColumn,
        private EntityManagerInterface $em,
        private FormFactoryInterface $formFactory,
        private HelperInterface $helper,
        private RequestStack $request
    ) {
    }

    public function get(RouteConfigInterface $route): FormInterface
    {
        $options = ['validation_groups' => false, 'attr' => ['name' => 'filters', 'novalidate' => 'novalidate']];
        $form = $this->formFactory->create($route->formName, new ($route->entityNameFull)(), $options);
        $form->remove('submit');
        $session = $this->request->getSession();
        foreach ($route->dataColumns as $column) {
            $name = $this->dataColumn->getName($column);
            $value = $session->get('filters')[$route->entityNameLow][$name]['value'] ?? null;
            if (isset($column['noIndex'])) {
                $form->remove($name);
            } elseif (!empty($value)) {
                if (isset($column['type']) && $column['type'] == 'date') {
                    try {
                        $data = $session->get('filters')[$route->entityNameLow][$name.'_operator']['value'] ?? null;
                        $form->get($name.'_operator')->setData($data);
                        $value = new DateTimeImmutable($value);
                    } catch (Exception) {
                    }
                } elseif (isset($column['objects'])) {
                    $value = $this->em->getRepository($this->helper->getFullEntityName($name))->find($value->getId());
                }
                $form->get($name)->setData($value);
            }
        }
        $form->handleRequest($this->request->getCurrentRequest());
        return $form;
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Form;

use App\Interface\FormElementInterface;
use Symfony\Component\Form\FormInterface;

final readonly class FormElementService implements FormElementInterface
{
    public function remove(FormInterface $form, array $elements): FormInterface
    {
        foreach ($elements as $element) {
            if ($form->has($element)) {
                $form->remove($element);
            }
        }
        return $form;
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Form;

use App\Entity\Company;
use App\Entity\CompanyContact;
use App\Entity\EmailType;
use App\Entity\Event;
use App\Entity\Gender;
use App\Entity\JobMarket;
use App\Entity\JobTitle;
use App\Entity\Operator;
use App\Entity\Recruiter;
use App\Entity\RecruiterContact;
use App\Entity\Title;
use App\Interface\FormFieldOptionInterface;
use App\Interface\HelperInterface;
use App\Repository\ColumnRepository;
use App\Repository\PlaceholderRepository;
use App\Validator\CustomUserPassword;
use App\Validator\Password;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\PositiveOrZero;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class FormFieldOptionService implements FormFieldOptionInterface
{
    public array $options;

    public function __construct(
        private ColumnRepository $column,
        private EntityManagerInterface $em,
        private HelperInterface $helper,
        private PlaceholderRepository $placeholder,
        private TranslatorInterface $translator,
    ) {
        $options = $this->getOptionList();
        $options = $this->getTexts($options);
        $options = $this->getConstraints($options);
        $options = $this->getPasswords($options);
        $options = $this->getChoices($options);
        $options = $this->getEntities($options);
        $options = $this->getCopies($options);
        $this->options = $options;
    }

    public function get(string $key, $isMapped = true): mixed
    {
        $options = $this->options[$key];
        if (!$isMapped) {
            $options['mapped'] = false;
        }
        return $options;
    }

    private function getOptionList(): array
    {
        $dti = 'datetime_immutable';
        $ph = $this->getLabel('choose');
        return [
            'address' => ['required' => false],
            'application_portal_link' => ['required' => false],
            'city' => ['required' => false],
            'columns' => ['choices' => $this->getColumns(false), 'multiple' => true, 'mapped' => false],
            'company' => ['required' => false, 'placeholder' => $ph, 'priority' => 4],
            'company_contact' => ['required' => false, 'placeholder' => $ph, 'priority' => 3],
            'data_field' => ['choices' => $this->getDataFields(), 'mapped' => false],
            'date_from' => ['input' => $dti, 'widget' => 'single_text', 'empty_data' => null, 'mapped' => false],
            'date_to' => ['input' => $dti, 'widget' => 'single_text', 'empty_data' => null, 'mapped' => false],
            'email_address' => ['empty_data' => ''],
            'email_type' => [],
            'end_date' => ['input' => $dti, 'required' => false, 'widget' => 'single_text'],
            'event' => [],
            'firstname' => ['required' => false],
            'gender' => [],
            'hidden' => ['mapped' => false],
            'is_informal' => ['required' => false],
            'job_description_link' => ['required' => false],
            'job_market' => ['required' => false, 'placeholder' => $ph],
            'job_title' => [],
            'lastname' => ['empty_data' => '', 'required' => false],
            'link' => ['required' => false],
            'login_submit' => ['label' => $this->getLabel('login.submit')],
            'name' => ['empty_data' => ''],
            'new_job_title' => ['mapped' => false, 'required' => false],
            'new_password' => ['required' => false],
            'no_automatic_emails' => ['required' => false],
            'notes' => ['required' => false],
            'old_password' => ['empty_data' => '', 'mapped' => false, 'required' => false],
            'operator' => [],
            'password' => [],
            '_password' => ['required' => false],
            'phone_number' => ['required' => false],
            'priority' => ['required' => false],
            'recruiter' => ['required' => false, 'placeholder' => $ph, 'priority' => 2],
            'recruiter_contact' => ['required' => false, 'placeholder' => $ph, 'priority' => 1],
            'reference_number' => ['required' => false],
            'repeat_password' => [],
            'reset_submit' => ['label' => $this->getLabel('resetPasswordRequest.submit')],
            'result' => [],
            'start_date' => ['input' => $dti, 'widget' => 'single_text', 'constraints' => [new NotBlank()]],
            'subject' => ['empty_data' => ''],
            'submit' => [],
            'text' => ['empty_data' => ''],
            'title' => ['placeholder' => $ph, 'required' => false],
            'type' => ['choices' => $this->getTypes(), 'placeholder' => $ph, 'required' => false, 'mapped' => false],
            'user_email_address' => ['label' => $this->getLabel('emailAddress')],
            'user_firstname' => ['label' => $this->getLabel('firstname')],
            'user_lastname' => ['label' => $this->getLabel('lastname')],
            '_username' => ['required' => false],
            'username' => ['help' => $this->getHelpText('userUsername')],
            'value' => ['required' => false],
            'website' => ['required' => false],
            'zip' => ['required' => false],
        ];
    }

    private function getColumns(bool $hasId = true): array
    {
        $choices = [];
        $columns = $this->column->findBy([], ['table' => 'asc', 'id' => 'asc']);
        $oldTableName = null;
        foreach ($columns as $column) {
            $tableName = $column->getTable()->getName();
            if ($tableName != $oldTableName) {
                $choices[$tableName] = [];
                $oldTableName = $tableName;
            }
            $name = $column->getTable()->getName().'.'.$column->getName();
            $choices[$tableName][$column->getName()] = ($hasId) ? 'c'.$column->getId() : $name;
        }
        return $choices;
    }

    private function getLabel($name): string
    {
        return 'form.'.$name.'.label';
    }

    private function getDataFields(): array
    {
        $choices = $this->getColumns();
        $name = $this->translator->trans('placeholder.index.title');
        $choices[$name] = [];
        $placeholders = $this->placeholder->getUserResults();
        foreach ($placeholders as $placeholder) {
            $choices[$name][$placeholder->getName()] = 'p'.$placeholder->getId();
        }
        return $choices;
    }

    private function getTypes(): array
    {
        $translator = $this->translator;
        return [$translator->trans('company.index.title') => 'company', $translator->trans('recruiter.index.title') => 'recruiter'];
    }

    private function getHelpText($name): string
    {
        return 'form.'.$name.'.helpText';
    }

    private function getTexts(array $options): array
    {
        $blacklist = $this->getTextBlacklist();
        foreach ($options as $key => $option) {
            if (is_null($value = $blacklist[$key] ?? null) || $value) {
                $name = (str_starts_with($key, '_')) ? substr($key, 1) : $key;
                $name = str_replace('_operator', '', $name);
                $name = lcfirst($this->helper->getPascalCase($name));
                if ($value != 'noLabel') {
                    $options[$key]['label'] = $this->getLabel($name);
                }
                if ($value != 'noHelp' && !str_contains($key, 'submit')) {
                    $options[$key]['help'] = $this->getHelpText($name);
                }
            }
        }
        return $options;
    }

    private function getTextBlacklist(): array
    {
        return [
            'hidden' => '',
            'login_submit' => '',
            'new_password' => '',
            'password' => '',
            'repeat_password' => '',
            'reset_submit' => '',
            'user_email_address' => 'noLabel',
            'user_firstname' => 'noLabel',
            'user_lastname' => 'noLabel',
            'username' => 'noHelp'
        ];
    }

    private function getConstraints(array $options): array
    {
        $constraints = $this->getConstraintList();
        foreach ($options as $key => $option) {
            if ($value = $constraints[$key] ?? null) {
                $validator = (isset($value['validator'])) ? [$value['validator']] : [];
                $max = $value['max'] ?? 4096;
                $options[$key]['constraints'] = $this->getConstraint($value['min'] ?? 0, $max, $validator);
                $options[$key]['attr'] = ['maxlength' => $max, 'size' => $max];
            }
        }
        return $options;
    }

    private function getConstraintList(): array
    {
        return [
            'address' => ['max' => 191],
            'city' => ['max' => 191],
            'email_address' => ['min' => 1, 'max' => 191, 'validator' => new Email()],
            'firstname' => ['max' => 191],
            'lastname' => ['max' => 191],
            'link' => ['max' => 191],
            'name' => ['min' => 2, 'max' => 191],
            'new_job_title' => ['max' => 191],
            'new_password' => ['validator' => new Password()],
            'old_password' => ['validator' => new CustomUserPassword()],
            'password' => ['min' => 1, 'validator' => new Password()],
            'phone_number' => ['max' => 191],
            'priority' => ['max' => 4, 'validator' => new PositiveOrZero()],
            'repeat_password' => ['min' => 1, 'validator' => new Password()],
            'result' => ['min' => 1],
            'user_email_address' => ['min' => 1, 'max' => 191, 'validator' => new Email()],
            'user_firstname' => ['min' => 2, 'max' => 191],
            'user_lastname' => ['min' => 2, 'max' => 191],
            'username' => ['min' => 8, 'max' => 20],
            'value' => ['max' => 191],
            'zip' => ['max' => 191]
        ];
    }

    private function getConstraint(int $min, int $max, array $additions = []): array
    {
        $notBlank = ($min > 0) ? [new NotBlank()] : [];
        $length = [new Length(array_merge(['min' => $min, 'max' => $max]))];
        return array_merge($additions, $length, $notBlank);
    }

    private function getPasswords(array $options): array
    {
        $passwords = $this->getPasswordList();
        foreach ($options as $key => $option) {
            if (!isset($passwords[$key])) {
                continue;
            }
            $options[$key]['type'] = PasswordType::class;
            $options[$key]['first_options']['label'] = $this->getLabel($passwords[$key]['label1']);
            $options[$key]['first_options']['help'] = $this->getHelpText($passwords[$key]['help']);
            $options[$key]['second_options']['label'] = $this->getLabel($passwords[$key]['label2']);
            $options[$key]['second_options']['help'] = $this->getHelpText($passwords[$key]['help']);
            $options[$key]['empty_data'] = '';
            $options[$key]['mapped'] = false;
            $options[$key]['options'] = ['attr' => ['class' => 'password-field']];
        }
        return $options;
    }

    private function getPasswordList(): array
    {
        return [
            'new_password' => ['label1' => 'newPassword', 'label2' => 'newPasswordRepeat', 'help' => 'newPassword'],
            'password' => ['label1' => 'password', 'label2' => 'repeatPassword', 'help' => 'repeatPassword'],
            'repeat_password' => ['label1' => 'password', 'label2' => 'repeatPassword', 'help' => 'repeatPassword'],
        ];
    }

    private function getChoices(array $options): array
    {
        $choices = $this->getChoiceList();
        foreach ($options as $key => $option) {
            if (in_array($key, $choices)) {
                $options[$key]['mapped'] = false;
                $options[$key]['choice_translation_domain'] = false;
            }
        }
        return $options;
    }

    private function getChoiceList(): array
    {
        return ['data_field'];
    }

    private function getEntities(array $options): array
    {
        $entities = $this->getEntityList();
        $sort = [['column' => 'name', 'sort' => 'asc']];
        foreach ($options as $key => $option) {
            if ($value = $entities[$key] ?? null) {
                $method = $value['method'] ?? 'findUserData';
                $options[$key]['class'] = $value['class'];
                $repository = $this->em->getRepository($value['class']);
                $qb = $repository->$method($value['criteria'] ?? [], $value['sort'] ?? $sort);
                $options[$key]['query_builder'] = $qb;
                $options[$key]['choice_label'] = $value['label'] ?? 'name';
            }
        }
        return $options;
    }

    private function getEntityList(): array
    {
        $criteria = ['title' => 'x'];
        $sort = [['column' => 'email_address', 'sort' => 'asc']];
        $contact = ['criteria' => $criteria, 'label' => 'email_address', 'sort' => $sort, 'method' => 'findData'];
        return [
            'company' => ['class' => Company::class],
            'company_contact' => array_merge(['class' => CompanyContact::class], $contact),
            'email_type' => ['class' => EmailType::class, 'method' => 'findData'],
            'event' => ['class' => Event::class],
            'gender' => ['class' => Gender::class],
            'job_title' => ['class' => JobTitle::class],
            'job_market' => ['class' => JobMarket::class],
            'operator' => ['class' => Operator::class, 'method' => 'findData'],
            'recruiter' => ['class' => Recruiter::class],
            'recruiter_contact' => array_merge(['class' => RecruiterContact::class], $contact),
            'title' => ['class' => Title::class]
        ];
    }

    private function getCopies(array $options): array
    {
        $copies = $this->getCopyList();
        foreach ($copies as $key => $values) {
            foreach ($values as $oldKey => $newOptions) {
                $options[$key] = $options[$oldKey];
                $options[$key] = array_merge($options[$key], $newOptions);
            }
        }
        return $options;
    }

    private function getCopyList(): array
    {
        $options = ['placeholder' => $this->getLabel('choose'), 'required' => false, 'mapped' => false];
        return [
            'email_address_application' => ['email_address' => ['required' => false, 'mapped' => false, 'constraints' => []]],
            'gender_application' => ['gender' => $options],
            'job_title_application' => ['job_title' => $options],
            'type_name' => ['name' => ['required' => false, 'mapped' => false, 'constraints' => []]]
        ];
    }
}

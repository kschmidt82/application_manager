<?php

declare(strict_types=1);

namespace App\Service\Form;

use App\Interface\DataInterface;
use App\Interface\FormElementInterface;
use App\Interface\RouteConfigInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class FormService implements \App\Interface\FormInterface
{
    public array $validation;

    public function __construct(
        private DataInterface $data,
        private FormElementInterface $formElement,
        private FormFactoryInterface $formFactory,
        private RequestStack $request,
        private TranslatorInterface $translator
    ) {
    }

    public function get(RouteConfigInterface $route): FormInterface
    {
        $options = ['validation_groups' => ['Default', $route->action]];
        $form = $this->formFactory->create($route->formName, $route->entity, $options);
        $form = $this->formElement->remove($form, $route->removeFormElements);
        [$entity, $form] = $route->beforeValidation?->modify($route, $form) ?? [$route->entity, $form];
        $form->handleRequest($this->request->getCurrentRequest());
        if ($form->isSubmitted()) {
            $form = $this->getCorrectErrors($form);
            [$form, $entity] = $route->afterValidation?->modify($route, $form) ?? [$form, $entity];
            if ($form->isValid() && !$this->request->getSession()->get('invalid') && $entity) {
                $this->data->flush($route->with(['entity' => $entity]));
                $route->afterFlush?->modify($route, $form);
            }
            $this->request->getSession()->set('invalid', null);
        }
        return $form;
    }

    private function getCorrectErrors(FormInterface $form): FormInterface
    {
        $errors = $form->getErrors(true);
        $clearErrors = false;
        $method = 'clearErrors';
        foreach ($errors as $error) {
            $key1 = $error?->getOrigin()?->getName();
            $key2 = $error?->getOrigin()?->getParent()?->getName();
            $alreadyUsed = 'This value is already used.';
            if ($key1 && $key2) {
                $form->get($key2)[$key1]->$method();
                if (!$this->request->getSession()->get('invalid')) {
                    $options = 'first_options';
                    $errorText = $this->translator->trans($form->get($key2)->getConfig()->getOption($options)['help']);
                    $errorText = ($error->getMessageTemplate() == $alreadyUsed) ? $error->getMessage() : $errorText;
                    $form->get($key2)[$key1]->addError(new FormError($errorText));
                }
            } elseif ($key1) {
                $form->get($key1)->$method();
                if (!$this->request->getSession()->get('invalid')) {
                    $errorText = $this->translator->trans($form->get($key1)->getConfig()->getOption('help'));
                    $errorText = ($error->getMessageTemplate() == $alreadyUsed) ? $error->getMessage() : $errorText;
                    $form->get($key1)->addError(new FormError($errorText));
                }
            } elseif ($error->getMessageTemplate() == $alreadyUsed && !$this->request->getSession()->get('invalid')) {
                $key = $error->getCause()->getConstraint()->fields[1];
                $form->get($key)->addError(new FormError($error->getMessage()));
                $clearErrors = true;
            }
        }
        return ($clearErrors) ? $form->$method() : $form;
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Entity\Application;
use App\Interface\AfterFlushInterface;
use App\Interface\ApplicationEventInterface;
use App\Interface\RouteConfigInterface;
use App\Repository\EventRepository;
use Symfony\Component\Form\FormInterface;

final readonly class ApplicationAfterFlushService implements AfterFlushInterface
{
    public function __construct(private ApplicationEventInterface $applicationEvent, private EventRepository $event)
    {
    }

    public function modify(RouteConfigInterface $route, FormInterface $form): void
    {
        if ($form->isValid() && $route->entity instanceof Application) {
            $this->applicationEvent->create($route->entity, $this->event->find(1));
        }
    }
}

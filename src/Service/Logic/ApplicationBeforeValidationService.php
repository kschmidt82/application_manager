<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Interface\BeforeValidationInterface;
use App\Interface\HelperInterface as Helper;
use App\Interface\RouteConfigInterface;
use Doctrine\ORM\EntityManagerInterface as EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

final readonly class ApplicationBeforeValidationService implements BeforeValidationInterface
{
    public function __construct(private EntityManager $em, private Helper $helper, private RequestStack $request)
    {
    }

    public function modify(RouteConfigInterface $route, FormInterface $form): array
    {
        $form = $this->switchElement($form, 'company_contact');
        $form = $this->switchElement($form, 'recruiter_contact');
        $this->request->getSession()->set('invalid', $this->request->getCurrentRequest()->get('hidden'));
        $this->request->getCurrentRequest()->request->set('hidden', 0);
        return [$route->entity, $form];
    }

    private function switchElement(FormInterface $form, string $element): FormInterface
    {
        $foreignKey = substr($element, 0, -8);
        $oldOptions = $form->get($element)->getConfig()->getOptions();
        $optionElements = ['required', 'placeholder', 'priority', 'class', 'choice_label', 'label', 'help'];
        foreach ($optionElements as $key) {
            $options[$key] = $oldOptions[$key];
        }
        $criteria = ['title' => 'x'];
        if ($value = $this->request->getCurrentRequest()->get($foreignKey) ?? $form->get($foreignKey)->getNormData()) {
            $entity = $this->em->getRepository($this->helper->getFullEntityName($foreignKey))->find($value);
            $criteria = [$foreignKey => $entity];
        }
        $method = 'findData';
        $options['query_builder'] = $this->em->getRepository($options['class'])->$method($criteria);
        $form->remove($element);
        $form->add($element, EntityType::class, $options);
        return $form;
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Entity\Company;
use App\Entity\CompanyContact;
use App\Entity\JobTitle;
use App\Entity\Recruiter;
use App\Entity\RecruiterContact;
use App\Interface\AfterValidationInterface;
use App\Interface\RouteConfigInterface;
use App\Repository\CompanyContactRepository;
use App\Repository\CompanyRepository;
use App\Repository\JobTitleRepository;
use App\Repository\RecruiterContactRepository;
use App\Repository\RecruiterRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class ApplicationCreateAfterValidationService implements AfterValidationInterface
{
    public function __construct(
        private CompanyRepository $company,
        private CompanyContactRepository $companyContact,
        private EntityManagerInterface $em,
        private JobTitleRepository $jobTitle,
        private RecruiterRepository $recruiter,
        private RecruiterContactRepository $recruiterContact,
        private Security $security,
        private TranslatorInterface $translator
    )
    {}

    public function modify(RouteConfigInterface $route, FormInterface $form): array
    {
        $application = $route->entity;
        if ($noType = (!$route->entity?->getCompany() && !$route->entity?->getRecruiter())) {
            $this->addError($form, 'type_name', 'name');
        }
        if ($noContact = (!$route->entity?->getCompanyContact() && !$route->entity?->getRecruiterContact())) {
            $this->addError($form, 'gender_application', 'gender');
            $this->addError($form, 'email_address_application', 'emailAddress');
        }
        if ($noType || $noContact) {
            $this->addError($form, 'type', 'type');
        }
        if (!$form->get('job_title_application')->getData()) {
            $this->addError($form, 'new_job_title', 'newJobTitle');
        }
        if (!$form->isValid()) {
            return [$form, $application];
        }
        $type = $form->get('type')->getData();
        $name = $form->get('type_name');
        $criteria = ['user' => $this->security->getUser(), 'name' => $name->getData()];
        $this->addExistingError($name, $type == 'company' && $name->getData(), $criteria, 'company');
        $this->addExistingError($name, $type == 'recruiter' && $name->getData(), $criteria, 'recruiter');
        $email = $form->get('email_address_application');
        $company = $route->entity?->getCompany();
        $condition = $company && !$name->getData() && $email->getData();
        $criteria = ['company' => $company, 'email_address' => $email->getData()];
        $this->addExistingError($email, $condition, $criteria, 'companyContact');
        $recruiter = $route->entity?->getRecruiter();
        $condition = $recruiter && !$name->getData() && $email->getData();
        $criteria = ['recruiter' => $recruiter, 'email_address' => $email->getData()];
        $this->addExistingError($email, $condition, $criteria, 'recruiterContact');
        $jobTitle = $form->get('new_job_title');
        $criteria = ['name' => $jobTitle->getData()];
        if ($jobTitle->getData() && $this->jobTitle->findUserData($criteria)->getQuery()->getResult()) {
            $jobTitle->addError(new FormError($this->translator->trans('existingJobTitle')));
        }
        if (!$form->isValid()) {
            return [$form, $application];
        }
        $relation = new Company();
        $contact = new CompanyContact();
        $newJobTitle = new JobTitle();
        $relationMethod = 'setCompany';
        $contactMethod = 'setCompanyContact';
        $relationFormName = 'company';
        $contactFormName = 'company_contact';
        if ($type == 'recruiter') {
            $relation = new Recruiter();
            $contact = new RecruiterContact();
            $relationMethod = 'setRecruiter';
            $contactMethod = 'setRecruiterContact';
            $relationFormName = 'recruiter';
            $contactFormName = 'recruiter_contact';
        }
        if ($name->getData()) {
            $relation
                ->setName($name->getData())
                ->setWebsite($form->get('website')->getData())
                ->setUser($this->security->getUser())
                ->setCreatedAt(new DateTimeImmutable())
            ;
            $this->em->persist($relation);
        } else {
            $relation = $form->get($relationFormName)->getData();
        }
        if ($email->getData()) {
            $contact
                ->$relationMethod(
                    $relation
                )
                ->setGender($form->get('gender_application')->getData())
                ->setTitle($form->get('title')->getData())
                ->setIsInformal($form->get('is_informal')->getData())
                ->setFirstname($form->get('firstname')->getData())
                ->setLastname($form->get('lastname')->getData())
                ->setEmailAddress($form->get('email_address_application')->getData())
                ->setCreatedAt(new DateTimeImmutable())
            ;
            $this->em->persist($contact);
        }
        if ($jobTitle->getData()) {
            $newJobTitle
                ->setUser($this->security->getUser())
                ->setName($jobTitle->getData())
                ->setCreatedAt(new DateTimeImmutable())
            ;
            $this->em->persist($newJobTitle);
        }
        $this->em->flush();
        if ($name->getData()) {
            $application->$relationMethod($relation);
        } else {
            $application->$relationMethod($form->get($relationFormName)->getData());
        }
        if ($email->getData()) {
            $application->$contactMethod($contact);
        } else {
            $application->$contactMethod($form->get($contactFormName)->getData());
        }
        if ($jobTitle->getData()) {
            $application->setJobTitle($newJobTitle);
        } else {
            $application->setJobTitle($form->get('job_title_application')->getData());
        }
        return [$form, $application];
    }

    private function addError(FormInterface $form, string $errorField, string $textField): void
    {
        if (!$form->get($errorField)->getData()) {
            $form->get($errorField)->addError(new FormError($this->translator->trans('form.'.$textField.'.helpText')));
        }
    }

    private function addExistingError(FormInterface $form, bool $condition, array $criteria, string $repository): void
    {
        if ($condition && $this->$repository->findBy($criteria)) {
            $form->addError(new FormError($this->translator->trans('existing'.ucfirst($repository))));
        }
    }
}

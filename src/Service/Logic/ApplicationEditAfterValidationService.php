<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Interface\AfterValidationInterface;
use App\Interface\RouteConfigInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class ApplicationEditAfterValidationService implements AfterValidationInterface
{
    public function __construct(private RequestStack $request, private TranslatorInterface $translator)
    {
    }

    public function modify(RouteConfigInterface $route, FormInterface $form): array
    {
        if (!$this->request->getSession()->get('invalid') && $form->isValid()) {
            if (!$route->entity?->getCompany() && !$route->entity?->getRecruiter()) {
                $errorText = $this->translator->trans('requiresAtLeastOneCompanyRecruiter');
                $form->get('company')->addError(new FormError($errorText));
            } elseif (!$route->entity?->getCompanyContact() && !$route->entity?->getRecruiterContact()) {
                $errorText = $this->translator->trans('requiresAtLeastOneContact');
                $form->get('company_contact')->addError(new FormError($errorText));
            }
        }
        return [$form, $route->entity];
    }
}

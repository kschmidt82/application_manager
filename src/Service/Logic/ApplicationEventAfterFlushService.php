<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Interface\AfterFlushInterface;
use App\Interface\ApplicationEventInterface;
use App\Interface\RouteConfigInterface;
use App\Repository\EventRepository;
use Symfony\Component\Form\FormInterface;

final readonly class ApplicationEventAfterFlushService implements AfterFlushInterface
{
    public function __construct(private ApplicationEventInterface $applicationEvent, private EventRepository $event)
    {
    }

    public function modify(RouteConfigInterface $route, FormInterface $form): void
    {
        $closed = $route->entity->getApplication()->getApplicationEvents()->filter(function ($key) {
            return $key->getEvent()->getId() == 15;
        });
        if ($form->isValid() && !count($closed) && in_array($route->entity->getEvent()->getId(), [12, 13, 14, 16])) {
            $this->applicationEvent->create($route->entity->getApplication(), $this->event->find(15));
        }
    }
}

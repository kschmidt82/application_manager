<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Entity\Application;
use App\Entity\Event;
use App\Interface\ApplicationEventInterface;
use App\Interface\ColumnFactoryInterface;
use App\Interface\ColumnInterface;
use App\Interface\ConditionInterface;
use App\Interface\FlashInterface;
use App\Interface\LogicInterface;
use App\Interface\MailInterface;
use App\Interface\PlaceholderFactoryInterface;
use App\Interface\PlaceholderInterface;
use App\Interface\RedirectInterface;
use App\Interface\RouteConfigInterface;
use App\Interface\SearchesInterface;
use App\Repository\ApplicationRepository;
use App\Repository\EmailRepository;
use App\Repository\EventRepository;
use DateTimeImmutable;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;

final class ApplicationLogicService implements LogicInterface
{
    private ColumnInterface $column;
    private bool $isError;
    private PlaceholderInterface $placeholder;

    public function __construct(
        private readonly ApplicationEventInterface $applicationEvent,
        private readonly ApplicationRepository $application,
        private readonly ColumnFactoryInterface $columnFactory,
        private readonly ConditionInterface $condition,
        private readonly EmailRepository $email,
        private readonly EventRepository $event,
        private readonly FlashInterface $flash,
        private readonly MailInterface $mail,
        private readonly ParameterBagInterface $parameterBag,
        private readonly PlaceholderFactoryInterface $placeholderFactory,
        private readonly RedirectInterface $redirect,
        private readonly SearchesInterface $searches
    ) {
    }

    public function modify(RouteConfigInterface $route, ?FormInterface $form = null): array
    {
        $applications = $this->application->getUserResults();
        if ($route->action == 'close') {
            $this->sendMails($applications, 'Beendigung', 'Beendigungs-E-Mail');
        } else {
            $this->sendMails($applications, 'Bewerbung', 'Bewerbungs-E-Mail');
            $this->sendMails($applications, 'Erinnerung 2', 'Erinnerungs-E-Mail');
            $this->sendMails($applications, 'Erinnerung 1', 'Erinnerungs-E-Mail');
        }
        if (!(isset($this->isError) && $this->isError)) {
            $this->flash->add($route, 'emailsSentSuccessfully');
        } else {
            $this->flash->add($route, 'emailsNotSentSuccessfully', 'error');
        }
        return [$this->redirect->getParameters('app_application_index', $route->renderParameters)];
    }

    private function sendMails(array $applications, string $emailTypeName, string $eventName): void
    {
        $emails = $this->email->findByEmailType($emailTypeName);
        foreach ($applications as $application) {
            if (!($event = $this->getEvent($application, $emailTypeName, $eventName))) {
                continue;
            }
            $this->column = $this->columnFactory->create($application);
            $this->placeholder = $this->placeholderFactory->create($this->column);
            $email = null;
            foreach ($emails as $email) {
                $object = ($email->getDataField()->getColumn()) ? $this->column : $this->placeholder;
                if ($check = $this->condition->isCorrect($email, $object->getReplacementFromObject($email))) {
                    break;
                }
            }
            if (!isset($check) || !$check) {
                continue;
            }
            $subject = $this->getReplacedText($email->getSubject());
            $text = $this->getReplacedText($email->getText());
            $notes = 'Betreff: '.$subject."\r\n\r\n\r\n".$text;
            if ($application->getNoAutomaticEmails() && $emailTypeName == 'Bewerbung') {
                $this->createApplicationFile($notes);
            } elseif (!$application->getNoAutomaticEmails()) {
                $this->mail->sendApplicationMail($this->placeholder->getReplacement('E-Mail-Adresse'), $subject, $text);
            }
            if ($application->getNoAutomaticEmails() || $this->mail->isSent()) {
                $this->applicationEvent->create($application, $event, $notes);
                if ($emailTypeName == 'Beendigung') {
                    $this->applicationEvent->create($application, $this->event->find(15));
                }
            } else {
                $this->isError = true;
                $this->createApplicationFile($notes);
            }
        }
    }

    private function getEvent(Application $application, string $emailTypeName, string $eventName): ?Event
    {
        $event = $this->event->findOneBy(['name' => $eventName]);
        $applicationEvents = $application->getApplicationEvents();
        $existingEvent = $applicationEvents->filter(function ($key) use ($eventName) {
            return $key->getEvent()->getName() == $eventName;
        });

        if (
            ($emailTypeName != 'Erinnerung 2' && count($existingEvent)) ||
            ($emailTypeName == 'Erinnerung 2' && !count($existingEvent)) || count($existingEvent) > 1
        ) {
            return null;
        }
        $closed = $applicationEvents->filter(function ($key) {
            return in_array($key->getEvent()->getId(), [12, 13, 14, 15, 16]);
        });
        if ($emailTypeName == 'Beendigung' && is_numeric($application->getRecruiter()?->getId())) {
            $closed = $applicationEvents->filter(function ($key) {
                return $key->getEvent()->getId() == 13;
            });
        }
        if (count($closed)) {
            return null;
        }
        $lastApplicationEvent = $applicationEvents->get(count($applicationEvents) - 1);
        if (str_starts_with($emailTypeName, 'Erinnerung')) {
            $date = $lastApplicationEvent->getCreatedAt();
            $interval = $date->diff(new DateTimeImmutable());
            if ($interval->days < $this->parameterBag->get('app.daysBeforeReminder') || $interval->invert) {
                return null;
            }
        }
        return $event;
    }

    private function getReplacedText(string $text): string
    {
        $search = $this->searches->get($text);
        $replace = $this->getReplaces($search);
        $replaced = str_replace($search, $replace, $text);
        $search = ['  ', ' ,', "\r\n ", " \r\n"];
        $replace = [' ', ',', "\r\n", "\r\n"];
        do {
            $replaced = str_replace($search, $replace, $replaced, $count);
        } while ($count);
        return trim($replaced);
    }

    private function getReplaces(array $search): array
    {
        $replace = [];
        foreach ($search as $value) {
            $placeholder = trim(substr($value, 2, -2));
            $object = (str_contains($placeholder, '.')) ? $this->column : $this->placeholder;
            $replace[] = $object->getReplacement($placeholder);
        }
        return $replace;
    }

    private function createApplicationFile(string $notes): void
    {
        $dir = $this->parameterBag->get('app.applicationsDir');
        file_put_contents($dir.'Bewerbung '.$this->placeholder->getReplacement('Unternehmen').'.docx', $notes);
    }
}

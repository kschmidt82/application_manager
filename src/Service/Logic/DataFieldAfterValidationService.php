<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Entity\DataField;
use App\Interface\DataFieldAfterValidationInterface;
use App\Interface\DateInterface;
use App\Interface\HelperInterface;
use App\Interface\RouteConfigInterface;
use App\Repository\DataFieldRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class DataFieldAfterValidationService implements DataFieldAfterValidationInterface
{
    public function __construct(
        private DataFieldRepository $dataField,
        private DateInterface $date,
        private EntityManagerInterface $em,
        private HelperInterface $helper,
        private TranslatorInterface $translator
    ) {
    }

    public function modify(RouteConfigInterface $route, FormInterface $form, string $key): array
    {
        if (!$form->isValid()) {
            return [$form, $route->entity];
        }
        $entity = $this->getDataField($route, $form);
        $criteria = [
            $key => $entity->{'get'.$this->helper->getPascalCase($key)}(),
            'data_field' => $entity->getDataField(),
            'operator' => $entity->getOperator(),
            'value' => $entity->getValue()
        ];
        if ($entity->getId()) {
            $criteria['id'] = $entity->getId();
        }
        $method = 'getResults';
        if ($this->em->getRepository($route->entityNameFull)->$method($criteria)) {
            $errorText = $this->translator->trans('This value is already used.', [], 'validators');
            $form->get('data_field')->addError(new FormError($errorText));
        } elseif (!$entity->getValue() && in_array($entity->getOperator()->getValue(), ['eq', 'eq_not'])) {
            $form->get('value')->addError(new FormError($this->translator->trans('valueRequiredWithOperator')));
        }
        return [$form, $entity];
    }

    private function getDataField(RouteConfigInterface $route, FormInterface $form): object
    {
        $field = $form->get('data_field')->getData();
        $id = substr($field, 1);
        $name = (str_starts_with($field, 'c')) ? 'Column' : 'Placeholder';
        $entityNameFull = $this->helper->getFullEntityName($name);
        $data = $this->em->getRepository($entityNameFull)->find($id);
        if (!($dataField = $this->dataField->findOneBy([lcfirst($name) => $data]))) {
            $dataField = (new DataField())->{'set'.$name}($data)->setCreatedAt($this->date->getModified());
            $this->em->persist($dataField);
            $this->em->flush();
        }
        $route->entity->setDataField($dataField);
        return $route->entity;
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Interface\BeforeValidationInterface;
use App\Interface\RouteConfigInterface;
use Symfony\Component\Form\FormInterface;

final readonly class DataFieldBeforeValidationService implements BeforeValidationInterface
{
    public function modify(RouteConfigInterface $route, FormInterface $form): array
    {
        $field = $route->entity->getDataField();
        $value = ($id = $field?->getColumn()?->getId()) ? 'c'.$id : null;
        $value = ($id = $field?->getPlaceholder()?->getId()) ? 'p'.$id : $value;
        $form->get('data_field')?->setData($value);
        return [$route->entity, $form];
    }
}

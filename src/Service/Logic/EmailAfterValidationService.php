<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Interface\AfterValidationInterface;
use App\Interface\DataFieldAfterValidationInterface;
use App\Interface\RouteConfigInterface;
use Symfony\Component\Form\FormInterface;

final readonly class EmailAfterValidationService implements AfterValidationInterface
{
    public function __construct(private DataFieldAfterValidationInterface $dataField)
    {
    }

    public function modify(RouteConfigInterface $route, FormInterface $form): array
    {
        return $this->dataField->modify($route, $form, 'email_type');
    }
}

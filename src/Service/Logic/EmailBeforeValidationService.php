<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Interface\BeforeValidationInterface;
use App\Interface\RouteConfigInterface;
use Symfony\Component\Form\FormInterface;

final readonly class EmailBeforeValidationService implements BeforeValidationInterface
{
    public function __construct(private DataFieldBeforeValidationService $dataField)
    {
    }

    public function modify(RouteConfigInterface $route, FormInterface $form): array
    {
        return $this->dataField->modify($route, $form);
    }
}

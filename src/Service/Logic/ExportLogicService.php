<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Interface\ColumnFactoryInterface;
use App\Interface\DateInterface;
use App\Interface\LogicInterface;
use App\Interface\RouteConfigInterface;
use App\Repository\ApplicationRepository;
use DateTimeImmutable;
use Exception;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class ExportLogicService implements LogicInterface
{
    public function __construct(
        private ApplicationRepository $application,
        private ColumnFactoryInterface $columnFactory,
        private DateInterface $date,
        private EncoderInterface $encoder,
        private TranslatorInterface $translator
    ) {
    }

    public function modify(RouteConfigInterface $route, ?FormInterface $form = null): array|Response
    {
        if (!$form->isSubmitted()) {
            return [$route->renderParameters];
        }
        if (!$form->get('columns')->getData()) {
            $form->get('columns')->addError(new FormError($this->translator->trans('form.columns.helpText')));
        }
        if (!$form->isValid()) {
            return [$route->renderParameters, $form];
        }
        $formattedDate = $form->get('date_to')->getData()->format('Y-m-d');
        $date = null;
        try {
            $date = new DateTimeImmutable($this->date->addOneDay($formattedDate, 'Y-m-d H:i:s'));
        } catch (Exception) {
        }
        $applications = $this->application->findByDateRange($form->get('date_from')->getData(), $date);
        $rows = [];
        foreach ($applications as $application) {
            $column = $this->columnFactory->create($application);
            $columns = $form->get('columns')->getData();
            $replaced = [];
            foreach ($columns as $element) {
                $replaced[] = $column->getReplacement($element);
            }
            $rows[] = $replaced;
        }
        $response = new Response($this->encoder->encode($rows, 'csv', ['no_headers' => true]));
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        return $response;
    }
}

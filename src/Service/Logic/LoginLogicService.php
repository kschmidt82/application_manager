<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Interface\LogicInterface;
use App\Interface\RedirectInterface;
use App\Interface\RouteConfigInterface;
use App\Repository\UserRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

final readonly class LoginLogicService implements LogicInterface
{
    public function __construct(
        private AuthenticationUtils $authUtils,
        private RedirectInterface $redirect,
        private RequestStack $request,
        private RouterInterface $router,
        private Security $security,
        private UserRepository $user
    ) {
    }

    public function modify(RouteConfigInterface $route, ?FormInterface $form = null): array
    {
        $error = $this->authUtils->getLastAuthenticationError();
        $username = $this->security->getUser()?->getUserIdentifier();
        if ($username) {
            return [$this->redirect->getParameters('app_application_index')];
        }
        if (!empty($error) && $error->getMessageKey() == 'notVerified') {
            $name = $this->request->getSession()->get('_security.last_username');
            $user = $this->user->findOneBy(['username' => $name]);
            $parameters = ['id' => $user->getId(), 'name' => $user->getUsername(), 'email' => $user->getEmailAddress()];
            $renderParameters['link'] = $this->router->generate('app_user_email', $parameters);
        }
        return [
            array_merge($route->renderParameters, $renderParameters ?? [], [
                'username' => $username,
                'error' => $error,
                'noList' => true,
                'noMandatory' => true
            ])
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Interface\AfterValidationInterface;
use App\Interface\RouteConfigInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class PlaceholderAfterValidationService implements AfterValidationInterface
{
    public function __construct(private TranslatorInterface $translator)
    {
    }

    public function modify(RouteConfigInterface $route, FormInterface $form): array
    {
        if ($form->isValid() && str_contains($form->get('name')->getNormData(), '.')) {
            $form->get('name')->addError(new FormError($this->translator->trans('mustNotContainDot')));
        }
        return [$form, $route->entity];
    }
}

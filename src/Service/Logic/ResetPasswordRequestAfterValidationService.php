<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Entity\User;
use App\Interface\AfterValidationInterface;
use App\Interface\RouteConfigInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final readonly class ResetPasswordRequestAfterValidationService implements AfterValidationInterface
{
    public function __construct(private UserPasswordHasherInterface $hash)
    {
    }

    public function modify(RouteConfigInterface $route, FormInterface $form): array
    {
        $entity = ($route->entity instanceof User) ? $route->entity : null;
        $entity->setPassword($this->hash->hashPassword($entity, $form->get('password')->getData()));
        return [$form, $entity];
    }
}

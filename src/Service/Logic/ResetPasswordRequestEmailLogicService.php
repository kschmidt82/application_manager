<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Interface\LogicInterface;
use App\Interface\RouteConfigInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface as ResetPasswordHelper;

final readonly class ResetPasswordRequestEmailLogicService implements LogicInterface
{
    public function __construct(private RequestStack $request, private ResetPasswordHelper $resetPasswordHelper)
    {
    }

    public function modify(RouteConfigInterface $route, ?FormInterface $form = null): array
    {
        $session = $this->request->getSession();
        $helper = $this->resetPasswordHelper;
        return [['resetToken' => $session->get('ResetPasswordToken') ?? $helper->generateFakeResetToken()]];
    }
}

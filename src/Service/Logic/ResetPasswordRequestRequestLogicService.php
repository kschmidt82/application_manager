<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Entity\User;
use App\Interface\LogicInterface;
use App\Interface\MailInterface;
use App\Interface\RedirectInterface;
use App\Interface\RouteConfigInterface;
use App\Repository\UserRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

final readonly class ResetPasswordRequestRequestLogicService implements LogicInterface
{
    public function __construct(
        private MailInterface $mail,
        private RedirectInterface $redirect,
        private RequestStack $request,
        private ResetPasswordHelperInterface $resetPasswordHelper,
        private UserRepository $user
    ) {
    }

    public function modify(RouteConfigInterface $route, ?FormInterface $form = null): array
    {
        if (!($form->isSubmitted() && $form->isValid())) {
            return [$route->renderParameters];
        }
        $criteria = ['email_address' => $form->get('email_address')->getData()];
        if (!($user = $this->user->findOneBy($criteria))) {
            return [$this->redirect->getParameters('app_check_email')];
        }
        try {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
        } catch (ResetPasswordExceptionInterface) {
            return [$this->redirect->getParameters('app_check_email')];
        }
        $template = 'reset_password_request/email.html.twig';
        $parameters = ['resetToken' => $resetToken];
        $user = ($user instanceof User) ? $user : null;
        $this->mail->send($user, 'reset_password_request.email.title', $template, $parameters);
        $resetToken->clearToken();
        $this->request->getSession()->set('ResetPasswordToken', $resetToken);
        return [$this->redirect->getParameters('app_check_email')];
    }
}

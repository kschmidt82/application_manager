<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Interface\FlashInterface;
use App\Interface\FormInterface;
use App\Interface\LogicInterface;
use App\Interface\RedirectInterface;
use App\Interface\RouteConfigInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface as Form;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

final readonly class ResetPasswordRequestResetLogicService implements LogicInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private FlashInterface $flash,
        private FormInterface $form,
        private RedirectInterface $redirect,
        private RequestStack $request,
        private ResetPasswordHelperInterface $resetPasswordHelper,
        private TranslatorInterface $translator,
        private UserPasswordHasherInterface $hash
    ) {
    }

    public function modify(RouteConfigInterface $route, ?Form $form = null): array
    {
        $session = $this->request->getSession();
        if ($route->value) {
            $session->set('ResetPasswordToken', $route->value);
            return [$this->redirect->getParameters('app_reset_password')];
        }
        if (!($token = $session->get('ResetPasswordToken'))) {
            throw new NotFoundHttpException();
        }
        try {
            $user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
        } catch (ResetPasswordExceptionInterface $e) {
            $message = ResetPasswordExceptionInterface::MESSAGE_PROBLEM_VALIDATE;
            $messageText = $this->translator->trans($message, [], 'ResetPasswordBundle');
            $errorText = $this->translator->trans($e->getReason(), [], 'ResetPasswordBundle');
            $this->flash->add($route, sprintf('%s - %s', $messageText, $errorText), 'error');
            return [$this->redirect->getParameters('app_forgot_password_request')];
        }
        $user = ($user instanceof User) ? $user : null;
        $message = 'reset_password_request.request.successful';
        $parameters = ['entity' => $user, 'formName' => ChangePasswordType::class, 'message' => $message];
        $form = $this->form->get($route->with($parameters));
        if (!($form->isSubmitted() && $form->isValid())) {
            return [array_merge($route->renderParameters, ['form' => $form])];
        } else {
            $encodedPassword = $this->hash->hashPassword($user, $form->get('password')->getData());
            $user->setPassword($encodedPassword);
            $this->em->persist($user);
            $this->em->flush();
        }
        $this->resetPasswordHelper->removeResetRequest($token);
        $session->remove('ResetPasswordPublicToken');
        $session->remove('ResetPasswordCheckEmail');
        $session->remove('ResetPasswordToken');
        return [$this->redirect->getParameters('app_login')];
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Entity\User;
use App\Interface\AfterValidationInterface;
use App\Interface\RouteConfigInterface;
use App\Interface\UserMailInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final readonly class UserCreateAfterValidationService implements AfterValidationInterface
{
    public function __construct(private UserMailInterface $userMail, private UserPasswordHasherInterface $hash)
    {
    }

    public function modify(RouteConfigInterface $route, FormInterface $form): array
    {
        $entity = $route->entity;
        if ($form->isValid() && $entity instanceof User) {
            $entity->setPassword($this->hash->hashPassword($entity, $form->get('repeat_password')->getData()));
            $this->userMail->sendConfirmation($entity);
        }
        return [$form, $entity];
    }
}

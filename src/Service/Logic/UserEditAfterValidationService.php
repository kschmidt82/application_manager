<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Entity\User;
use App\Interface\AfterValidationInterface;
use App\Interface\RouteConfigInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final readonly class UserEditAfterValidationService implements AfterValidationInterface
{
    public function __construct(private UserPasswordHasherInterface $hash)
    {
    }

    public function modify(RouteConfigInterface $route, FormInterface $form): array
    {
        $entity = $route->entity;
        if ($form->isValid() && $entity instanceof User && !$form->get('new_password')->isEmpty()) {
            $entity->setPassword($this->hash->hashPassword($entity, $form->get('new_password')->getData()));
        }
        return [$form, $entity];
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Entity\User;
use App\Interface\FlashInterface;
use App\Interface\LogicInterface;
use App\Interface\RedirectInterface;
use App\Interface\RouteConfigInterface;
use App\Interface\UserMailInterface;
use App\Repository\UserRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

final readonly class UserEmailLogicService implements LogicInterface
{
    public function __construct(
        private FlashInterface $flash,
        private RedirectInterface $redirect,
        private RequestStack $request,
        private UserMailInterface $userMail,
        private UserRepository $user
    ) {
    }

    public function modify(RouteConfigInterface $route, ?FormInterface $form = null): array
    {
        $request = $this->request->getCurrentRequest();
        $email = $request->get('email');
        $criteria = ['id' => $request->get('id'), 'username' => $request->get('name'), 'email_address' => $email];
        $user = $this->user->findOneBy($criteria);
        if ($user instanceof User) {
            $this->userMail->sendConfirmation($user);
            $this->flash->add($route, 'user.email.successful');
            return [$this->redirect->getParameters('app_login')];
        }
        $this->flash->add($route, 'user.email.error', 'error');
        return [$this->redirect->getParameters('app_login')];
    }
}

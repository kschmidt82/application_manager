<?php

declare(strict_types=1);

namespace App\Service\Logic;

use App\Interface\EmailVerifierInterface;
use App\Interface\FlashInterface;
use App\Interface\LogicInterface;
use App\Interface\RedirectInterface;
use App\Interface\RouteConfigInterface;
use App\Repository\UserRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

final readonly class UserVerifyEmailLogicService implements LogicInterface
{
    public function __construct(
        private EmailVerifierInterface $emailVerifier,
        private FlashInterface $flash,
        private RedirectInterface $redirect,
        private RequestStack $request,
        private UserRepository $user
    ) {
    }

    public function modify(RouteConfigInterface $route, ?FormInterface $form = null): array
    {
        $criteria = ['username' => $this->request->getCurrentRequest()->get('username') ?? null];
        if (!($user = $this->user->findOneBy($criteria))) {
            $this->flash->add($route, 'user.create.error', 'error');
            return [$this->redirect->getParameters('app_login')];
        }
        $this->emailVerifier->handleEmailConfirmation($route, $user);
        return [$this->redirect->getParameters('app_login')];
    }
}

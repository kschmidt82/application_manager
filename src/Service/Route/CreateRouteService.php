<?php

declare(strict_types=1);

namespace App\Service\Route;

use App\Interface\ControllerConfigInterface;
use App\Interface\CreateInterface;
use App\Interface\DataInterface;
use App\Interface\DateInterface;
use App\Interface\FormInterface;
use App\Interface\RenderInterface;
use App\Interface\RouteInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;

final readonly class CreateRouteService implements CreateInterface
{
    public function __construct(
        private DataInterface $data,
        private DateInterface $date,
        private FormInterface $form,
        private RenderInterface $render,
        private RouteInterface $route,
        private Security $security
    ) {
    }

    public function callRoute(ControllerConfigInterface $controllerConfig): Response
    {
        $route = $this->route->getValues('create', $controllerConfig);
        $date = $this->date->getModified();
        $entity = $route->entity ?? new $route->entityNameFull();
        $entity = (method_exists($entity, 'setUser')) ? $entity->setUser($this->security->getUser()) : $entity;
        $entity = (method_exists($entity, 'setCreatedAt')) ? $entity->setCreatedAt($date) : $entity;
        $form = $this->form->get($route->with(['entity' => $entity]));
        $renderParameters = $this->data->getRedirectParameters($route, $form);
        return $this->route->getResponse($this->render->getParameters($route, $form, $renderParameters));
    }
}

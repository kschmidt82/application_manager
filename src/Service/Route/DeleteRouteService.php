<?php

declare(strict_types=1);

namespace App\Service\Route;

use App\Interface\ControllerConfigInterface;
use App\Interface\DataInterface;
use App\Interface\DeleteInterface;
use App\Interface\FlashInterface;
use App\Interface\RedirectInterface;
use App\Interface\RouteInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

final readonly class DeleteRouteService implements DeleteInterface
{
    public function __construct(
        private CsrfTokenManagerInterface $tokenManager,
        private DataInterface $data,
        private FlashInterface $flash,
        private RedirectInterface $redirect,
        private RequestStack $request,
        private RouteInterface $route,
    ) {
    }

    public function callRoute(ControllerConfigInterface $controllerConfig): Response
    {
        $route = $this->route->getValues('delete', $controllerConfig);
        $token = new CsrfToken('delete'.$route->entity->getId(), $this->request->getCurrentRequest()->get('_token'));
        $newRoute = $route->with(['message' => 'deleteSuccessful']);
        $isValid = ($this->tokenManager->isTokenValid($token));
        ($isValid) ? $this->data->flush($newRoute, 'remove') : $this->flash->add($route, 'deleteFailed', 'error');
        return $this->route->getResponse($this->redirect->getParameters($route->route, $route->renderParameters));
    }
}

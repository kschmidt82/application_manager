<?php

declare(strict_types=1);

namespace App\Service\Route;

use App\Interface\ControllerConfigInterface;
use App\Interface\DataInterface;
use App\Interface\EditInterface;
use App\Interface\FormInterface;
use App\Interface\RenderInterface;
use App\Interface\RouteInterface;
use Symfony\Component\HttpFoundation\Response;

final readonly class EditRouteService implements EditInterface
{
    public function __construct(
        private DataInterface $data,
        private FormInterface $form,
        private RenderInterface $render,
        private RouteInterface $route
    ) {
    }

    public function callRoute(ControllerConfigInterface $controllerConfig): Response
    {
        $route = $this->route->getValues('edit', $controllerConfig);
        $form = $this->form->get($route);
        $renderParameters = $this->data->getRedirectParameters($route, $form);
        return $this->route->getResponse($this->render->getParameters($route, $form, $renderParameters));
    }
}

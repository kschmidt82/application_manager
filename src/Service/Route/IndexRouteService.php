<?php

declare(strict_types=1);

namespace App\Service\Route;

use App\Interface\ControllerConfigInterface;
use App\Interface\IndexInterface;
use App\Interface\QueryInterface;
use App\Interface\RenderInterface;
use App\Interface\RouteInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

final readonly class IndexRouteService implements IndexInterface
{
    public function __construct(
        private QueryInterface $query,
        private RenderInterface $render,
        private RequestStack $request,
        private RouteInterface $route
    ) {
    }

    public function callRoute(ControllerConfigInterface $controllerConfig): Response
    {
        $route = $this->route->getValues('index', $controllerConfig);
        $this->request->getSession()->set($route->route, $route->page);
        $renderParameters = ['rows' => $this->query->getRows($route), 'columns' => $route->dataColumns];
        return $this->route->getResponse($this->render->getParameters($route, null, $renderParameters));
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Route;

use App\Enum\FormUse;
use App\Interface\ControllerConfigInterface;
use App\Interface\FormInterface as Form;
use App\Interface\OtherInterface;
use App\Interface\RenderInterface as Render;
use App\Interface\RouteInterface as Route;
use Symfony\Component\HttpFoundation\Response;

final readonly class OtherRouteService implements OtherInterface
{
    public function __construct(private Form $form, private Render $render, private Route $route)
    {
    }

    public function callRoute(ControllerConfigInterface $controllerConfig): Response
    {
        $route = $this->route->getValues('other', $controllerConfig);
        $form = null;
        if ($route->formUse == FormUse::BEFORE) {
            $form = $this->form->get($route);
        }
        $parameters = $route->logic->modify($route, $form ?? null);
        if ($parameters instanceof Response) {
            return $parameters;
        }
        $renderParameters = $parameters[0];
        $form = (count($parameters) == 2) ? $parameters[1] : $form;
        if ($route->formUse == FormUse::AFTER) {
            $form = $this->form->get($route);
        }
        return $this->route->getResponse($this->render->getParameters($route, $form, $renderParameters));
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Route;

use App\Interface\ControllerConfigInterface;
use App\Interface\HelperInterface;
use App\Interface\RouteInterface;
use App\Interface\RouteConfigInterface;
use App\Trait\ControllerRouteConfigTrait;
use Exception;
use ReflectionClass;
use Symfony\Bundle\SecurityBundle\Security;

final readonly class RouteConfigService implements RouteConfigInterface
{
    use ControllerRouteConfigTrait;

    public array $criteria;
    public string $entityNameFull;
    public string $entityNameLow;
    public string $formName;

    public function __construct(private HelperInterface $helper, private Security $security)
    {
    }

    public function set(RouteInterface $route, ControllerConfigInterface $controllerConfig, string $method): void
    {
        $entityName = $this->helper->getShortName(get_class($controllerConfig->controller));
        $this->action = $controllerConfig->action ?: $method;
        $this->afterFlush = $controllerConfig->afterFlush;
        $this->afterValidation = $controllerConfig->afterValidation;
        $this->beforeValidation = $controllerConfig->beforeValidation;
        $this->criteria = $route->criteria ?? ['user' => $this->security->getUser()];
        $this->dataColumns = $controllerConfig->dataColumns;
        $this->entity = $route->entity ?? $controllerConfig->entity;
        $this->entityNameFull = $this->helper->getFullEntityName($entityName);
        $this->entityNameLow = $this->helper->getLowerName($entityName);
        $this->formName = $this->helper->getFullFormName($entityName);
        $this->formUse = $controllerConfig->formUse;
        $this->logic = $controllerConfig->logic;
        $this->message = $controllerConfig->message ?: 'saveSuccessful';
        $this->page = $controllerConfig->page;
        $this->removeFormElements = $controllerConfig->removeFormElements;
        $renderParameters = array_merge($route->renderParameters ?? [], $controllerConfig->renderParameters);
        if (key_exists('subName', $renderParameters) && !key_exists('subLogo', $renderParameters)) {
            $renderParameters['subLogo'] = 'bi-person-fill';
        }
        $this->renderParameters = $renderParameters;
        $this->route = $controllerConfig->route ?: 'app_'.$this->entityNameLow.'_index';
        $this->value = $controllerConfig->value;
    }

    public function with(array $parameters): RouteConfigInterface
    {
        try {
            $reflection = new ReflectionClass($this);
            $clone = $reflection->newInstanceWithoutConstructor();
            $properties = get_object_vars($this);
            foreach ($properties as $key => $value) {
                $clone->$key = (key_exists($key, $parameters)) ? $parameters[$key] : $value;
            }
        } catch (Exception) {
        }
        return $clone ?? $this;
    }
}

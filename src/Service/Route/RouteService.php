<?php

declare(strict_types=1);

namespace App\Service\Route;

use App\Interface\AccessInterface;
use App\Interface\ControllerConfigInterface;
use App\Interface\HelperInterface;
use App\Interface\RouteInterface;
use App\Interface\RouteConfigInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

final class RouteService extends AbstractController implements RouteInterface
{
    public readonly array $criteria;
    public readonly ?object $entity;
    public readonly array $renderParameters;
    private readonly string $template;

    public function __construct(
        private readonly AccessInterface $access,
        private readonly EntityManagerInterface $em,
        private readonly HelperInterface $helper,
        private readonly RouterInterface $router,
        private readonly RouteConfigInterface $routeConfig
    ) {
    }

    public function getValues(string $method, ControllerConfigInterface $controllerConfig): RouteConfigService
    {
        $this->access->check($controllerConfig, $method);
        if ($controllerConfig->entityNameTop) {
            $this->setSubRouteConfig($controllerConfig, $method);
        }
        $this->template = $controllerConfig->template ?: $method;
        $this->routeConfig->set($this, $controllerConfig, $method);
        return $this->routeConfig;
    }

    private function setSubRouteConfig(ControllerConfigInterface $controllerConfig, string $method): void
    {
        $helper = $this->helper;
        $entityNameTop = $controllerConfig->entityNameTop;
        $entityNameTopLow = $helper->getLowerName($entityNameTop);
        $entityNameTopFull = $helper->getFullEntityName($entityNameTop);
        $entity = $controllerConfig->entity;
        $criteria = ['id' => $controllerConfig->value];
        $rows = (!$entity) ? $this->em->getRepository($entityNameTopFull)->findOneBy($criteria) : null;
        $this->criteria = [$entityNameTopLow => $rows ?? $entity->{'get'.$entityNameTop}()];
        $id = $controllerConfig->value ?: $entity->{'get'.$entityNameTop}()->getId();
        $parameters = [$entityNameTopLow.'_id' => $id];
        $topParameters = ['id' => $id];
        $renderParameters = ['parameters' => $parameters, 'topParameters' => $topParameters];
        $route = $this->getTopRoute($entityNameTopLow, 'show', $topParameters);
        $route = (!$route) ? $this->getTopRoute($entityNameTopLow, 'edit', $topParameters) : $route;
        $renderParameters['topRoute'] = $route;
        $this->renderParameters = (!$route) ? ['parameters' => $parameters] : $renderParameters;
        $class = $helper->getFullEntityName($helper->getShortName(get_class($controllerConfig->controller)));
        $this->entity = ($method == 'create') ? (new ($class)())->{'set'.$entityNameTop}($rows) : $entity;
    }

    private function getTopRoute(string $entityName, string $method, array $parameters): string
    {
        try {
            $route = 'app_'.$entityName.'_'.$method;
            $this->router->generate($route, $parameters);
            $topRoute = $route;
        } catch (Exception) {
        }
        return $topRoute ?? '';
    }

    public function getResponse(array $routeParameters): Response
    {
        $template = match ($this->template) {
            'create', 'edit', 'delete', 'other' => 'create_edit',
            'index' => 'index',
            default => $this->template
        };
        if (isset($routeParameters['redirect'])) {
            $redirect = $routeParameters['redirect'];
            return $this->redirectToRoute($redirect['route'], $redirect['parameters']);
        }
        return $this->render($this->getFullTemplate($template), $routeParameters);
    }

    private function getFullTemplate(string $template): string
    {
        if (!str_contains($template, '/')) {
            $template = 'other/'.$template;
        }
        return $template.'.html.twig';
    }
}

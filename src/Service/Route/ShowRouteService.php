<?php

declare(strict_types=1);

namespace App\Service\Route;

use App\Interface\ControllerConfigInterface;
use App\Interface\RenderInterface;
use App\Interface\RouteInterface;
use App\Interface\ShowInterface;
use Symfony\Component\HttpFoundation\Response;

final readonly class ShowRouteService implements ShowInterface
{
    public function __construct(private RenderInterface $render, private RouteInterface $route)
    {
    }

    public function callRoute(ControllerConfigInterface $controllerConfig): Response
    {
        $route = $this->route->getValues('show', $controllerConfig);
        $renderParameters = ['columns' => $route->dataColumns];
        return $this->route->getResponse($this->render->getParameters($route, null, $renderParameters));
    }
}

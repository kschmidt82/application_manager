<?php

declare(strict_types=1);

namespace App\Service\Variable;

use App\Entity\Application;
use App\Interface\ColumnInterface;
use App\Interface\HelperInterface;
use App\Repository\ColumnRepository;

final readonly class ColumnService implements ColumnInterface
{
    public function __construct(
        private Application $application,
        private ColumnRepository $column,
        private HelperInterface $helper
    ) {
    }

    public function getReplacementFromObject(object $data): string
    {
        $table = $data->getDataField()->getColumn()->getTable()->getValue();
        return $this->getResult($table, $data->getDataField()->getColumn()->getValue());
    }

    private function getTable(string $table, string $column): ?object
    {
        return match ($table) {
            'application' => $this->application,
            'application_event' => $this->getApplicationEvent($column),
            default => $this->application->{$this->getMethod($table)}()
        };
    }

    private function getApplicationEvent(string $column): ?object
    {
        $applicationEvents = $this->application->getApplicationEvents();
        if ($column != 'application_created_at') {
            return $applicationEvents->get(count($applicationEvents) - 1);
        }
        return array_values(
            $applicationEvents->filter(function ($key) {
                return $key->getEvent()->getName() == 'Bewerbungs-E-Mail';
            })->toArray()
        )[0];
    }

    private function getMethod(string $object): string
    {
        if ($object == 'application_created_at') {
            return 'getCreatedAt';
        }
        return 'get'.$this->helper->getPascalCase($object);
    }

    public function getResult(string $table, string $column): string
    {
        if (!($table = $this->getTable($table, $column))) {
            return '';
        }
        $parts = explode('.', $column);
        if (count($parts) == 2) {
            if (!($tempTable = $table->{$this->getMethod($parts[0])}())) {
                return '';
            }
            $table = $tempTable;
            $parts[0] = $parts[1];
        }
        return $this->modifyResult($table->{$this->getMethod($parts[0])}());
    }

    private function modifyResult(mixed $result): string
    {
        return match (gettype($result)) {
            'object' => $result->format('d.m.Y'),
            'boolean' => ($result) ? 'wahr' : 'falsch',
            default => (string)$result
        };
    }

    public function getReplacement(string $placeholder): string
    {
        $parts = explode('.', $placeholder);
        $column = $this->column->findOneByTableAndColumn($parts[0], $parts[1]);
        return ($column) ? $this->getResult($column->getTable()->getValue(), $column->getValue()) : '';
    }
}

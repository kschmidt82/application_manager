<?php

declare(strict_types=1);

namespace App\Service\Variable;

use App\Interface\ConditionInterface;

final readonly class ConditionService implements ConditionInterface
{
    public function isCorrect(object $data, string $replacement): bool
    {
        return $this->checkReplacement($data->getOperator()->getValue(), trim($replacement), $data->getValue());
    }

    private function checkReplacement(string $operator, mixed $value, mixed $expectedValue): bool
    {
        return match ($operator) {
            'eq' => ($value === $expectedValue),
            'eq_not' => ($value !== $expectedValue),
            'null' => (is_null($value) || $value === ''),
            'not_null' => (!(is_null($value) || $value === '')),
            'true' => ($value == 'wahr'),
            'false' => ($value == 'falsch')
        };
    }
}

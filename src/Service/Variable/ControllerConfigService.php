<?php

declare(strict_types=1);

namespace App\Service\Variable;

use App\Enum\FormUse;
use App\Interface\ControllerConfigInterface;
use App\Trait\ControllerRouteConfigTrait;

class ControllerConfigService implements ControllerConfigInterface
{
    use ControllerRouteConfigTrait;

    public readonly object $controller;
    public readonly string $entityNameTop;
    public readonly string $entityNameTopTop;
    public readonly bool $login;
    public readonly string $template;
    public readonly int $userId;

    public function __construct(object $controller)
    {
        $this->controller = $controller;
    }

    public function setValues(array $keys = [], array $values = []): self
    {
        $this->set($keys, $values);
        $this->setControllerValues();
        $this->setStandardValues();
        return $this;
    }

    private function setControllerValues(): void
    {
        $keys = ['dataColumns', 'entityNameTop', 'entityNameTopTop', 'renderParameters'];
        foreach ($keys as $key) {
            if (isset($this->controller->{$key}) && !isset($this->{$key})) {
                $this->{$key} = $this->controller->{$key};
            }
        }
    }

    private function setStandardValues(): void
    {
        $keys = ['action', 'afterFlush', 'afterValidation', 'beforeValidation', 'dataColumns', 'entity'];
        array_push($keys, 'entityNameTop', 'formUse', 'logic', 'login', 'message', 'page', 'removeFormElements');
        array_push($keys, 'renderParameters', 'route', 'template', 'value');
        $values = ['', null, null, null, [], null, '', FormUse::NO, null, true, '', 1, [], [], '', '', ''];
        $this->set($keys, $values);
        $this->entityNameTopTop ??= '';
        $this->userId ??= 0;
    }

    private function set(array $keys, array $values): void
    {
        for ($i = 0; $i < count($keys); $i++) {
            if (!isset($this->{$keys[$i]})) {
                $this->{$keys[$i]} = $values[$i];
            }
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Variable;

use App\Interface\DataColumnInterface;

final readonly class DataColumnService implements DataColumnInterface
{
    public function getName(array $column): string
    {
        $name = $column['name'] ?? $column['id'];
        return $column['lowerName'] ?? $name;
    }
}

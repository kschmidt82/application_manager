<?php

declare(strict_types=1);

namespace App\Service\Variable;

use App\Interface\DateInterface;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use Exception;

final readonly class DateService implements DateInterface
{
    public function getModified(): DateTimeImmutable
    {
        $timestamp = time();
        $timestamp = ($timestamp % 3600 == 0) ? $timestamp + 1 : $timestamp;
        return (new DateTimeImmutable())->setTimestamp($timestamp);
    }

    public function addOneDay(string $dateTime, string $format = 'Y-m-d'): string
    {
        try {
            $date = new DateTime($dateTime);
            $date->add(new DateInterval('P1D'));
        } catch (Exception) {
        }
        return (isset($date)) ? $date->format($format) : '';
    }
}

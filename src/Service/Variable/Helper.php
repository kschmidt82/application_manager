<?php

declare(strict_types=1);

namespace App\Service\Variable;

use App\Interface\HelperInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final readonly class Helper implements HelperInterface
{
    public function __construct(private ParameterBagInterface $parameterBag)
    {
    }

    public function getLowerName(string $name): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $name));
    }

    public function getFullEntityName(string $entity): string
    {
        return $this->parameterBag->get('app.entityNamespace').$entity;
    }

    public function getFullFormName(string $entity): string
    {
        return $this->parameterBag->get('app.formNamespace').$entity.'Type';
    }

    public function getShortName(string $name, string $search = 'Controller'): string
    {
        return str_replace($search, '', substr(strrchr($name, '\\'), 1));
    }

    public function getPascalCase(string $string): string
    {
        return ucfirst(str_replace('_', '', ucwords($string, '_')));
    }
}

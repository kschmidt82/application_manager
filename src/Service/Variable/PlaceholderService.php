<?php

declare(strict_types=1);

namespace App\Service\Variable;

use App\Interface\ColumnInterface;
use App\Interface\ConditionInterface;
use App\Interface\PlaceholderInterface;
use App\Interface\SearchesInterface;
use App\Repository\ConditionRepository;
use App\Repository\ResultRepository;

final readonly class PlaceholderService implements PlaceholderInterface
{
    public function __construct(
        private ColumnInterface $column,
        private ConditionInterface $condition,
        private ConditionRepository $conditionRepository,
        private ResultRepository $result,
        private SearchesInterface $searches
    ) {
    }

    public function getReplacementFromObject(object $data): string
    {
        return $this->getReplacement($data->getDataField()->getPlaceholder()->getName());
    }

    public function getReplacement(string $placeholderName): string
    {
        $results = $this->result->findByPlaceholder($placeholderName);
        foreach ($results as $result) {
            $conditions = $this->conditionRepository->getResults(['result' => $result]);
            foreach ($conditions as $condition) {
                if ($column = $condition->getDataField()->getColumn()) {
                    $replacement = $this->column->getResult($column->getTable()->getValue(), $column->getValue());
                } else {
                    $replacement = $this->getReplacementFromObject($condition);
                }
                if (!$this->condition->isCorrect($condition, $replacement)) {
                    continue 2;
                }
            }
            $result = $result->getResult();
            $search = $this->searches->get($result);
            return str_replace($search, $this->getReplaces($search), $result);
        }
        return '';
    }

    private function getReplaces(array $search): array
    {
        $replace = [];
        foreach ($search as $value) {
            $placeholder = trim(substr($value, 2, -2));
            $object = (str_contains($placeholder, '.')) ? $this->column : $this;
            $replace[] = $object->getReplacement($placeholder);
        }
        return $replace;
    }
}

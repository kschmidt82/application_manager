<?php

declare(strict_types=1);

namespace App\Service\Variable;

use App\Interface\RedirectInterface;
use Symfony\Component\HttpFoundation\RequestStack;

final readonly class RedirectService implements RedirectInterface
{
    public function __construct(private RequestStack $request)
    {
    }

    public function getParameters(string $route, array $renderParameters = []): array
    {
        $renderParameters['parameters'] ??= [];
        if ($page = $this->request->getSession()->get($route)) {
            $renderParameters['parameters']['page'] = $page;
        }
        return ['redirect' => ['route' => $route, 'parameters' => $renderParameters['parameters']]];
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Variable;

use App\Interface\RenderInterface;
use App\Interface\RouteConfigInterface as RouteConfig;
use Symfony\Component\Form\FormInterface as Form;

final readonly class RenderService implements RenderInterface
{
    public function getParameters(RouteConfig $route, ?Form $form = null, array $renderParameters = []): array
    {
        $entityNameLow = $route->entityNameLow;
        $entity = $route->entity;
        $action = $route->action;
        return array_merge(
            ['action' => $action, 'name' => $entityNameLow, 'row' => $entity, 'form' => $form, 'parameters' => []],
            $route->renderParameters,
            $renderParameters
        );
    }
}

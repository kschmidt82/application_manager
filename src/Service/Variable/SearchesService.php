<?php

declare(strict_types=1);

namespace App\Service\Variable;

use App\Interface\SearchesInterface;

final readonly class SearchesService implements SearchesInterface
{
    public function get(string $text): array
    {
        $search = [];
        $parts = explode('{{', $text);
        foreach ($parts as $part) {
            $parts2 = explode('}}', $part);
            if ($parts2[0]) {
                $search[] = '{{'.$parts2[0].'}}';
            }
        }
        return $search;
    }
}

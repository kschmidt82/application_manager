<?php

declare(strict_types=1);

namespace App\Trait;

trait CompanyRecruiterColumnTrait
{
    private array $columns = [
        ['id' => 'name'],
        ['id' => 'address'],
        ['id' => 'zip'],
        ['id' => 'city'],
        ['id' => 'website', 'type' => 'link', 'noIndex' => true],
        ['id' => 'createdAt', 'lowerName' => 'created_at', 'type' => 'date']
    ];
}

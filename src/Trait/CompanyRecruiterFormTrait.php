<?php

declare(strict_types=1);

namespace App\Trait;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

trait CompanyRecruiterFormTrait
{
    use FormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, $this->field->get('name'))
            ->add('address', TextType::class, $this->field->get('address'))
            ->add('zip', TextType::class, $this->field->get('zip'))
            ->add('city', TextType::class, $this->field->get('city'))
            ->add('website', TextType::class, $this->field->get('website'))
            ->add('submit', SubmitType::class, $this->field->get('submit'))
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Trait;

trait ContactColumnTrait
{
    private array $columns = [
        ['id' => 'isInformal', 'lowerName' => 'is_informal', 'type' => 'bool', 'noIndex' => true],
        ['id' => 'name', 'name' => 'gender', 'object' => true, 'noIndex' => true],
        ['id' => 'name', 'name' => 'title', 'object' => true],
        ['id' => 'firstname'],
        ['id' => 'lastname'],
        ['id' => 'emailAddress', 'lowerName' => 'email_address'],
        ['id' => 'phoneNumber', 'lowerName' => 'phone_number', 'noIndex' => true],
        ['id' => 'address', 'noIndex' => true],
        ['id' => 'zip', 'noIndex' => true],
        ['id' => 'city', 'noIndex' => true],
        ['id' => 'notes', 'noIndex' => true]
    ];
}

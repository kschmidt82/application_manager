<?php

declare(strict_types=1);

namespace App\Trait;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

trait ContactFormTrait
{
    use FormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('gender', EntityType::class, $this->field->get('gender'))
            ->add('title', EntityType::class, $this->field->get('title'))
            ->add('is_informal', CheckboxType::class, $this->field->get('is_informal'))
            ->add('firstname', TextType::class, $this->field->get('firstname'))
            ->add('lastname', TextType::class, $this->field->get('lastname'))
            ->add('email_address', TextType::class, $this->field->get('email_address'))
            ->add('phone_number', TextType::class, $this->field->get('phone_number'))
            ->add('address', TextType::class, $this->field->get('address'))
            ->add('zip', TextType::class, $this->field->get('zip'))
            ->add('city', TextType::class, $this->field->get('city'))
            ->add('notes', TextareaType::class, $this->field->get('notes'))
            ->add('submit', SubmitType::class, $this->field->get('submit'))
        ;
    }
}

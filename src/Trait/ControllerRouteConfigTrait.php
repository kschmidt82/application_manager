<?php

declare(strict_types=1);

namespace App\Trait;

use App\Enum\FormUse;
use App\Interface\AfterFlushInterface;
use App\Interface\AfterValidationInterface;
use App\Interface\BeforeValidationInterface;
use App\Interface\LogicInterface;
use Symfony\Component\HttpFoundation\Request;

trait ControllerRouteConfigTrait
{
    public readonly string $action;
    public readonly ?AfterFlushInterface $afterFlush;
    public readonly ?AfterValidationInterface $afterValidation;
    public readonly ?BeforeValidationInterface $beforeValidation;
    public readonly array $dataColumns;
    public readonly ?object $entity;
    public readonly ?FormUse $formUse;
    public readonly ?LogicInterface $logic;
    public readonly string $message;
    public readonly int $page;
    public readonly array $removeFormElements;
    public readonly array $renderParameters;
    public readonly ?Request $request;
    public readonly string $route;
    public readonly mixed $value;
}

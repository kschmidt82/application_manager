<?php

declare(strict_types=1);

namespace App\Trait;

use App\Interface\ControllerConfigInterface;

trait ControllerTrait
{
    private readonly ControllerConfigInterface $controllerConfig;
    public readonly array $dataColumns;
    public readonly string $entityNameTop;
    public readonly string $entityNameTopTop;
    public readonly array $removeFormElements;
    public readonly array $renderParameters;
}

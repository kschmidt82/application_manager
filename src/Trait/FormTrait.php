<?php

declare(strict_types=1);

namespace App\Trait;

use App\Interface\FormFieldOptionInterface as FormFieldOption;
use App\Interface\HelperInterface as Helper;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait FormTrait
{
    private readonly ?string $id;

    public function __construct(private readonly FormFieldOption $field, private readonly Helper $helper)
    {
        $id = $this->helper->getFullEntityName($this->helper->getShortName(get_class(), 'Type'));
        $this->id = (class_exists($id)) ? $id : null;
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
    }

    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array_merge(
                ['data_class' => $this->id ?? null, 'attr' => ['novalidate' => 'novalidate']],
                (method_exists($this, 'getDefaults')) ? $this->getDefaults() : []
            )
        );
    }

    public function getBlockPrefix(): string
    {
        return '';
    }

    public function getParent(): string
    {
        return FormType::class;
    }
}

<?php

declare(strict_types=1);

namespace App\Trait;

use App\Interface\HelperInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\SecurityBundle\Security;

trait RepositoryTrait
{
    public function __construct(
        private readonly HelperInterface $helper,
        private readonly ManagerRegistry $registry,
        private readonly Security $security
    ) {
        $id = $this->helper->getFullEntityName($this->helper->getShortName(get_class(), 'Repository'));
        parent::__construct($registry, $id);
    }

    public function getResults(array $criteria = [], array $sort = []): ?array
    {
        return $this->findData($criteria, $sort)->getQuery()->getResult();
    }

    public function findData(array $criteria = [], array $sort = []): QueryBuilder
    {
        $sort = $sort ?: [['column' => 'id', 'sort' => 'desc']];
        return $this->setQuery($this->createQueryBuilder('q'), $criteria, $sort);
    }

    private function setQuery(QueryBuilder $qb, array $criteria, array $order = []): QueryBuilder
    {
        foreach ($criteria as $key => $value) {
            $column = $this->getFullColumn($key);
            $parameter = str_replace('.', '_', $column);
            $condition = $column.' IS NULL';
            if (is_array($value)) {
                $operator = match ($value['operator']) {
                    'eq' => '=',
                    'like' => 'LIKE',
                    'gt' => '>',
                    'lt' => '<'
                };
                $condition = $column.' '.$operator.' :'.$parameter;
                if ($operator == 'LIKE' && !is_object($value['value'])) {
                    $value['value'] = '%'.$value['value'].'%';
                }
                $qb->setParameter($parameter, $value['value']);
            } elseif (!is_null($value)) {
                $addition = $column.' = :'.$parameter;
                if ($key == 'user') {
                    $condition .= ' OR '.$addition;
                    $condition = '('.$condition.')';
                } elseif ($key == 'id') {
                    $condition = $column.' <> :'.$parameter;
                } else {
                    $condition = $addition;
                }
                $qb->setParameter($parameter, $value);
            }
            $qb->andWhere($condition);
        }
        foreach ($order as $statement) {
            $qb->addOrderBy($this->getFullColumn($statement['column']), $statement['sort']);
        }
        return $qb;
    }

    private function getFullColumn(string $column): string
    {
        return (str_contains($column, '-')) ? str_replace('-', '.', $column) : 'q.'.$column;
    }

    public function getUserResults(array $criteria = [], array $sort = []): ?array
    {
        return $this->findUserData($criteria, $sort)->getQuery()->getResult();
    }

    public function findUserData(array $criteria = [], array $sort = []): QueryBuilder
    {
        $criteria = array_merge(['user' => $this->security->getUser()], $criteria);
        return $this->setQuery($this->createQueryBuilder('q'), $criteria, $sort);
    }
}

<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

final class CustomUserPassword extends UserPassword
{
    public $message = 'This value is not valid.';

    public function validatedBy(): string
    {
        return CustomUserPasswordValidator::class;
    }
}

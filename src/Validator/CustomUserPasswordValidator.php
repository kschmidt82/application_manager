<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Security\Core\Validator\Constraints\UserPasswordValidator;
use Symfony\Component\Validator\Constraint;

final class CustomUserPasswordValidator extends UserPasswordValidator
{
    public function validate(mixed $password, Constraint $constraint): void
    {
        if (empty($password)) {
            return;
        }
        parent::validate($password, $constraint);
    }
}

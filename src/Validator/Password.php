<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

final class Password extends Constraint
{
    public string $message = 'This value is not valid.';
}

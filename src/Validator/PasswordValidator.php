<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use UnexpectedValueException;

final class PasswordValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!($constraint instanceof Password)) {
            throw new UnexpectedTypeException($constraint, Password::class);
        }
        if (empty($value)) {
            return;
        }
        if (!is_string($value)) {
            throw new UnexpectedValueException('Value must be string.');
        }
        $regex = '^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#%*-])[A-Za-z\d@$!%*?&#%*-]{8,4096}$^';
        if (!preg_match($regex, $value)) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

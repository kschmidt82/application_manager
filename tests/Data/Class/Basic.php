<?php

declare(strict_types=1);

namespace App\Tests\Data\Class;

use App\Repository\UserRepository;
use App\Service\Variable\Helper;
use Exception;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Translation\DataCollectorTranslator;

abstract class Basic extends WebTestCase
{
    protected KernelBrowser $client;
    protected string $controller;
    protected Crawler $crawler;
    protected array $elements = [];
    protected array $flashMessages = [
        'error' => ['text' => 'form.name.helpText', 'selector' => 'div'],
        'success' => ['text' => 'saveSuccessful', 'selector' => 'span'],
        'deleteSuccess' => ['text' => 'deleteSuccessful', 'selector' => 'span']
    ];
    protected array $hrefs = [];
    protected bool $login = true;
    protected string $method = 'edit';
    protected array $pageElements = [
        'headline' => ['text' => '', 'selector' => 'h2'],
        'createLink' => ['text' => 'create', 'selector' => 'a'],
        'editLink' => ['text' => 'edit', 'selector' => 'a'],
        'backLink' => ['text' => 'back', 'selector' => 'a'],
        'deleteLink' => ['text' => 'delete', 'selector' => 'a'],
        'multiShowLink' => ['text' => '<i class="bi-sunglasses"></i>', 'selector' => 'a'],
        'multiEditLink' => ['text' => '<i class="bi-pencil-fill"></i>', 'selector' => 'a'],
        'multiDeleteLink' => ['text' => '<i class="bi-trash-fill"></i>', 'selector' => 'a'],
        'multiSubLink' => ['text' => '<i class="bi-person-fill"></i>', 'selector' => 'a'],
        'saveButton' => ['text' => 'form.submit.label', 'selector' => 'button'],
        'labelText' => ['text' => 'form.name.label', 'selector' => 'label'],
        'text' => ['text' => '', 'selector' => 'span'],
        'column' => ['text' => 'N94', 'selector' => 'td'],
        'topLink' => ['text' => '', 'selector' => 'a'],
        'subLink' => ['text' => '', 'selector' => 'a']
    ];
    protected array $selectors = [];
    protected array $texts = [];
    protected DataCollectorTranslator $translator;
    protected string $url = '';

    protected function setup(): void
    {
        $this->unsetPageElements();
        $this->pageElements = $this->setElements($this->pageElements, $this->texts);
        $this->flashMessages = $this->setElements($this->flashMessages, $this->texts);
        $this->pageElements = $this->setElements($this->pageElements, $this->selectors, 'selector');
        $this->flashMessages = $this->setElements($this->flashMessages, $this->selectors, 'selector');
    }

    private function unsetPageElements(): void
    {
        $pageElements = $this->pageElements;
        $elements = $this->elements;
        foreach ($pageElements as $key => $pageElement) {
            if (!in_array($key, $elements) && !key_exists($key, $this->texts) && !key_exists($key, $this->hrefs)) {
                unset($this->pageElements[$key]);
            }
        }
    }

    protected function setElements(array $elements, array $modifications, string $element = 'text'): array
    {
        foreach ($modifications as $key => $modification) {
            if (isset($elements[$key])) {
                $elements[$key][$element] = $modification;
            }
        }
        return $elements;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $properties = get_object_vars($this);
        foreach ($properties as $property => $value) {
            if (isset($this->$property)) {
                unset($this->$property);
            }
        }
    }

    protected function setClient(?string $url = null, ?bool $login = null, int $id = 1): void
    {
        $this->url = $url ?? $this->url;
        $login ??= $this->login;
        ($login) ? $this->login($id) : $this->noLogin();
        $this->setHrefs();
        $this->pageElements = $this->setElements($this->pageElements, $this->hrefs, 'href');
    }

    private function login(int $id = 1): void
    {
        $client = $this->getClient();
        $repository = $this->getObject(UserRepository::class);
        $client->loginUser($repository->find($id));
        $this->request($client, $this->url);
    }

    private function getClient(): KernelBrowser
    {
        $client = static::createClient();
        $client->followRedirects();
        return $client;
    }

    private function getObject(string $className): ?object
    {
        try {
            return static::getContainer()->get($className);
        } catch (Exception) {
        }
        return null;
    }

    protected function request(KernelBrowser $client, string $url): void
    {
        $helper = $this->getObject(Helper::class);
        $this->controller ??= $helper->getLowerName($helper->getShortName(get_called_class(), 'ControllerTest'));
        $uri = '/'.$this->controller;
        $uri .= ($url) ? '/'.$url : '';
        $client->request('GET', $uri);
        $this->client = $client;
        $this->crawler = $client->getCrawler();
        $this->translator ??= $this->getObject('translator');
    }

    private function noLogin(): void
    {
        $client = $this->getClient();
        $this->request($client, $this->url);
    }

    private function setHrefs(): void
    {
        $controller = $this->controller;
        $links = ['createLink', 'multiShowLink', 'multiEditLink', 'multiDeleteLink', 'editLink', 'backLink'];
        $links[] = 'deleteLink';
        $hrefs = ['/'.$controller.'/create', '/'.$controller.'/94/show', '/'.$controller.'/94/edit', '#'];
        array_push($hrefs, '/'.$controller.'/94/edit', '/'.$controller, '#');
        for ($i = 0; $i < count($links); $i++) {
            if (isset($this->pageElements[$links[$i]])) {
                $this->pageElements[$links[$i]]['href'] = $hrefs[$i];
            }
        }
    }
}

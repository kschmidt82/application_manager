<?php

declare(strict_types=1);

namespace App\Tests\Data\Class;

abstract class Create extends Basic
{
    protected array $elements = ['labelText', 'backLink', 'saveButton'];
    protected string $url = 'create';
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Class;

abstract class Delete extends Basic
{
    protected array $texts = ['saveButton' => 'delete'];
    protected string $url = '1/edit';
}

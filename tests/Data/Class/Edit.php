<?php

declare(strict_types=1);

namespace App\Tests\Data\Class;

abstract class Edit extends Basic
{
    protected array $elements = ['labelText', 'backLink', 'deleteLink', 'saveButton'];
    protected string $url = '1/edit';
}

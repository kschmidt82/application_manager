<?php

declare(strict_types=1);

namespace App\Tests\Data\Class;

abstract class Index extends Basic
{
    protected array $elements = ['multiShowLink', 'multiEditLink', 'multiDeleteLink', 'createLink', 'column'];
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Class;

abstract class Show extends Basic
{
    protected array $elements = ['column', 'editLink', 'backLink', 'deleteLink'];
    protected string $method = 'show';
    protected string $url = '94/show';
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class ApplicationEditError
{
    public static function provide(): array
    {
        return [
            [
                ['company' => '', 'company_contact' => '', 'recruiter' => '', 'recruiter_contact' => ''],
                'requiresAtLeastOneCompanyRecruiter'
            ],
            [
                ['company' => '1', 'company_contact' => '', 'recruiter' => '', 'recruiter_contact' => ''],
                'requiresAtLeastOneContact'
            ]
        ];
    }
}

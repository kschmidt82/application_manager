<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class ApplicationEventSuccess
{
    public static function provide(): array
    {
        return [[['event' => '4', 'notes' => 'N200']]];
    }
}

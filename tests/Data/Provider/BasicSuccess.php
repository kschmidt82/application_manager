<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class BasicSuccess
{
    public static function provide(): array
    {
        return [[['name' => 'N200']]];
    }
}

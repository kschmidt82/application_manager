<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class ConditionError
{
    public static function provide(): array
    {
        return [[['data_field' => 'p1', 'operator' => '4', 'value' => 'V4'], 'This value is already used.']];
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class ConditionSuccess
{
    public static function provide(): array
    {
        return [[['data_field' => 'p2', 'operator' => '3', 'value' => 'test']]];
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class ContactError
{
    public static function provide(): array
    {
        return [
            [['email_address' => 'a@a.'], 'form.emailAddress.helpText'],
            [['email_address' => 'e20@test.test'], 'This value is already used.']
        ];
    }
}

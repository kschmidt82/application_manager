<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class ContactSuccess
{
    public static function provide(): array
    {
        return [[['firstname' => 'Max', 'lastname' => 'Mustermann', 'email_address' => 'mm@gmail.com']]];
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class LoginError
{
    public static function provide(): array
    {
        return [
            [['_username' => 'testuser', '_password' => 'foobar123'], 'Invalid credentials.'],
            [['_username' => '', '_password' => 'foobar123'], 'Invalid credentials.'],
            [['_username' => 'testuser', '_password' => ''], 'Invalid credentials.'],
            [['_username' => '', '_password' => ''], 'Invalid credentials.'],
            [['_username' => 'testuser2', '_password' => 'Test123!'], 'notVerified']
        ];
    }
}

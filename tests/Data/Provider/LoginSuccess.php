<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class LoginSuccess
{
    public static function provide(): array
    {
        return [[['_username' => 'testuser1', '_password' => 'Test123!']]];
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class PlaceholderError
{
    public static function provide(): array
    {
        return [
            [['name' => 'x'], 'form.name.helpText'],
            [['name' => 'N20'], 'This value is already used.'],
            [['name' => 'N.20'], 'mustNotContainDot']
        ];
    }
}

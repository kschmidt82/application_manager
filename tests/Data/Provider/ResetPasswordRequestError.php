<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class ResetPasswordRequestError
{
    public static function provide(): array
    {
        return [[['email_address' => 'a@a.'], 'form.emailAddress.helpText']];
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class ResetPasswordRequestSuccess
{
    public static function provide(): array
    {
        return [[['email_address' => 'e1@test.test']]];
    }
}

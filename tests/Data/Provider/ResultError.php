<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class ResultError
{
    public static function provide(): array
    {
        return [[['result' => 'R4'], 'This value is already used.']];
    }
}

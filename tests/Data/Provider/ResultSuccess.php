<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class ResultSuccess
{
    public static function provide(): array
    {
        return [[['result' => 'test']]];
    }
}

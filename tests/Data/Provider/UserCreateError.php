<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class UserCreateError
{
    public static function provide(): array
    {
        return [
            [['username' => 'x'], 'form.userUsername.helpText'],
            [['email_address' => 'a@a.'], 'form.userEmailAddress.helpText'],
            [['email_address' => 'e20@test.test'], 'This value is already used.'],
            [['repeat_password[first]' => 'Test123!!'], 'form.repeatPassword.helpText'],
            [
                ['repeat_password[first]' => 'Test123!!', 'repeat_password[second]' => 'Test123!'],
                'form.repeatPassword.helpText'
            ],
            [['firstname' => 'x'], 'form.userFirstname.helpText'],
            [['lastname' => 'x'], 'form.userLastname.helpText'],
            [
                [
                    'username' => 'x',
                    'email_address' => 'e20@test.test',
                    'repeat_password[first]' => 'Test123!!',
                    'firstname' => 'x',
                    'lastname' => 'x'
                ],
                'form.userUsername.helpText'
            ],
            [
                [
                    'username' => 'x',
                    'email_address' => 'e20@test.test',
                    'repeat_password[first]' => 'Test123!!',
                    'firstname' => 'x',
                    'lastname' => 'x'
                ],
                'This value is already used.'
            ],
            [
                [
                    'username' => 'x',
                    'email_address' => 'e20@test.test',
                    'repeat_password[first]' => 'Test123!!',
                    'firstname' => 'x',
                    'lastname' => 'x'
                ],
                'form.repeatPassword.helpText'
            ],
            [
                [
                    'username' => 'x',
                    'email_address' => 'e20@test.test',
                    'repeat_password[first]' => 'Test123!!',
                    'firstname' => 'x',
                    'lastname' => 'x'
                ],
                'form.userFirstname.helpText'
            ],
            [
                [
                    'username' => 'x',
                    'email_address' => 'e20@test.test',
                    'repeat_password[first]' => 'Test123!!',
                    'firstname' => 'x',
                    'lastname' => 'x'
                ],
                'form.userLastname.helpText'
            ],
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class UserCreateSuccess
{
    public static function provide(): array
    {
        return [
            [
                [
                    'username' => 'testuser',
                    'repeat_password[first]' => 'Test123!!',
                    'repeat_password[second]' => 'Test123!!',
                    'firstname' => 'Max',
                    'lastname' => 'Mustermann',
                    'email_address' => 'test@test.xyz'
                ]
            ]
        ];
    }
}

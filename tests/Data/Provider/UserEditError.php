<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class UserEditError
{
    public static function provide(): array
    {
        return [
            [['new_password[first]' => 'Test123!!'], 'form.newPassword.helpText'],
            [['new_password[first]' => 'Test123!!', 'new_password[second]' => 'Test123!'], 'form.newPassword.helpText'],
            [['firstname' => 'x'], 'form.userFirstname.helpText'],
            [['lastname' => 'x'], 'form.userLastname.helpText'],
            [
                ['new_password[first]' => 'Test123!!', 'firstname' => 'x', 'lastname' => 'x'],
                'form.newPassword.helpText'
            ],
            [
                ['new_password[first]' => 'Test123!!', 'firstname' => 'x', 'lastname' => 'x'],
                'form.userFirstname.helpText'
            ],
            [
                ['new_password[first]' => 'Test123!!', 'firstname' => 'x', 'lastname' => 'x'],
                'form.userLastname.helpText'
            ]
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Provider;

final readonly class UserEditSuccess
{
    public static function provide(): array
    {
        return [[['firstname' => 'Max', 'lastname' => 'Mustermann']]];
    }
}

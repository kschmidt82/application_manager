<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait CreateBasicTestsTrait
{
    use CreateTestsTrait;
    use TestErrorFormTrait;
    use TestSuccessFormTrait;
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait CreateTestsTrait
{
    use ErrorFormCheckTrait;
    use SuccessFormCheckTrait;
    use TestEmptyFormTrait;
    use TestNoLoginTrait;
    use TestPageElementsTrait;
}

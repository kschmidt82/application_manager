<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait DeleteTestsTrait
{
    use TestNoLoginTrait;
    use TestRemoveTrait;
    use TestWrongUserTrait;
}

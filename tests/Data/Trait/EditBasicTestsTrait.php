<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait EditBasicTestsTrait
{
    use EditTestsTrait;
    use TestErrorFormTrait;
    use TestSuccessFormTrait;
}

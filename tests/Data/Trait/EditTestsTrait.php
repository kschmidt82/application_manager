<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait EditTestsTrait
{
    use ErrorFormCheckTrait;
    use ShowTestsTrait;
    use SuccessFormCheckTrait;
}

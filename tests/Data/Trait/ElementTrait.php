<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

use Symfony\Component\DomCrawler\Crawler;

trait ElementTrait
{
    private function checkElement(array $element): void
    {
        $domain = ($element['text'] == 'notVerified') ? 'security' : 'messages';
        if (str_ends_with($element['text'], '.')) {
            $domain = (!$this->controller) ? 'security' : 'validators';
        }
        $nodes = $this->crawler->filter($element['selector'])->each(function (Crawler $node) {
            return trim($node->text()) ?: trim($node->html());
        });
        $this->assertContains($this->translator->trans($element['text'], [], $domain), $nodes);
        if (isset($element['href'])) {
            $nodes = $this->crawler->filter($element['selector'])->each(function (Crawler $node) {
                return trim($node->attr('href'));
            });
            $this->assertContains($element['href'], $nodes);
        }
    }
}

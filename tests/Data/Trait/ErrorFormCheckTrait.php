<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait ErrorFormCheckTrait
{
    use FormTrait;

    private function checkErrorForm(array $formElements, string $text): void
    {
        $this->checkForm('error', ['error' => $text], $formElements);
    }
}

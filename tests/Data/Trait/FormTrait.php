<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait FormTrait
{
    use ElementTrait;

    private function checkForm(string $key, array $flashMessages = [], array $formElements = []): void
    {
        $this->setClient();
        $text = $this->translator->trans($this->pageElements['saveButton']['text']);
        $this->crawler = $this->client->submitForm($text, $formElements);
        $this->flashMessages = $this->setElements($this->flashMessages, $flashMessages);
        $this->checkElement($this->flashMessages[$key]);
    }
}

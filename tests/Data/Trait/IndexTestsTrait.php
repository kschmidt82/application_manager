<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait IndexTestsTrait
{
    use TestNoLoginTrait;
    use TestPageElementsTrait;
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait ShowTestsTrait
{
    use IndexTestsTrait;
    use TestWrongUserTrait;
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait SuccessFormCheckTrait
{
    use FormTrait;

    private function checkSuccessForm(array $formElements): void
    {
        $this->checkForm('success', [], $formElements);
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

use App\Tests\Data\Provider\ApplicationEventSuccess;
use PHPUnit\Framework\Attributes\DataProviderExternal;

trait TestApplicationEventSuccessFormTrait
{
    #[DataProviderExternal(ApplicationEventSuccess::class, 'provide')]
    public function testSuccessForm(array $formElements): void
    {
        $this->checkSuccessForm($formElements);
    }
}

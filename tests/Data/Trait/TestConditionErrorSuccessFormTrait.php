<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

use App\Tests\Data\Provider\ConditionError;
use App\Tests\Data\Provider\ConditionSuccess;
use PHPUnit\Framework\Attributes\DataProviderExternal;

trait TestConditionErrorSuccessFormTrait
{
    #[DataProviderExternal(ConditionError::class, 'provide')]
    public function testErrorForm(array $formElements, string $text): void
    {
        $this->checkErrorForm($formElements, $text);
    }

    #[DataProviderExternal(ConditionSuccess::class, 'provide')]
    public function testSuccessForm(array $formElements): void
    {
        $this->checkSuccessForm($formElements);
    }
}

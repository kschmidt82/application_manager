<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

use App\Tests\Data\Provider\ContactError;
use App\Tests\Data\Provider\ContactSuccess;
use PHPUnit\Framework\Attributes\DataProviderExternal;

trait TestContactErrorSuccessFormTrait
{
    #[DataProviderExternal(ContactError::class, 'provide')]
    public function testErrorForm(array $formElements, string $text): void
    {
        $this->checkErrorForm($formElements, $text);
    }

    #[DataProviderExternal(ContactSuccess::class, 'provide')]
    public function testSuccessForm(array $formElements): void
    {
        $this->checkSuccessForm($formElements);
    }
}

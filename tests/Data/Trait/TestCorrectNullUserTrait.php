<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait TestCorrectNullUserTrait
{
    public function testCorrectNullUser(): void
    {
        $this->setClient('2/'.$this->method);
        $this->assertResponseStatusCodeSame(200);
    }
}

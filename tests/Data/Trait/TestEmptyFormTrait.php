<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait TestEmptyFormTrait
{
    use FormTrait;

    public function testEmptyForm(): void
    {
        $this->checkForm('error');
    }
}

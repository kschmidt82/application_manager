<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

use App\Tests\Data\Provider\BasicError;
use PHPUnit\Framework\Attributes\DataProviderExternal;

trait TestErrorFormTrait
{
    #[DataProviderExternal(BasicError::class, 'provide')]
    public function testErrorForm(array $formElements, string $text): void
    {
        $this->checkErrorForm($formElements, $text);
    }
}

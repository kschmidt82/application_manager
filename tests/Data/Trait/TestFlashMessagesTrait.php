<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

use function PHPUnit\Framework\assertSame;

trait TestFlashMessagesTrait
{
    use ElementTrait;

    public function testFlashMessages(): void
    {
        $this->setClient();
        if ($this->flashMessages) {
            foreach ($this->flashMessages as $flashMessage) {
                $this->checkElement($flashMessage);
            }
        } else {
            assertSame($this->flashMessages, []);
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait TestNoAdmin
{
    public function testNoAdmin(): void
    {
        $this->setClient(null, null, 2);
        $this->assertResponseStatusCodeSame(403);
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait TestNoDataTrait
{
    use ElementTrait;

    public function testNoData(): void
    {
        $this->setClient('2');
        $this->checkElement(['text' => 'noDataFound', 'selector' => 'span']);
    }
}

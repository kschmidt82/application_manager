<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait TestNoLoginTrait
{
    use ElementTrait;

    public function testNoLogin(): void
    {
        $this->setClient(login: false);
        if ($this->login) {
            $this->checkElement(['text' => 'form.login.submit.label', 'selector' => 'button']);
        } else {
            $this->assertSame(false, $this->login);
        }
    }
}

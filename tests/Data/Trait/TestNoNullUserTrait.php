<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait TestNoNullUserTrait
{
    public function testNoNullUser(): void
    {
        $this->setClient('2/'.$this->method);
        $this->assertResponseStatusCodeSame(404);
    }
}

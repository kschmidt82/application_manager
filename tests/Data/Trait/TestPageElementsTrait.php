<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

use function PHPUnit\Framework\assertSame;

trait TestPageElementsTrait
{
    use ElementTrait;

    public function testPageElements(): void
    {
        $this->setClient();
        if (count($this->pageElements)) {
            foreach ($this->pageElements as $pageElement) {
                $this->checkElement($pageElement);
            }
        } else {
            assertSame($this->pageElements, []);
        }
    }
}

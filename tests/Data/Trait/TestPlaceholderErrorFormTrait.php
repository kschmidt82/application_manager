<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

use App\Tests\Data\Provider\PlaceholderError;
use PHPUnit\Framework\Attributes\DataProviderExternal;

trait TestPlaceholderErrorFormTrait
{
    #[DataProviderExternal(PlaceholderError::class, 'provide')]
    public function testErrorForm(array $formElements, string $text): void
    {
        $this->checkErrorForm($formElements, $text);
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait TestRemoveTrait
{
    use FormTrait;

    public function testRemove(): void
    {
        $this->checkForm('deleteSuccess');
    }
}

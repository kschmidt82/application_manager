<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

use App\Tests\Data\Provider\ResultError;
use App\Tests\Data\Provider\ResultSuccess;
use PHPUnit\Framework\Attributes\DataProviderExternal;

trait TestResultErrorSuccessFormTrait
{
    #[DataProviderExternal(ResultError::class, 'provide')]
    public function testErrorForm(array $formElements, string $text): void
    {
        $this->checkErrorForm($formElements, $text);
    }

    #[DataProviderExternal(ResultSuccess::class, 'provide')]
    public function testSuccessForm(array $formElements): void
    {
        $this->checkSuccessForm($formElements);
    }
}

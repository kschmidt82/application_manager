<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

use App\Tests\Data\Provider\BasicSuccess;
use PHPUnit\Framework\Attributes\DataProviderExternal;

trait TestSuccessFormTrait
{
    #[DataProviderExternal(BasicSuccess::class, 'provide')]
    public function testSuccessForm(array $formElements): void
    {
        $this->checkSuccessForm($formElements);
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Data\Trait;

trait TestWrongUserTrait
{
    public function testWrongUser(): void
    {
        $this->setClient('3/'.$this->method);
        $this->assertResponseStatusCodeSame(403);
    }
}

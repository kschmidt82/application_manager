<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Create;

use App\Tests\Data\Class\Create;
use App\Tests\Data\Provider\ApplicationCreateError;
use App\Tests\Data\Trait\EditTestsTrait;
use PHPUnit\Framework\Attributes\DataProviderExternal;

final class ApplicationControllerTest extends Create
{
    use EditTestsTrait;

    protected array $texts = ['headline' => 'application.create.title', 'labelText' => 'form.company.label'];

    #[DataProviderExternal(ApplicationCreateError::class, 'provide')]
    public function testErrorForm(array $formElements, string $text): void
    {
        $this->checkErrorForm($formElements, $text);
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Create;

use App\Tests\Data\Class\Create;
use App\Tests\Data\Trait\ShowTestsTrait;
use App\Tests\Data\Trait\SuccessFormCheckTrait;
use App\Tests\Data\Trait\TestApplicationEventSuccessFormTrait;

final class ApplicationEventControllerTest extends Create
{
    use TestApplicationEventSuccessFormTrait;
    use SuccessFormCheckTrait;
    use ShowTestsTrait;

    protected array $hrefs = ['topLink' => '/application/1/show', 'backLink' => '/application_event/1'];
    protected array $texts = [
        'headline' => 'application_event.create.title',
        'labelText' => 'form.event.label',
        'topLink' => 'application_event.topName'
    ];
    protected string $url = '1/create';
}

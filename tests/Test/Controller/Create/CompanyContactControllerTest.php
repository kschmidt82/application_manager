<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Create;

use App\Tests\Data\Class\Create;
use App\Tests\Data\Trait\CreateTestsTrait;
use App\Tests\Data\Trait\TestContactErrorSuccessFormTrait;

final class CompanyContactControllerTest extends Create
{
    use CreateTestsTrait;
    use TestContactErrorSuccessFormTrait;

    protected array $hrefs = ['topLink' => '/company/1/show', 'backLink' => '/company_contact/1'];
    protected array $texts = [
        'headline' => 'company_contact.create.title',
        'labelText' => 'form.firstname.label',
        'topLink' => 'company_contact.topName',
        'error' => 'form.lastname.helpText'
    ];
    protected string $url = '1/create';
}

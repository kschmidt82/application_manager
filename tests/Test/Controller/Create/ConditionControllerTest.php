<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Create;

use App\Tests\Data\Class\Create;
use App\Tests\Data\Trait\ErrorFormCheckTrait;
use App\Tests\Data\Trait\ShowTestsTrait;
use App\Tests\Data\Trait\SuccessFormCheckTrait;
use App\Tests\Data\Trait\TestConditionErrorSuccessFormTrait;

final class ConditionControllerTest extends Create
{
    use ErrorFormCheckTrait;
    use ShowTestsTrait;
    use SuccessFormCheckTrait;
    use TestConditionErrorSuccessFormTrait;

    protected array $hrefs = ['topLink' => '/result/1/show', 'backLink' => '/condition/1'];
    protected array $texts = [
        'headline' => 'condition.create.title',
        'labelText' => 'form.operator.label',
        'topLink' => 'condition.topName'
    ];
    protected string $url = '1/create';
}

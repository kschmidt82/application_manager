<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Create;

use App\Tests\Data\Class\Create;
use App\Tests\Data\Trait\CreateTestsTrait;
use App\Tests\Data\Trait\TestPlaceholderErrorFormTrait;
use App\Tests\Data\Trait\TestSuccessFormTrait;

final class PlaceholderControllerTest extends Create
{
    use CreateTestsTrait;
    use TestPlaceholderErrorFormTrait;
    use TestSuccessFormTrait;

    protected array $texts = ['headline' => 'placeholder.create.title'];
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Create;

use App\Tests\Data\Class\Create;
use App\Tests\Data\Trait\CreateTestsTrait;
use App\Tests\Data\Trait\TestContactErrorSuccessFormTrait;

final class RecruiterContactControllerTest extends Create
{
    use CreateTestsTrait;
    use TestContactErrorSuccessFormTrait;

    protected array $hrefs = ['topLink' => '/recruiter/1/show', 'backLink' => '/recruiter_contact/1'];
    protected array $texts = [
        'headline' => 'recruiter_contact.create.title',
        'labelText' => 'form.firstname.label',
        'topLink' => 'recruiter_contact.topName',
        'error' => 'form.lastname.helpText'
    ];
    protected string $url = '1/create';
}

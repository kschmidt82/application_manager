<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Create;

use App\Tests\Data\Class\Create;
use App\Tests\Data\Trait\ErrorFormCheckTrait;
use App\Tests\Data\Trait\ShowTestsTrait;
use App\Tests\Data\Trait\SuccessFormCheckTrait;
use App\Tests\Data\Trait\TestResultErrorSuccessFormTrait;

final class ResultControllerTest extends Create
{
    use ErrorFormCheckTrait;
    use ShowTestsTrait;
    use SuccessFormCheckTrait;
    use TestResultErrorSuccessFormTrait;

    protected array $hrefs = ['topLink' => '/placeholder/1/edit', 'backLink' => '/result/1'];
    protected array $texts = [
        'headline' => 'result.create.title',
        'labelText' => 'form.result.label',
        'topLink' => 'result.topName'
    ];
    protected string $url = '1/create';
}

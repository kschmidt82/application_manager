<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Create;

use App\Tests\Data\Class\Create;
use App\Tests\Data\Trait\CreateBasicTestsTrait;

final class TitleControllerTest extends Create
{
    use CreateBasicTestsTrait;

    protected array $texts = ['headline' => 'title.create.title'];
}

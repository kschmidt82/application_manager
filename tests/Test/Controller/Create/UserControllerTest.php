<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Create;

use App\Tests\Data\Class\Basic;
use App\Tests\Data\Provider\UserCreateError;
use App\Tests\Data\Provider\UserCreateSuccess;
use App\Tests\Data\Trait\CreateTestsTrait;
use App\Tests\Data\Trait\TestNoAdmin;
use PHPUnit\Framework\Attributes\DataProviderExternal;

final class UserControllerTest extends Basic
{
    use CreateTestsTrait;
    use TestNoAdmin;

    protected array $elements = ['saveButton'];
    protected array $texts = [
        'headline' => 'user.create.title',
        'labelText' => 'form.firstname.label',
        'error' => 'form.userUsername.helpText',
        'success' => 'user.create.successful'
    ];
    protected string $url = 'register';

    #[DataProviderExternal(UserCreateError::class, 'provide')]
    public function testErrorForm(array $formElements, string $text): void
    {
        $this->checkErrorForm($formElements, $text);
    }

    #[DataProviderExternal(UserCreateSuccess::class, 'provide')]
    public function testSuccessForm(array $formElements): void
    {
        $this->checkSuccessForm($formElements);
    }
}

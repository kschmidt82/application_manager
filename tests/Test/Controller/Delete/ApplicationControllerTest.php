<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Delete;

use App\Tests\Data\Class\Delete;
use App\Tests\Data\Trait\DeleteTestsTrait;

final class ApplicationControllerTest extends Delete
{
    use DeleteTestsTrait;
}

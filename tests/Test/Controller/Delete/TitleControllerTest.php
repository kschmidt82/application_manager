<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Delete;

use App\Tests\Data\Class\Delete;
use App\Tests\Data\Trait\DeleteTestsTrait;
use App\Tests\Data\Trait\TestNoNullUserTrait;

final class TitleControllerTest extends Delete
{
    use DeleteTestsTrait;
    use TestNoNullUserTrait;
}

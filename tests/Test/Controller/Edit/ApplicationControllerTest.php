<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Edit;

use App\Tests\Data\Class\Edit;
use App\Tests\Data\Provider\ApplicationEditError;
use App\Tests\Data\Trait\EditTestsTrait;
use PHPUnit\Framework\Attributes\DataProviderExternal;

final class ApplicationControllerTest extends Edit
{
    use EditTestsTrait;

    protected array $texts = [
        'headline' => 'application.edit.title',
        'labelText' => 'form.company.label',
        'subLink' => 'application.subName'
    ];

    #[DataProviderExternal(ApplicationEditError::class, 'provide')]
    public function testErrorForm(array $formElements, string $text): void
    {
        $this->checkErrorForm($formElements, $text);
    }
}

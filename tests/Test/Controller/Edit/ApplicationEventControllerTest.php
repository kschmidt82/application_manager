<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Edit;

use App\Tests\Data\Class\Edit;
use App\Tests\Data\Trait\EditTestsTrait;
use App\Tests\Data\Trait\TestApplicationEventSuccessFormTrait;

final class ApplicationEventControllerTest extends Edit
{
    use EditTestsTrait;
    use TestApplicationEventSuccessFormTrait;

    protected array $hrefs = ['topLink' => '/application/1/show', 'backLink' => '/application_event/1'];
    protected array $texts = [
        'headline' => 'application_event.edit.title',
        'labelText' => 'form.event.label',
        'topLink' => 'application_event.topName'
    ];
}

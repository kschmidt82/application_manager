<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Edit;

use App\Tests\Data\Class\Edit;
use App\Tests\Data\Trait\EditTestsTrait;
use App\Tests\Data\Trait\TestContactErrorSuccessFormTrait;

final class CompanyContactControllerTest extends Edit
{
    use EditTestsTrait;
    use TestContactErrorSuccessFormTrait;

    protected array $hrefs = ['topLink' => '/company/1/show', 'backLink' => '/company_contact/1'];
    protected array $texts = [
        'headline' => 'company_contact.edit.title',
        'labelText' => 'form.firstname.label',
        'topLink' => 'company_contact.topName'
    ];
}

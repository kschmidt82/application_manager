<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Edit;

use App\Tests\Data\Class\Edit;
use App\Tests\Data\Trait\EditTestsTrait;
use App\Tests\Data\Trait\TestConditionErrorSuccessFormTrait;

final class ConditionControllerTest extends Edit
{
    use EditTestsTrait;
    use TestConditionErrorSuccessFormTrait;

    protected array $hrefs = ['topLink' => '/result/1/show', 'backLink' => '/condition/1'];
    protected array $texts = [
        'headline' => 'condition.edit.title',
        'labelText' => 'form.operator.label',
        'topLink' => 'condition.topName'
    ];
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Edit;

use App\Tests\Data\Class\Edit;
use App\Tests\Data\Trait\EditBasicTestsTrait;
use App\Tests\Data\Trait\TestNoNullUserTrait;

final class JobTitleControllerTest extends Edit
{
    use EditBasicTestsTrait;
    use TestNoNullUserTrait;

    protected array $texts = ['headline' => 'job_title.edit.title'];
}

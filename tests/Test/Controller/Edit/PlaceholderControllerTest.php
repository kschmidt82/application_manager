<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Edit;

use App\Tests\Data\Class\Edit;
use App\Tests\Data\Trait\EditTestsTrait;
use App\Tests\Data\Trait\TestNoNullUserTrait;
use App\Tests\Data\Trait\TestPlaceholderErrorFormTrait;
use App\Tests\Data\Trait\TestSuccessFormTrait;

final class PlaceholderControllerTest extends Edit
{
    use EditTestsTrait;
    use TestNoNullUserTrait;
    use TestPlaceholderErrorFormTrait;
    use TestSuccessFormTrait;

    protected array $texts = ['headline' => 'placeholder.edit.title', 'subLink' => 'placeholder.subName'];
}

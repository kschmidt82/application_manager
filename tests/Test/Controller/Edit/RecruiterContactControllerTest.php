<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Edit;

use App\Tests\Data\Class\Edit;
use App\Tests\Data\Trait\EditTestsTrait;
use App\Tests\Data\Trait\TestContactErrorSuccessFormTrait;

final class RecruiterContactControllerTest extends Edit
{
    use EditTestsTrait;
    use TestContactErrorSuccessFormTrait;

    protected array $hrefs = ['topLink' => '/recruiter/1/show', 'backLink' => '/recruiter_contact/1'];
    protected array $texts = [
        'headline' => 'recruiter_contact.edit.title',
        'labelText' => 'form.firstname.label',
        'topLink' => 'recruiter_contact.topName'
    ];
}

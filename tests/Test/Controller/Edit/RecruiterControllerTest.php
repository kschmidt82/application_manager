<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Edit;

use App\Tests\Data\Class\Edit;
use App\Tests\Data\Trait\EditBasicTestsTrait;

final class RecruiterControllerTest extends Edit
{
    use EditBasicTestsTrait;

    protected array $texts = ['headline' => 'recruiter.edit.title', 'subLink' => 'recruiter.subName'];
}

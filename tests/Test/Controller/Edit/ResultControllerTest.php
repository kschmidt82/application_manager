<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Edit;

use App\Tests\Data\Class\Edit;
use App\Tests\Data\Trait\EditTestsTrait;
use App\Tests\Data\Trait\TestResultErrorSuccessFormTrait;

final class ResultControllerTest extends Edit
{
    use EditTestsTrait;
    use TestResultErrorSuccessFormTrait;

    protected array $hrefs = ['topLink' => '/placeholder/1/edit', 'backLink' => '/result/1'];
    protected array $texts = [
        'headline' => 'result.edit.title',
        'labelText' => 'form.result.label',
        'topLink' => 'result.topName',
        'subLink' => 'result.subName'
    ];
}

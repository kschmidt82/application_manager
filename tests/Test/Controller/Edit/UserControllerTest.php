<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Edit;

use App\Tests\Data\Class\Edit;
use App\Tests\Data\Provider\UserEditError;
use App\Tests\Data\Provider\UserEditSuccess;
use App\Tests\Data\Trait\EditTestsTrait;
use PHPUnit\Framework\Attributes\DataProviderExternal;

final class UserControllerTest extends Edit
{
    use EditTestsTrait;

    protected array $elements = ['saveButton'];
    protected array $texts = [
        'headline' => 'user.edit.title',
        'labelText' => 'form.firstname.label',
        'text' => 'user.edit.changePassword'
    ];

    #[DataProviderExternal(UserEditError::class, 'provide')]
    public function testErrorForm(array $formElements, string $text): void
    {
        $this->checkErrorForm($formElements, $text);
    }

    #[DataProviderExternal(UserEditSuccess::class, 'provide')]
    public function testSuccessForm(array $formElements): void
    {
        $this->checkSuccessForm($formElements);
    }
}

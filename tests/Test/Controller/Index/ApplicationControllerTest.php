<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Index;

use App\Tests\Data\Class\Index;
use App\Tests\Data\Trait\IndexTestsTrait;

final class ApplicationControllerTest extends Index
{
    use IndexTestsTrait;

    protected array $hrefs = ['multiSubLink' => '/application_event/94', 'backLink' => '/application/emails'];
    protected array $texts = [
        'headline' => 'application.index.title',
        'multiSubLink' => '<i class="bi-calendar-event-fill"></i>',
        'backLink' => 'sendApplicationEmails'
    ];
}

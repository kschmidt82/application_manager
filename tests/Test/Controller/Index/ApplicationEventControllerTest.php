<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Index;

use App\Tests\Data\Class\Index;
use App\Tests\Data\Trait\IndexTestsTrait;

final class ApplicationEventControllerTest extends Index
{
    use IndexTestsTrait;

    protected array $hrefs = [
        'createLink' => '/application_event/1/create',
        'topLink' => '/application/1/show',
        'multiShowLink' => '/application_event/1/show',
        'multiEditLink' => '/application_event/1/edit'
    ];
    protected array $texts = [
        'headline' => 'application_event.index.title',
        'topLink' => 'application_event.topName',
        'column' => 'N1'
    ];
    protected string $url = '1';
}

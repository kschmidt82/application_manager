<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Index;

use App\Tests\Data\Class\Index;
use App\Tests\Data\Trait\IndexTestsTrait;
use App\Tests\Data\Trait\TestNoDataTrait;

final class CompanyContactControllerTest extends Index
{
    use IndexTestsTrait;
    use TestNoDataTrait;

    protected array $hrefs = ['createLink' => '/company_contact/1/create', 'topLink' => '/company/1/show'];
    protected array $texts = [
        'headline' => 'company_contact.index.title',
        'topLink' => 'company_contact.topName',
        'column' => 'L94'
    ];
    protected string $url = '1';
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Index;

use App\Tests\Data\Class\Index;
use App\Tests\Data\Trait\IndexTestsTrait;

final class CompanyControllerTest extends Index
{
    use IndexTestsTrait;

    protected array $hrefs = ['multiSubLink' => '/company_contact/94'];
    protected array $texts = ['headline' => 'company.index.title'];
}

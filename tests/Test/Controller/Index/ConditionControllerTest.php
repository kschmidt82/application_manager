<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Index;

use App\Tests\Data\Class\Index;
use App\Tests\Data\Trait\IndexTestsTrait;

final class ConditionControllerTest extends Index
{
    use IndexTestsTrait;

    protected array $hrefs = ['createLink' => '/condition/1/create', 'topLink' => '/result/1/show'];
    protected array $texts = ['headline' => 'condition.index.title', 'topLink' => 'condition.topName'];
    protected string $url = '1';
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Index;

use App\Tests\Data\Class\Index;
use App\Tests\Data\Trait\IndexTestsTrait;

final class JobMarketControllerTest extends Index
{
    use IndexTestsTrait;

    protected array $texts = ['headline' => 'job_market.index.title'];
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Index;

use App\Tests\Data\Class\Basic;
use App\Tests\Data\Trait\IndexTestsTrait;

final class PlaceholderControllerTest extends Basic
{
    use IndexTestsTrait;

    protected array $elements = ['createLink', 'multiEditLink', 'multiDeleteLink', 'column'];
    protected array $hrefs = ['multiSubLink' => '/result/94'];
    protected array $texts = [
        'headline' => 'placeholder.index.title',
        'multiSubLink' => '<i class="bi-asterisk"></i>'
    ];
}

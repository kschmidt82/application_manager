<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Index;

use App\Tests\Data\Class\Index;
use App\Tests\Data\Trait\IndexTestsTrait;
use App\Tests\Data\Trait\TestNoDataTrait;

final class RecruiterContactControllerTest extends Index
{
    use IndexTestsTrait;
    use TestNoDataTrait;

    protected array $hrefs = ['createLink' => '/recruiter_contact/1/create', 'topLink' => '/recruiter/1/show'];
    protected array $texts = [
        'headline' => 'recruiter_contact.index.title',
        'topLink' => 'recruiter_contact.topName',
        'column' => 'L94'
    ];
    protected string $url = '1';
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Index;

use App\Tests\Data\Class\Index;
use App\Tests\Data\Trait\IndexTestsTrait;

final class ResultControllerTest extends Index
{
    use IndexTestsTrait;

    protected array $hrefs = [
        'createLink' => '/result/1/create',
        'topLink' => '/placeholder/1/edit',
        'multiSubLink' => '/condition/94'
    ];
    protected array $texts = [
        'headline' => 'result.index.title',
        'topLink' => 'result.topName',
        'multiSubLink' => '<i class="bi-asterisk"></i>',
        'column' => 'R94'
    ];
    protected string $url = '1';
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Index;

use App\Tests\Data\Class\Basic;
use App\Tests\Data\Trait\IndexTestsTrait;

final class TitleControllerTest extends Basic
{
    use IndexTestsTrait;

    protected array $elements = ['createLink', 'multiEditLink', 'multiDeleteLink', 'column'];
    protected array $texts = ['headline' => 'title.index.title'];
}

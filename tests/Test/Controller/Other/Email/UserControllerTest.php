<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Other\Email;

use App\Tests\Data\Class\Basic;
use App\Tests\Data\Trait\IndexTestsTrait;
use App\Tests\Data\Trait\TestFlashMessagesTrait;

final class UserControllerTest extends Basic
{
    use IndexTestsTrait;
    use TestFlashMessagesTrait;

    protected array $flashMessages = ['error' => ['text' => 'user.email.error', 'selector' => 'span']];
    protected string $url = 'email/1/testuser1/e2@test.test';
}

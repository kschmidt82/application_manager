<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Other\Emails;

use App\Tests\Data\Class\Basic;
use App\Tests\Data\Trait\IndexTestsTrait;
use App\Tests\Data\Trait\TestFlashMessagesTrait;

final class ApplicationControllerTest extends Basic
{
    use IndexTestsTrait;
    use TestFlashMessagesTrait;

    protected array $flashMessages = ['success' => ['text' => 'emailsSentSuccessfully', 'selector' => 'span']];
    protected array $texts = ['headline' => 'application.index.title'];
    protected string $url = 'emails';
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Other\Export;

use App\Tests\Data\Class\Basic;
use App\Tests\Data\Trait\IndexTestsTrait;

final class ExportControllerTest extends Basic
{
    use IndexTestsTrait;

    protected array $texts = ['headline' => 'export.export.title'];
}

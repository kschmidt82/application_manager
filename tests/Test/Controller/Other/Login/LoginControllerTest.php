<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Other\Login;

use App\Tests\Data\Class\Basic;
use App\Tests\Data\Provider\LoginError;
use App\Tests\Data\Provider\LoginSuccess;
use App\Tests\Data\Trait\CreateTestsTrait;
use PHPUnit\Framework\Attributes\DataProviderExternal;

final class LoginControllerTest extends Basic
{
    use CreateTestsTrait;

    protected string $controller = '';
    protected bool $login = false;
    protected array $selectors = ['error' => 'span'];
    protected array $texts = [
        'headline' => 'login.login.title',
        'saveButton' => 'form.login.submit.label',
        'error' => 'Invalid credentials.',
        'success' => 'login.login.loginSuccessful'
    ];

    #[DataProviderExternal(LoginError::class, 'provide')]
    public function testErrorForm(array $formElements, string $text): void
    {
        $this->checkErrorForm($formElements, $text);
    }

    #[DataProviderExternal(LoginSuccess::class, 'provide')]
    public function testSuccessForm(array $formElements): void
    {
        $this->checkSuccessForm($formElements);
    }
}

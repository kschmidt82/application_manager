<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Other\Logout;

use App\Tests\Data\Class\Basic;
use App\Tests\Data\Trait\IndexTestsTrait;

final class LoginControllerTest extends Basic
{
    use IndexTestsTrait;

    protected string $controller = '';
    protected bool $login = false;
    protected array $texts = ['headline' => 'login.login.title', 'saveButton' => 'form.login.submit.label'];
    protected string $url = 'logout';
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Other\Request;

use App\Tests\Data\Class\Basic;
use App\Tests\Data\Provider\ResetPasswordRequestError;
use App\Tests\Data\Provider\ResetPasswordRequestSuccess;
use App\Tests\Data\Trait\CreateTestsTrait;
use PHPUnit\Framework\Attributes\DataProviderExternal;

final class ResetPasswordRequestControllerTest extends Basic
{
    use CreateTestsTrait;

    protected string $controller = 'reset-password';
    protected bool $login = false;
    protected array $texts = [
        'headline' => 'reset_password_request.request.title',
        'saveButton' => 'form.resetPasswordRequest.submit.label',
        'error' => 'form.emailAddress.helpText',
        'success' => 'reset_password_request.check_email.exist'
    ];

    #[DataProviderExternal(ResetPasswordRequestError::class, 'provide')]
    public function testErrorForm(array $formElements, string $text): void
    {
        $this->checkErrorForm($formElements, $text);
    }

    #[DataProviderExternal(ResetPasswordRequestSuccess::class, 'provide')]
    public function testSuccessForm(array $formElements): void
    {
        $this->checkSuccessForm($formElements);
    }
}

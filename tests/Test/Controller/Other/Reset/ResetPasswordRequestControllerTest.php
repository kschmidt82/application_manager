<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Other\Reset;

use App\Tests\Data\Class\Basic;
use App\Tests\Data\Trait\IndexTestsTrait;

final class ResetPasswordRequestControllerTest extends Basic
{
    use IndexTestsTrait;

    protected string $controller = 'reset-password';
    protected bool $login = false;
    protected array $texts = ['headline' => 'reset_password_request.request.title'];
    protected string $url = 'reset/test';
}

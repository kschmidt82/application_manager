<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Other\VerifyEmail;

use App\Tests\Data\Class\Basic;
use App\Tests\Data\Trait\IndexTestsTrait;
use App\Tests\Data\Trait\TestFlashMessagesTrait;

final class UserControllerTest extends Basic
{
    use IndexTestsTrait;
    use TestFlashMessagesTrait;

    protected array $flashMessages = ['error' => ['text' => 'user.create.error', 'selector' => 'span']];
    protected string $url = 'verify/email?id=x';
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Show;

use App\Tests\Data\Class\Show;
use App\Tests\Data\Trait\ShowTestsTrait;

final class ApplicationControllerTest extends Show
{
    use ShowTestsTrait;

    protected array $hrefs = ['subLink' => '/application_event/94'];
    protected array $texts = ['headline' => 'application.show.title', 'subLink' => 'application.subName'];
}

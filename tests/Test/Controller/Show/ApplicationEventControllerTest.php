<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Show;

use App\Tests\Data\Class\Show;
use App\Tests\Data\Trait\ShowTestsTrait;

final class ApplicationEventControllerTest extends Show
{
    use ShowTestsTrait;

    protected array $hrefs = ['topLink' => '/application/94/show', 'backLink' => '/application_event/94'];
    protected array $texts = ['headline' => 'application_event.show.title', 'topLink' => 'application_event.topName'];
}

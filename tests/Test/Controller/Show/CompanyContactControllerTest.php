<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Show;

use App\Tests\Data\Class\Show;
use App\Tests\Data\Trait\ShowTestsTrait;

final class CompanyContactControllerTest extends Show
{
    use ShowTestsTrait;

    protected array $hrefs = ['topLink' => '/company/1/show', 'backLink' => '/company_contact/1'];
    protected array $texts = [
        'headline' => 'company_contact.show.title',
        'topLink' => 'company_contact.topName',
        'column' => 'L94'
    ];
}

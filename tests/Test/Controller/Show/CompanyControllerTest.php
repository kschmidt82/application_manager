<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Show;

use App\Tests\Data\Class\Show;
use App\Tests\Data\Trait\ShowTestsTrait;

final class CompanyControllerTest extends Show
{
    use ShowTestsTrait;

    protected array $hrefs = ['subLink' => '/company_contact/94'];
    protected array $texts = ['headline' => 'company.show.title', 'subLink' => 'company.subName'];
}

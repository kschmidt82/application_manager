<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Show;

use App\Tests\Data\Class\Show;
use App\Tests\Data\Trait\ShowTestsTrait;

final class ConditionControllerTest extends Show
{
    use ShowTestsTrait;

    protected array $hrefs = ['topLink' => '/result/1/show', 'backLink' => '/condition/1'];
    protected array $texts = ['headline' => 'condition.show.title', 'topLink' => 'condition.topName'];
}

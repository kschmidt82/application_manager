<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Show;

use App\Tests\Data\Class\Show;
use App\Tests\Data\Trait\ShowTestsTrait;

final class EmailControllerTest extends Show
{
    use ShowTestsTrait;

    protected array $texts = ['headline' => 'email.show.title'];
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Show;

use App\Tests\Data\Class\Show;
use App\Tests\Data\Trait\ShowTestsTrait;
use App\Tests\Data\Trait\TestCorrectNullUserTrait;

final class JobMarketControllerTest extends Show
{
    use ShowTestsTrait;
    use TestCorrectNullUserTrait;

    protected array $texts = ['headline' => 'job_market.show.title'];
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Show;

use App\Tests\Data\Class\Show;
use App\Tests\Data\Trait\ShowTestsTrait;

final class RecruiterContactControllerTest extends Show
{
    use ShowTestsTrait;

    protected array $hrefs = ['topLink' => '/recruiter/1/show', 'backLink' => '/recruiter_contact/1'];
    protected array $texts = [
        'headline' => 'recruiter_contact.show.title',
        'topLink' => 'recruiter_contact.topName',
        'column' => 'L94'
    ];
}

<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Show;

use App\Tests\Data\Class\Show;
use App\Tests\Data\Trait\ShowTestsTrait;

final class RecruiterControllerTest extends Show
{
    use ShowTestsTrait;

    protected array $hrefs = ['subLink' => '/recruiter_contact/94'];
    protected array $texts = ['headline' => 'recruiter.show.title', 'subLink' => 'recruiter.subName'];
}

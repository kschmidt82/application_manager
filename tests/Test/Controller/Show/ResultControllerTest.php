<?php

declare(strict_types=1);

namespace App\Tests\Test\Controller\Show;

use App\Tests\Data\Class\Show;
use App\Tests\Data\Trait\ShowTestsTrait;

final class ResultControllerTest extends Show
{
    use ShowTestsTrait;

    protected array $hrefs = [
        'topLink' => '/placeholder/1/edit',
        'backLink' => '/result/1',
        'subLink' => '/condition/94'
    ];
    protected array $texts = [
        'headline' => 'result.show.title',
        'topLink' => 'result.topName',
        'subLink' => 'result.subName',
        'column' => 'R94'
    ];
}
